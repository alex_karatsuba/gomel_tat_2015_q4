package TestClasses;

import Letter.LetterFactory;
import Page.YandexMail.LoginPage;
import Page.YandexMail.PrepareLetterPage;
import Util.WebDriverHelper;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by Shaman on 27.12.2015.
 */
public class WrongAddressNameTest extends BaseTestClass {

    public static final By WRONG_ADDRESS_NAME_ERROR_LOCATOR = By.xpath("//span[@class = 'b-notification b-notification_error b-notification_error_invalid']");

    @Test(description = "Wrong address name test")
    public void realizeWrongAddressNameTest() {
        LoginPage wrongAddress = new LoginPage(driver);
        wrongAddress.login();
        PrepareLetterPage.prepareSentLetter(driver, LetterFactory.getWrongAddressLetter());
        PrepareLetterPage.sentLetter(driver);
        boolean result = WebDriverHelper.elementIsOnThisPage(driver, WRONG_ADDRESS_NAME_ERROR_LOCATOR);
        Assert.assertEquals(result, true, "You should write letter with wrong address." +
                " Mistake.");
    }
}
