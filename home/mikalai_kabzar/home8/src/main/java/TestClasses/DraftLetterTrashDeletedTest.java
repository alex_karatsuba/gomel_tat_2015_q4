package TestClasses;

import Letter.LetterFactory;
import Page.YandexMail.*;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by Shaman on 28.12.2015.
 */
public class DraftLetterTrashDeletedTest extends BaseTestClass {

    private boolean letterIsInDraft = false;
    private boolean letterIsInTrash = false;
    private boolean letterDeleted = false;

    @Test(description = "Mail draft, trash and delete test")
    public void realizeMailDraftTrashDeleteTest() {
        boolean LetterInDraft = true;
        boolean LetterInTrash = true;
        IncomingPage incomingPage = new IncomingPage(driver);
        incomingPage.login();
        PrepareLetterPage.prepareSentLetter(driver, LetterFactory.getRandomLetter());
        incomingPage.setLetterId(PrepareLetterPage.findLetterIdDraft(PrepareLetterPage.waitForDraftLetter(driver)));
        DraftPage draftPage = new DraftPage(driver);
        draftPage.open();
        letterIsInDraft = draftPage.checkDeleteLetterIsHere(incomingPage.getLetterId());
        draftPage.goToLetter(incomingPage.getLetterId());
        incomingPage.sleep(1);
        draftPage.deleteLetter();
        incomingPage.sleep(1);
        TrashPage trashPage = new TrashPage(driver);
        trashPage.open();
        letterIsInTrash = trashPage.checkDeleteLetterIsHere(incomingPage.getLetterId());
        trashPage.goToLetter(incomingPage.getLetterId());
        trashPage.deleteLetter();
        incomingPage.sleep(1);
        LetterInTrash = trashPage.checkLetterIsHere(incomingPage.getLetterId());
        draftPage.open();
        LetterInDraft = draftPage.checkLetterIsHere(incomingPage.getLetterId());
        if (!LetterInTrash && !LetterInDraft) {
            letterDeleted = true;
        }
        boolean result;
        if (letterIsInDraft && letterIsInTrash && letterDeleted) {
            result = true;
        } else {
            result = false;
        }
        Assert.assertEquals(result, true, "Mail is not exist in folder Draft or Trash or not delete" +
                " Mistake.");
    }
}
