package Util;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Shaman on 03.01.2016.
 */
public class WebDriverHelper {

    public static boolean elementIsOnThisPage(WebDriver driver, By locator) {
        boolean result = true;
        try {
            new WebDriverWait(driver, 5).until(ExpectedConditions.visibilityOfElementLocated(locator));
        } catch (Exception e) {
            result = false;
        }
        return result;
    }

    public static WebElement waitForElementIsClickable(WebDriver driver, By locator) {
        new WebDriverWait(driver, 20).until(ExpectedConditions.elementToBeClickable(locator));
        return driver.findElement(locator);
    }
}
