package Page.YandexMail;

import Util.WebDriverHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by Shaman on 05.01.2016.
 */
public class TrashPage extends LoginPage {

    public TrashPage(WebDriver driver) {
        super(driver);
    }

    public void open() {
        WebDriverHelper.waitForElementIsClickable(driver, TRASH_MAIL_FOLDER_LOCATOR).click();
    }

    public void goToLetter(String letterId) {
        WebElement trashLetter = WebDriverHelper.waitForElementIsClickable(driver, By.xpath("//div[contains(@class,'" + letterId + "')]"));
        trashLetter.click();
    }

    public void deleteLetter() {
        WebElement deleteButton = WebDriverHelper.waitForElementIsClickable(driver, By.xpath("//a[@data-action = 'delete']"));
        deleteButton.click();
    }

    public boolean checkLetterIsHere(String letterId) {
        return WebDriverHelper.elementIsOnThisPage(driver, By.xpath("//div[@data-id = '" + letterId + "']"));
    }

}
