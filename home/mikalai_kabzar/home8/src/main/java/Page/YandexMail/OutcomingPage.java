package Page.YandexMail;

import Util.WebDriverHelper;
import org.openqa.selenium.WebDriver;

/**
 * Created by Shaman on 05.01.2016.
 */
public class OutcomingPage extends LoginPage {

    public OutcomingPage(WebDriver driver) {
        super(driver);
    }

    public void open() {
        WebDriverHelper.waitForElementIsClickable(driver, TRASH_MAIL_FOLDER_LOCATOR).click();
    }
}
