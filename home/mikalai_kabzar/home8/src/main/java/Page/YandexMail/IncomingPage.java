package Page.YandexMail;

import Util.WebDriverHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by Shaman on 05.01.2016.
 */
public class IncomingPage extends LoginPage {

    protected String letterId = "";

    public IncomingPage(WebDriver driver) {
        super(driver);
    }

    public void open() {
        WebDriverHelper.waitForElementIsClickable(driver, MAIL_INCOMING_FOLDER_LOCATOR).click();
    }

    public void findLetterId() {
        String letterIdTemp = driver.findElement(By.xpath("//div[@class = 'b-mail-dropdown__item b-mail-dropdown__item_with-icon']/a[contains(@href,'#thread')]")).getAttribute("href");
        letterId = letterIdTemp.substring(letterIdTemp.lastIndexOf("/") + 1);
    }

    public void findEmptyLetterId() {
        String urlTemp = driver.findElement(By.xpath("//a[@class = 'b-statusline__link']")).getAttribute("href");
        open();
        letterId = urlTemp.substring(urlTemp.lastIndexOf("/") + 1);
    }

    public String getLetterId() {
        return letterId;
    }

    public void setLetterId(String letterId) {
        this.letterId = letterId;
    }


}
