package TestClasses;

import Util.Content;
import Util.WebDriverHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

/**
 * Created by Shaman on 05.01.2016.
 */
public class BaseTestClass {

    public static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 20;
    public static final String DOWNLOAD_PATH = "C:\\";
    public static final String LOCALHOST = "http://localhost:5555/wd/hub";

    public WebDriver driver;

    @BeforeClass(groups = "Firefox")
    public WebDriver prepareBrowserFirefox() {
        if (driver != null) {
            shutdownWebDriver();
        }
        Content.fillContentMap(new java.io.File("").getAbsolutePath() +
                "\\src\\main\\resources\\Files for tests\\Upload\\", "*.txt");
        WebDriverHelper.setDownloadPath(DOWNLOAD_PATH);
        FirefoxProfile profile = new FirefoxProfile();
        profile.setPreference("browser.download.dir",
                WebDriverHelper.getDownloadPath());
        profile.setPreference("browser.download.folderList", 2);
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "text/plain");
        driver = new FirefoxDriver(profile);
        driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS,
                TimeUnit.SECONDS);
        return driver;
    }

    @BeforeClass(groups = "Chrome")
    public WebDriver prepareBrowserChrome() {
        if (driver != null) {
            shutdownWebDriver();
        }
        Content.fillContentMap(new java.io.File("").getAbsolutePath() +
                "\\src\\main\\resources\\Files for tests\\Upload\\", "*.txt");
        WebDriverHelper.setDownloadPath(DOWNLOAD_PATH);
        HashMap<String, Object> chromeOptions = new HashMap<>();
        chromeOptions.put("profile.default_content_settings.popups", 0);
        chromeOptions.put("download.prompt_for_download", "false");
        chromeOptions.put("download.default_directory", DOWNLOAD_PATH);
        chromeOptions.put("download.directory_upgrade", true);
        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("prefs", chromeOptions);
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        try {
            driver = new RemoteWebDriver(new URL(LOCALHOST), capabilities);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS,
                TimeUnit.SECONDS);
        return driver;
    }

    @AfterClass
    public void shutdownWebDriver() {
        try {
            driver.quit();
        } catch (Exception e) {
            System.err.println("Problem with shutting down driver: " + e.getMessage());
        } finally {
            driver = null;
        }
    }

}
