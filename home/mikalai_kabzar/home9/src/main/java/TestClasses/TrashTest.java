package TestClasses;

import Page.YandexMail.YandexDiskPage;
import Page.YandexMail.YandexTrashPage;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by Shaman on 09.01.2016.
 */
public class TrashTest extends BaseTestClass {

    @Test(description = "Trash test")
    public void realizeTrashTest() {
        YandexDiskPage yandexDiskPage = new YandexDiskPage(driver);
        yandexDiskPage.login();
        yandexDiskPage.open();
        YandexTrashPage yandexTrashPage = new YandexTrashPage(driver);
        yandexTrashPage.clearYandexDiskTrash();
        Assert.assertTrue(yandexDiskPage.upload("TestFile№1.txt"));
        //Assert.assertTrue(yandexDiskPage.upload("TestFile№2.txt"));
        //Assert.assertTrue(yandexDiskPage.upload("TestFile№3.txt"));
        //Assert.assertTrue(yandexDiskPage.upload("TestFile№4.txt"));
        //Assert.assertTrue(yandexDiskPage.upload("TestFile№5.txt"));
        yandexDiskPage.refresh();
        Assert.assertTrue(yandexDiskPage.deleteFileToTrash("TestFile№1.txt"));
        //Assert.assertTrue(yandexDiskPage.deleteFileToTrash("TestFile№2.txt"));
        //Assert.assertTrue(yandexDiskPage.deleteFileToTrash("TestFile№3.txt"));
        //Assert.assertTrue(yandexDiskPage.deleteFileToTrash("TestFile№4.txt"));
        //Assert.assertTrue(yandexDiskPage.deleteFileToTrash("TestFile№5.txt"));

        yandexTrashPage.open();
        Assert.assertTrue(yandexTrashPage.deleteFilePermanently("TestFile№1.txt"));
        //Assert.assertTrue(yandexTrashPage.deleteFilePermanently("TestFile№2.txt"));
        //Assert.assertTrue(yandexTrashPage.deleteFilePermanently("TestFile№3.txt"));
        //Assert.assertTrue(yandexTrashPage.deleteFilePermanently("TestFile№4.txt"));
        //Assert.assertTrue(yandexTrashPage.deleteFilePermanently("TestFile№5.txt"));
    }
}
