package TestClasses;

import Page.YandexMail.YandexDiskPage;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by Shaman on 24.12.2015.
 */
public class UploadDownloadTest extends BaseTestClass {

    @Test(description = "Upload/Download test")
    public void realizeUploadDownloadTest() {
        YandexDiskPage yandexDiskPage = new YandexDiskPage(driver);
        yandexDiskPage.login();
        yandexDiskPage.open();
        Assert.assertTrue(yandexDiskPage.upload("TestFile№1.txt"));
        //Assert.assertTrue(yandexDiskPage.upload("TestFile№2.txt"));
        //Assert.assertTrue(yandexDiskPage.upload("TestFile№3.txt"));
        //Assert.assertTrue(yandexDiskPage.upload("TestFile№4.txt"));
        //Assert.assertTrue(yandexDiskPage.upload("TestFile№5.txt"));
        yandexDiskPage.refresh();
        Assert.assertTrue(yandexDiskPage.download("TestFile№1.txt"));
        //Assert.assertTrue(yandexDiskPage.download("TestFile№2.txt"));
        //Assert.assertTrue(yandexDiskPage.download("TestFile№3.txt"));
        //Assert.assertTrue(yandexDiskPage.download("TestFile№4.txt"));
        //Assert.assertTrue(yandexDiskPage.download("TestFile№5.txt"));
    }
}
