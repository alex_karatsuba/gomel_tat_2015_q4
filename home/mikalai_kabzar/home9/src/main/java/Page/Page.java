package Page;

import org.openqa.selenium.WebDriver;

/**
 * Created by Shaman on 03.01.2016.
 */
public class Page {

    protected WebDriver driver;

    public Page(WebDriver driver) {
        this.driver = driver;
    }

    public void refresh() {
        driver.navigate().refresh();
    }

    public static void sleep(int sec) {
        try {
            Thread.sleep(Math.round(1000 * sec));
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
    }
}
