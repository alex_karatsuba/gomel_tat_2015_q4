package Page.YandexMail;

import Util.Content;
import Util.WebDriverHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

import java.io.File;
import java.util.List;

/**
 * Created by Shaman on 08.01.2016.
 */
public class YandexDiskPage extends LoginPage {

    public static final By YANDEX_DISK_FOLDER_LOCATOR =
            By.xpath("//a[contains(@class,'item_disk')]");
    public static final By YANDEX_DISK_FILES_LOCATOR =
            By.xpath("//div[contains(@class,'visible')]/div[@data-id = 'disk']");
    public static final By UPLOAD_BUTTON_LOCATOR =
            By.xpath("//input[@class = 'button__attach']");
    public static final By ALREADY_EXCISTS_LOCATOR =
            By.xpath("//span[@class = 'b-item-upload__error']");
    public static final By REPLACE_BUTTON_LOCATOR =
            By.xpath("//button[contains(@class,'replace')]");
    public static final By HIDE_BUTTON_LOCATOR =
            By.xpath("//button[contains(@class,'hide ns-action js-hide')]");
    public static final By CLOSE_BUTTON_LOCATOR =
            By.xpath("//button[contains(@class,'button-close ns-action js-hide')]");
    public static final By FILE_TO_TRASH_BUTTON_LOCATOR =
            By.xpath("//div[contains(@data-params,'/trash')]");
    public static final String projectPath = new java.io.File("").getAbsolutePath();
    public static final String uploadPath = projectPath + "\\src\\main\\resources\\" +
            "Files for tests\\Upload\\";
    public static final By FILE_PERMANENT_DELETE_BUTTON_LOCATOR =
            By.xpath("//div[contains(@class,'visible')]/div/div/div" +
                    "/button[@data-click-action='resource.delete']");
    public static final By DELETE_FILES_BUTTON_LOCATOR =
            By.xpath("//button[contains(@data-params,'resource.delete')]");
    public static final By PROGRESSBAR_LOCATOR =
            By.xpath("//div[@class = 'b-progressbar']");

    public YandexDiskPage(WebDriver driver) {
        super(driver);
    }

    public void open() {
        WebDriverHelper.waitForElementIsClickable(driver,
                YANDEX_DISK_FOLDER_LOCATOR).click();
    }

    public void openFiles() {
        WebDriverHelper.waitForElementIsClickable(driver,
                YANDEX_DISK_FILES_LOCATOR).click();
    }

    public By getFileXpath(String fileName) {
        return By.xpath("//div[@title = '" + fileName + "' and @data-nb = 'resource']");
    }

    private By getFileDownloadButton(String fileName) {
        return By.xpath("//button[@data-click-action='resource.download' " +
                "and contains(@data-params,'" + fileName + "')]");
    }

    public boolean upload(String fileName) {
        boolean fileUploadCorrect = false;
        try {
            WebDriverHelper.elementIsOnThisPage(driver, UPLOAD_BUTTON_LOCATOR);
            driver.findElement(UPLOAD_BUTTON_LOCATOR).sendKeys(uploadPath + fileName);
            if (WebDriverHelper.elementIsOnThisPage(driver, ALREADY_EXCISTS_LOCATOR)) {
                WebDriverHelper.waitForElementIsClickable(driver,
                        REPLACE_BUTTON_LOCATOR).click();
            }
            WebDriverHelper.waitUntilElementIsHide(driver,
                    HIDE_BUTTON_LOCATOR);
            WebDriverHelper.waitForElementIsClickable(driver,
                    CLOSE_BUTTON_LOCATOR).click();
        } catch (Exception e) {

        }
        fileUploadCorrect = WebDriverHelper.elementIsOnThisPage(driver,
                getFileXpath(fileName));
        return fileUploadCorrect;
    }

    public boolean download(String fileName) {
        boolean fileDownloadCorrect = false;
        File file = new File(WebDriverHelper.getDownloadPath() + fileName);
        if (file.exists()) {
            file.delete();
        }
        WebDriverHelper.waitForElementIsClickable(driver,
                getFileXpath(fileName)).click();
        WebDriverHelper.waitForElementIsClickable(driver,
                getFileDownloadButton(fileName)).click();
        if (!file.exists()) {
            int timer = 0;
            while (!file.exists() && timer < 120) {
                sleep(1);
                timer++;
            }
        }
        if (file.exists() && Content.getContentByFileName(fileName).
                equals(Content.readFile(file))) {
            fileDownloadCorrect = true;
        }
        return fileDownloadCorrect;
    }

    public boolean deleteFileToTrash(String fileName) {
        boolean fileDeleteCorrect = false;
        WebElement file = WebDriverHelper.waitForElementIsClickable(driver,
                getFileXpath(fileName));
        WebElement trash = WebDriverHelper.waitForElementIsClickable(driver,
                FILE_TO_TRASH_BUTTON_LOCATOR);
        Action action = new Actions(driver).dragAndDrop(file, trash).build();
        action.perform();
        WebDriverHelper.waitUntilElementIsHide(driver, PROGRESSBAR_LOCATOR);
        fileDeleteCorrect = !WebDriverHelper.elementIsOnThisPage(driver,
                getFileXpath(fileName));
        return fileDeleteCorrect;
    }

    public boolean deleteFilesCtrlClick(List<String> filesForTest) {
        boolean filesDeleteCorrect = true;
        Actions actions = new Actions(driver);
        actions.keyDown(Keys.CONTROL);
        for (int i = 0; i < filesForTest.size(); i++) {
            actions.click(WebDriverHelper.waitForElementIsClickable(driver,
                    getFileXpath(filesForTest.get(i))));
        }
        WebElement file = WebDriverHelper.waitForElementIsClickable(driver,
                getFileXpath(filesForTest.get(0)));
        WebElement trash = WebDriverHelper.waitForElementIsClickable(driver,
                FILE_TO_TRASH_BUTTON_LOCATOR);
        actions.keyUp(Keys.CONTROL).build().perform();
        Action action = new Actions(driver).dragAndDrop(file, trash).build();
        action.perform();
        WebDriverHelper.waitUntilElementIsHide(driver, PROGRESSBAR_LOCATOR);
        for (int i = 0; i < filesForTest.size(); i++) {
            if (WebDriverHelper.elementIsOnThisPage(driver,
                    getFileXpath(filesForTest.get(i)))) {
                filesDeleteCorrect = false;
            }
        }
        return filesDeleteCorrect;
    }
}

