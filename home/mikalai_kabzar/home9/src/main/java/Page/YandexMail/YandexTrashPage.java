package Page.YandexMail;

import Util.WebDriverHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.List;

/**
 * Created by Shaman on 09.01.2016.
 */
public class YandexTrashPage extends YandexDiskPage {

    public static final By TO_TRASH_FOLDER_LOCATOR = By.xpath("//" +
            "div[contains(@class,'visible')]/div/a[@href = '/client/trash']");
    public static final By CLEAN_TRASH_BUTTON_LOCATOR =
            By.xpath("//button[@data-click-action='trash.clean']");
    public static final By EMPTY_TRASH_BUTTON_LOCATOR =
            By.xpath("//button[@data-click-action='trash.clean' and contains(@class,'ui-button-disabled')]");
    public static final By ACCEPT_CLEAN_TRASH_BUTTON_LOCATOR =
            By.xpath("//button[contains(@class,'confirmation-accept')]");
    public YandexTrashPage(WebDriver driver) {
        super(driver);
    }

    public void open() {
        WebDriverHelper.waitForElementIsClickable(driver, TO_TRASH_FOLDER_LOCATOR)
                .click();
    }

    private By getFileDeletePermanentlyButton(String fileName) {
        return By.xpath("//div[contains(@class,'visible')]/div/div/div/" +
                "button[@data-click-action='resource.delete' and " +
                "contains(@data-params,'" + fileName + "')]");
    }

    private By getFileRestoreButton(String fileName) {
        return By.xpath("//div[contains(@class,'visible')]/div/div/div/" +
                "button[@data-click-action='resource.restore' and " +
                "contains(@data-params,'" + fileName + "')]");
    }

    public boolean deleteFilePermanently(String fileName) {
        boolean fileIsInTrash = false;
        boolean fileDeletePermanently = false;
        fileIsInTrash = WebDriverHelper.elementIsOnThisPage(driver,
                getFileXpath(fileName));
        WebDriverHelper.waitForElementIsClickable(driver,
                getFileXpath(fileName)).click();
        WebDriverHelper.waitForElementIsClickable(driver,
                getFileDeletePermanentlyButton(fileName)).click();
        WebDriverHelper.waitUntilElementIsHide(driver, PROGRESSBAR_LOCATOR);
        fileDeletePermanently = !WebDriverHelper.elementIsOnThisPage(driver,
                getFileXpath(fileName));
        if (fileIsInTrash && fileDeletePermanently) {
            return true;
        } else {
            return false;
        }
    }

    public boolean restoreFile(String fileName) {
        boolean fileIsInTrash = false;
        boolean fileRestored = false;
        fileIsInTrash = WebDriverHelper.elementIsOnThisPage(driver,
                getFileXpath(fileName));
        WebDriverHelper.waitForElementIsClickable(driver,
                getFileXpath(fileName)).click();
        WebDriverHelper.waitForElementIsClickable(driver,
                getFileRestoreButton(fileName)).click();
        sleep(1);
        fileRestored = !WebDriverHelper.elementIsOnThisPage(driver,
                getFileXpath(fileName));
        if (fileIsInTrash && fileRestored) {
            return true;
        } else {
            return false;
        }
    }

    public boolean deletedFilesCtrlClickIsHere(List<String> filesForTest) {
        boolean deletedCtrlClickFilesIsHere = true;
        for (int i = 0; i < filesForTest.size(); i++) {
            if (!WebDriverHelper.elementIsOnThisPage(driver,
                    getFileXpath(filesForTest.get(i)))) {
                deletedCtrlClickFilesIsHere = false;
            }
        }
        return deletedCtrlClickFilesIsHere;
    }

    public void clearYandexDiskTrash() {
        open();
        if (!WebDriverHelper.elementIsOnThisPage(driver,EMPTY_TRASH_BUTTON_LOCATOR)) {
            WebDriverHelper.waitForElementIsClickable(driver,
                    CLEAN_TRASH_BUTTON_LOCATOR).click();
            WebDriverHelper.waitForElementIsClickable(driver,
                    ACCEPT_CLEAN_TRASH_BUTTON_LOCATOR).click();
            WebDriverHelper.waitUntilElementIsHide(driver, PROGRESSBAR_LOCATOR);
        }
        openFiles();
    }

}
