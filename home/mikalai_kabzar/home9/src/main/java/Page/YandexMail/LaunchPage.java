package Page.YandexMail;

import Page.Page;
import Util.WebDriverHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by Shaman on 24.12.2015.
 */
public class LaunchPage extends Page {

    public static final String BASE_URL = "http://www.ya.ru";
    public static final By ENTER_BUTTON_LOCATOR = By.xpath("//a[contains(@href," +
            " 'mail.yandex')]");

    public LaunchPage(WebDriver driver) {
        super(driver);
    }

    public void launch() {
        driver.get(BASE_URL);
        WebElement enterButton = WebDriverHelper.waitForElementIsClickable(driver,
                ENTER_BUTTON_LOCATOR);
        enterButton.click();
    }

}
