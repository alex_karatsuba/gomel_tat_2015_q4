package Util;

import org.mozilla.universalchardet.UniversalDetector;
import java.io.FileInputStream;

/**
 * Created by Zerg on 15.12.2015.
 */
public class CommonFileUtils {
    public static String getFileEncodingType(String filePath) {
        byte[] buf = new byte[3];
        UniversalDetector detector = new UniversalDetector(null);
        int bufNum;
        try {
            FileInputStream fileInputStream = new FileInputStream(filePath);
            while ((bufNum = fileInputStream.read(buf)) > 0 && !detector.isDone()) {
                detector.handleData(buf, 0, bufNum);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        detector.dataEnd();
        String encoding = detector.getDetectedCharset();
        detector.reset();
        //if (encoding!=null) System.out.println("Found encoding: " + encoding);
        return encoding;
    }
}
