package Instance;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.util.Date;

/**
 * Created by Shaman on 23.12.2015.
 */
public abstract class WebDriverInstanceSetup {

    // AUT data
    public static final String BASE_URL = "http://www.ya.ru";

    // UI data
    public static final By ENTER_BUTTON_LOCATOR = By.xpath("//a[contains(@href, 'mail.yandex')]");
    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");
    public static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    public static final By TO_INPUT_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    public static final By SUBJECT_INPUT_LOCATOR = By.name("subj");
    public static final By MAIL_TEXT_LOCATOR = By.xpath("//*[@id='compose-send']");
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
    public static final By DRAFT_MAIL_FOLDER_LOCATOR = By.xpath("//a[@class = 'b-folders__folder__link' and @href = '#draft']");
    public static final By TRASH_MAIL_FOLDER_LOCATOR = By.xpath("//a[@class = 'b-folders__folder__link' and @href = '#trash']");
    public static final By MAIL_INCOMING_FOLDER_LOCATOR = By.xpath("//a[@class = 'b-folders__folder__link' and @href = '#inbox']");
    public static final By MAIL_OUTCOMING_FOLDER_LOCATOR = By.xpath("//a[@class = 'b-folders__folder__link' and @href = '#sent']");

    // Tools data
    public static final int TIME_OUT_MAIL_ARRIVED_SECONDS = 10;//
    public static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 20;
    public static final int DRIVE_IMPL_WAIT_TIMEOUT_SECONDS = 5;

    //private WebDriver driver;
    public static final String userLogin = "LokiEngine@yandex.ru"; // ACCOUNT
    public static final String userPassword = "TheBestPassword"; // ACCOUNT
    public static final String mailTo = "LokiEngine@yandex.ru"; // ENUM
    public static final String mailSubject = "test subject " + new Date(System.currentTimeMillis()); // RANDOM
    public static final String mailContent = "mail content " + new Date(System.currentTimeMillis());// RANDOM

    public abstract void prepareBrowser() throws MalformedURLException;

    public RemoteWebDriver driver;

    public RemoteWebDriver getDriver() {
        return driver;
    }
}
