package Listeners;

import org.testng.ISuite;
import org.testng.ISuiteListener;
import org.testng.xml.XmlSuite;

/**
 * Created by Shaman on 16.12.2015.
 */
public class Home7SuitListener implements ISuiteListener {

    @Override
    public void onStart(ISuite suite) {
        suite.getXmlSuite().setParallel(XmlSuite.ParallelMode.TRUE);
        suite.getXmlSuite().setThreadCount(4);
    }

    @Override
    public void onFinish(ISuite suite) {

    }
}
