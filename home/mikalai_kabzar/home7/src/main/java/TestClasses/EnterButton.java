package TestClasses;

import Instance.WebDriverInstanceSetup;
import TestClasses.ClassForChrome.WebDriverInstanceSetupChrome;
import TestClasses.ClassForFirefox.WebDriverInstanceSetupFirefox;
import org.openqa.selenium.WebElement;

import java.net.MalformedURLException;

/**
 * Created by Shaman on 24.12.2015.
 */
public class EnterButton {

    protected WebDriverInstanceSetup webDriverInstance;

    public void realizeEnterButton(String browserName) {
        if (browserName.equals("Chrome")) {
            webDriverInstance = new WebDriverInstanceSetupChrome();
        } else if (browserName.equals("Firefox")) {
            webDriverInstance = new WebDriverInstanceSetupFirefox();
        } else {
            webDriverInstance = new WebDriverInstanceSetupFirefox();
            //For mistakes in browserName and other browser
        }
        try {
            webDriverInstance.prepareBrowser();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        webDriverInstance.getDriver().get(WebDriverInstanceSetup.BASE_URL);
        WebElement enterButton = webDriverInstance.getDriver().
                findElement(WebDriverInstanceSetup.ENTER_BUTTON_LOCATOR);
        enterButton.click();
    }

    public WebDriverInstanceSetup getWebDriverInstance() {
        return webDriverInstance;
    }

    protected void sleep(int sec) {
        try {
            Thread.sleep(1000 * sec);                 //1000 milliseconds is one second.
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
    }
}
