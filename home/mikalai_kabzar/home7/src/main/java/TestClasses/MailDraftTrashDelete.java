package TestClasses;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * Created by Shaman on 28.12.2015.
 */
public class MailDraftTrashDelete extends LoginPositive {

    private boolean letterIsInDraft = false;
    private boolean letterIsInTrash = false;
    private boolean letterDeleted = false;
    protected String letterId = "";

    public void realizeMailDraftTrashDelete(String browserName) {

        boolean LetterInDraft = false;
        boolean LetterInTrash = false;

        realizeLoginPositive(browserName);
        WebElement composeButton = webDriverInstance.getDriver().
                findElement(webDriverInstance.COMPOSE_BUTTON_LOCATOR);
        composeButton.click();
        WebElement toInput = webDriverInstance.getDriver().
                findElement(webDriverInstance.TO_INPUT_LOCATOR);
        toInput.sendKeys(webDriverInstance.mailTo);
        WebElement subjectInput = webDriverInstance.getDriver().
                findElement(webDriverInstance.SUBJECT_INPUT_LOCATOR);
        subjectInput.sendKeys(webDriverInstance.mailSubject);
        WebElement mailContentText = webDriverInstance.getDriver().
                findElement(webDriverInstance.MAIL_TEXT_LOCATOR);
        mailContentText.sendKeys(webDriverInstance.mailContent);
        //Check Draft folder and get ID
        String tempUrl = webDriverInstance.getDriver().getCurrentUrl();
        String tempUrlChange = tempUrl;
        int timer = 0;
        while (tempUrl.equals(tempUrlChange) && timer < 100) {
            sleep(1);
            tempUrlChange = webDriverInstance.getDriver().getCurrentUrl();
            timer++;
        }
        letterId = tempUrlChange.substring(tempUrlChange.lastIndexOf("/") + 1);
        WebElement draftFolderButton = webDriverInstance.getDriver().
                findElement(webDriverInstance.DRAFT_MAIL_FOLDER_LOCATOR);
        draftFolderButton.click();
        letterIsInDraft = webDriverInstance.getDriver().
                findElement(By.xpath("//div[@data-id = '" + letterId + "']")).isEnabled();
        //Delete and check delete folder
        WebElement draftLetter = webDriverInstance.getDriver().
                findElement(By.xpath("//div[contains(@class,'" + letterId + "')]"));
        draftLetter.click();
        sleep(1);
        WebElement deleteButton = webDriverInstance.getDriver().
                findElement(By.xpath("//a[@data-action = 'compose.delete']"));
        deleteButton.click();
        sleep(1);
        WebElement trashFolderButton = webDriverInstance.getDriver().
                findElement(webDriverInstance.TRASH_MAIL_FOLDER_LOCATOR);
        trashFolderButton.click();
        letterIsInTrash = webDriverInstance.getDriver().
                findElement(By.xpath("//div[@data-id = '" + letterId + "']")).isEnabled();
        WebElement draftLetterNew = webDriverInstance.getDriver().
                findElement(By.xpath("//div[contains(@class,'" + letterId + "')]"));
        draftLetterNew.click();
        sleep(1);
        WebElement deleteButtonNew = webDriverInstance.getDriver().
                findElement(By.xpath("//a[@data-action = 'delete']"));
        deleteButtonNew.click();
        sleep(1);
        try {
            LetterInTrash = webDriverInstance.getDriver().
                    findElement(By.xpath("//div[@data-id = '" + letterId + "']")).isEnabled();
        } catch (Exception e) {

        }
        WebElement draftFolderButtonNew = webDriverInstance.getDriver().
                findElement(webDriverInstance.DRAFT_MAIL_FOLDER_LOCATOR);
        draftFolderButtonNew.click();
        try {
            LetterInDraft = webDriverInstance.getDriver().
                    findElement(By.xpath("//div[@data-id = '" + letterId + "']")).isEnabled();
        } catch (Exception e) {

        }
        if (!LetterInTrash && !LetterInDraft) {
            letterDeleted = true;
        }
    }

    public boolean isLetterIsInDraft() {
        return letterIsInDraft;
    }

    public boolean isLetterIsInTrash() {
        return letterIsInTrash;
    }

    public boolean isLetterDeleted() {
        return letterDeleted;
    }
}
