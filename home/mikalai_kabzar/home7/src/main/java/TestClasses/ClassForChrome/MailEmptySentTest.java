package TestClasses.ClassForChrome;

import TestClasses.MailEmptySent;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by Shaman on 27.12.2015.
 */
public class MailEmptySentTest {

    @Test(description = "Chrome mail empty sent test")
    public void realizeMailFilledSentTest() {
        MailEmptySent mailSentEmpty = new MailEmptySent();
        mailSentEmpty.realizeMailEmptySent(WebDriverInstanceSetupChrome.
                getBrowser());
        boolean result = false;
        if (mailSentEmpty.isLetterIsInInbox() && mailSentEmpty.isLetterIsInSent()) {
            result = true;
        }
        Assert.assertEquals(result, true, "Mail is not exist in folder Sent and in folder Inbox" +
                " Mistake.");
    }

}
