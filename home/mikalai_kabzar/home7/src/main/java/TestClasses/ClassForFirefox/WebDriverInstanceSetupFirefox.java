package TestClasses.ClassForFirefox;

import Instance.WebDriverInstanceSetup;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

/**
 * Created by Shaman on 23.12.2015.
 */
public class WebDriverInstanceSetupFirefox extends WebDriverInstanceSetup {

    private static String browser = "Firefox";

    @BeforeClass(description = "Prepare Firefox browser")
    public void prepareBrowser() throws MalformedURLException {
        driver = new RemoteWebDriver(new URL("http://localhost:5555/wd/hub"),
                DesiredCapabilities.firefox());
        driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS,
                TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(DRIVE_IMPL_WAIT_TIMEOUT_SECONDS,
                TimeUnit.SECONDS);
    }

    @AfterClass(description = "Close Firefox browser")
    public void clearBrowser() {
        try {
            driver.quit();
        } catch (Exception e) {
            System.err.println("Problem with shutting down driver " + e.getMessage());
        } finally {
            driver = null;
        }
    }

    public static String getBrowser() {
        return browser;
    }
}
