package TestClasses.ClassForFirefox;

import TestClasses.LoginNegative;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by Shaman on 24.12.2015.
 */
public class LoginNegativeTest {

    @Test(description = "Firefox login negative test")
    public void realizeLoginNegativeTest() {
        LoginNegative loginN = new LoginNegative();
        loginN.realizeLoginNegative(WebDriverInstanceSetupFirefox.
                getBrowser());
        boolean result = false;
        if (loginN.getWebDriverInstance().getDriver().getCurrentUrl().
                indexOf("passport.yandex") != -1) {
            result = true;
        }
        Assert.assertEquals(result, true, "You should login with fakse password." +
                "password. Mistake.");
    }
}
