package TestClasses.ClassForFirefox;

import TestClasses.EnterButton;
import org.testng.annotations.Test;

/**
 * Created by Shaman on 23.12.2015.
 */
public class EnterButtonTest {

    @Test(description = "Firefox launch test")
    public void realizeEnterButtonTest() {
        EnterButton enterButton = new EnterButton();
        enterButton.realizeEnterButton(WebDriverInstanceSetupFirefox.
                getBrowser());
    }


}
