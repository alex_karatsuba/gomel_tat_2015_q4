package TestClasses;

import org.testng.annotations.*;

import java.util.Date;

/**
 * Created by Shaman on 23.12.2015.
 */
public class BaseAppTest {

    @BeforeSuite
    public void beforeSuite() {
        System.out.println("BeforeSuite test passed");
    }

    @BeforeGroups
    public void beforeGroup() {
        System.out.println("BeforeGroups test passed");
    }

    @BeforeMethod
    public void beforeMethod() {
        System.out.println("BeforeMethod test passed");
    }

    @AfterMethod
    public void afterMethod() {
        System.out.println("AfterMethod test passed");
    }

    @AfterGroups
    public void afterGroup() {
        System.out.println("AfterGroups test passed");
    }

    @AfterSuite
    public void afterSuite() {
        System.out.println("AfterSuite test passed");
    }
}
