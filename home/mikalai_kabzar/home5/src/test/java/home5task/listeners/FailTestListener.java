package home5task.listeners;

import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

/**
 * Created by Shaman on 17.12.2015.
 */
public class FailTestListener extends TestListenerAdapter {
    @Override
    public void onTestFailure(ITestResult testRes) {
        System.out.println("Error message: ");
        FileWriter fWriter = null;
        File ReportWord = new File("d:\\TAT\\Error\\FailReport.txt");
        try {
            ReportWord.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            if (!ReportWord.exists()) {
                ReportWord.createNewFile();
            }

            fWriter = new FileWriter(ReportWord, true);
            fWriter.write("\r\n Fail " + new Date(System.currentTimeMillis()));
            fWriter.write("\r\n=> TestName: " + testRes.getTestClass().getName());
            fWriter.write("\r\n=> MethodName: " + testRes.getName());
            fWriter.write("\r\n");
        } catch (IOException e) {
            System.out.println("Error message: " + e.getMessage());

        } finally {
            try {
                fWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
