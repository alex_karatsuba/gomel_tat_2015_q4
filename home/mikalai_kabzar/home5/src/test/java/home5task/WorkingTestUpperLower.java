package home5task;

import home4taskFilesForTest.WordList;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by Shaman on 22.12.2015.
 */
public class WorkingTestUpperLower extends BaseAppTest {

    @Test(groups = "d")
    public void checkSomeHelloFilesIndependentOnLowerCase() {
        sleep();
        checkTime();
        WordList WList = new WordList();
        WList.getWords(testFolder + "test3", fileMask);
        int result = WList.getValues().size();
        Assert.assertEquals(result, 25, "Wrong count");
    }

    @Test(groups = "d")
    public void checkSomeHelloFilesIndependentOnUpperCase() {
        sleep();
        checkTime();
        WordList WList = new WordList();
        WList.getWords(testFolder + "test4", fileMask);
        int result = WList.getValues().size();
        Assert.assertEquals(result, 25, "Wrong count");
    }
}
