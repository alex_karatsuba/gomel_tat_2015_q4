package home5task;

import org.testng.annotations.Factory;

/**
 * Created by Shaman on 17.12.2015.
 */
public class FactoryTest extends BaseAppTest {
    @Factory
    public Object[] newFactory() {
        return new Object[]{
                new WorkingTestUpperLower(),
                new WorkTesting(),
                new DataProviderTest()
        };
    }
}
