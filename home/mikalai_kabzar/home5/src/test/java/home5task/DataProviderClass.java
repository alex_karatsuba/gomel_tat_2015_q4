package home5task;

import org.testng.annotations.DataProvider;

/**
 * Created by Shaman on 17.12.2015.
 */
public class DataProviderClass {
    @DataProvider(name = "create")
    public static Object[][] fileNameResultForCheck() {
        return new Object[][]{
                {"test4", new Integer(25)},
                {"test5", new Integer(8)},
                {"test6", new Integer(12)}
        };
    }
}

