package com.gomel.tat.home4task;

/**
 * class HomeWork4 is main class
 *
 * @version
1.0 14 Dec 2015
 * @author
MIkalai Kabzar
 */

public class HomeWork4 {

    public static void main(String[] args) {
        WordList Engine = new WordList();
        Engine.run();
    }
}
