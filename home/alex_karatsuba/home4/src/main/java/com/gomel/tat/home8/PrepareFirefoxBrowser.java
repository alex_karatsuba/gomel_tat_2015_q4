package com.gomel.tat.home8;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;


public class PrepareFirefoxBrowser {


    public static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 30;
    public static final int DRIVER_IMPL_WAIT_TIMEOUT_SECONDS = 20;


    private WebDriver driver;

    @Test(description = "Prepare Firefox browser")
    public void prepareBrowser() {
        driver = new FirefoxDriver();
        driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(DRIVER_IMPL_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
    }
}
