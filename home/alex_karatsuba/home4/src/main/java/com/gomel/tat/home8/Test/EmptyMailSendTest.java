package com.gomel.tat.home8.Test;

import com.gomel.tat.home8.Letter.Letter;
import com.gomel.tat.home8.Page.YandexPage.ComposePage;
import com.gomel.tat.home8.Page.YandexPage.LoginPage;
import com.gomel.tat.home8.Utilities.WebDriverHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class EmptyMailSendTest {

    protected WebDriver driver;

    private String USER_LOGIN = "mr.einshtein2016@yandex.ru";
    private String USER_PASSWD = "mreintat2015";
    String ENTER_ADRESS = "";
    String ENTER_SUBJECT = "";
    String ENTER_CONTENT = "";
    public static final By ERROR_ADRESS_MESSAGE_LOCATOR = By.className("b-notification__i");

    @BeforeClass
    public void prepareBrowser() {
        driver = WebDriverHelper.getWebDriver();
    }

    @Test
    public void sendEmptyMail() {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.open();
        loginPage.login(USER_LOGIN, USER_PASSWD);
        ComposePage composePage = new ComposePage(driver);
        composePage.open();
        Letter letter = new Letter(ENTER_ADRESS, ENTER_SUBJECT, ENTER_CONTENT);
        composePage.sendLetter(letter);
        Assert.assertTrue(driver.findElement(ERROR_ADRESS_MESSAGE_LOCATOR).isDisplayed());
    }
}
