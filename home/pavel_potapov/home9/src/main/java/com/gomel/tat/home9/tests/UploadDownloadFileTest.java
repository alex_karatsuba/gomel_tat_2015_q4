package com.gomel.tat.home9.tests;

import com.gomel.tat.home9.ui.page.yandexmail.*;
import org.openqa.selenium.*;
import org.testng.Assert;
import org.testng.annotations.*;

import java.io.IOException;
import java.nio.file.*;

/**
 * Created by Pavel on 09.01.2016.
 */
public class UploadDownloadFileTest extends BaseClass {


    private BsicYandexDiskPage basicDiskPage;
    private UploadPage uploadModalWindowPage;
    private MainDiskPanelPage mainDiskPanelPage;
    private DowmloadFilesClass findDowmloadFilePC;
    private ActionDiskClass overallActions;

    @Test(description = "Upload file and download file")
    public void uploadDownload() throws InterruptedException, IOException {
        basicDiskPage = new BsicYandexDiskPage(myDriver);
        uploadModalWindowPage = new UploadPage(myDriver);
        mainDiskPanelPage = new MainDiskPanelPage(myDriver);
        findDowmloadFilePC = new DowmloadFilesClass();

        logOnMail();


        basicDiskPage.diskLinkShow();
        basicDiskPage.uploadFiles().sendKeys(uploadFrom);
        uploadModalWindowPage.uploadCheckComplete();
        uploadModalWindowPage.closeWindow();

        WebElement uploadFile = basicDiskPage.findUploadFile(UPLOAD_FILE_DISK_LOCATOR);
        uploadFile.click();
        mainDiskPanelPage.downloadFile();

        while (!Files.exists(PATH_DOWNLOAD)) {
            Thread.sleep(1000);
        }

        String expecteContentFile = new String("Hello Home Task 9 !");
        String realCcontentFile = findDowmloadFilePC.fileContent(PATH_DOWNLOAD);

        Assert.assertNotNull(uploadFile);
        Assert.assertEquals(realCcontentFile, expecteContentFile);

        System.out.println("Upload file and download file test success");
    }

    @AfterMethod(description = "Delete the test data")
    public void deletedTestData() throws IOException, InterruptedException {
        basicDiskPage = new BsicYandexDiskPage(myDriver);
        overallActions = new ActionDiskClass();

        Files.deleteIfExists(PATH_DOWNLOAD);
        WebElement uploadedFile = basicDiskPage.findUploadFile(UPLOAD_FILE_DISK_LOCATOR);
        overallActions.removePermanently(uploadedFile, myDriver, UPLOAD_FILE_TRASH_LOCATOR);

    }

}