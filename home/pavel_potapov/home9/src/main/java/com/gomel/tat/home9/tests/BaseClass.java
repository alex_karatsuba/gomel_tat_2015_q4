package com.gomel.tat.home9.tests;

import com.gomel.tat.home9.util.MyWebDriver;
import org.apache.commons.lang3.ObjectUtils;
import org.openqa.selenium.*;
import org.testng.annotations.*;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
/**
 * Created by Pavel on 09.01.2016.
 */
public class BaseClass extends BeforePrepare {
    private Path FolderUpload;
    private Path FolderDownload;
    private Path Folder;
    private Path File1;
    private Path File2;



    @BeforeSuite
    public void beforeSuite() throws IOException {
        System.out.println("Сreating a directory and files");
        if(!Files.exists(PATH_DOWNLOAD)) {
        Folder = Paths.get("D:", "TAThome9");
        FolderUpload = Folder.resolve("upload");
        FolderDownload = Folder.resolve("download");
        Files.createDirectories(FolderUpload);
        Files.createDirectories(FolderDownload);
        File1 = FolderUpload.resolve("HomeTask9.txt");
        File2 = FolderUpload.resolve("HomeTask9copy.txt");

            Files.createFile(File1);
            Files.createFile(File2);
            PrintWriter home9 = new PrintWriter("D:\\TAThome9\\upload\\HomeTask9.txt", "UTF-8");
            home9.println("Hello Home Task 9 !");
            home9.close();
            PrintWriter home9copy = new PrintWriter("D:\\TAThome9\\upload\\HomeTask9copy.txt", "UTF-8");
            home9copy.println("Hello Home Task 9 !");
            home9copy.close();
        }
    }

    @BeforeClass
    @Parameters({"webDriverName"})
    protected void SetupDriverNameClass(String webDriverName) throws MalformedURLException {
        myDriver = new MyWebDriver(webDriverName);

    }

    public void clickButtonEnter() {
        WebElement buttonSet = myDriver.getDriver().findElement(ENTER_BUTTON_LOCATOR);
        buttonSet.click();

    }

    public void logOnMail(String userLogin, String userPassword) {
        myDriver.getDriver().get(BASE_URL);

        WebElement loginSet = myDriver.waitForShow(LOGIN_INPUT_LOCATOR);
        WebElement passwordSet = myDriver.waitForShow(PASSWORD_INPUT_LOCATOR);

        loginSet.clear();
        loginSet.sendKeys(userLogin);
        passwordSet.sendKeys(userPassword);
    }

    public void wrongLogOnMail(String userPassword) {
        logOnMail(userLogin, userPassword);

    }

    public void correctLogOnMail() {
        logOnMail(userLogin, userPassword);

    }

     public void logOnMail() {
        myDriver.getDriver().get(BASE_URL);

        WebElement loginInput = myDriver.waitForShow(LOGIN_INPUT_LOCATOR);
        WebElement passwordImput = myDriver.waitForShow(PASSWORD_INPUT_LOCATOR);
        WebElement buttonEnter = myDriver.getDriver().findElement(ENTER_BUTTON_LOCATOR);

        loginInput.clear();
        loginInput.sendKeys(userLogin);
        passwordImput.sendKeys(userPassword);
        buttonEnter.click();

    }



    @AfterClass
    protected void ClearTestClass() {
        myDriver.getDriver().quit();

    }

}