package com.gomel.tat.home9.ui.page.yandexmail;

import com.gomel.tat.home9.util.MyWebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
/**
 * Created by Pavel on 09.01.2016.
 */
public class BsicYandexDiskPage extends BeforePreparePage {
    protected MyWebDriver myDriver;

    public BsicYandexDiskPage(MyWebDriver myDriver) {
        this.myDriver = myDriver;
    }

    public WebElement uploadFiles() {
        WebElement upload = myDriver.getDriver().findElement(UPLOAD_BUTTON_LOCATOR);
        return upload;
    }

    public void diskLinkShow() {
        myDriver.waitForShow(DISK_LINK_LOCATOR, 20);

    }

    public WebElement findUploadFile(By path) {
        WebElement uploadFile = myDriver.waitForShow(path, 20);
        return uploadFile;

    }

    public WebElement trash() {
        WebElement trash = myDriver.getDriver().findElement(TRASH_ICON_LOCATOR);
        return trash;
    }

    public String getIdentifUser() {
        WebElement identification = myDriver.waitForShow(USER_NAME_HEADER_LOCATOR);
        System.out.println(identification.getText());
        return identification.getText();
    }

}