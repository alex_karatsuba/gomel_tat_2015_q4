package com.gomel.tat.home9.tests;


import com.gomel.tat.home9.ui.page.yandexmail.*;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.*;
import org.testng.Assert;
import org.testng.annotations.*;

import java.io.IOException;

/**
 * Created by Pavel on 09.01.2016.
 */
public class RemoveFewFilesTest extends BaseClass {
    private BsicYandexDiskPage basicDiskPage;
    private UploadPage uploadPage;
    private MainDiskPanelPage mainDiskPanelPage;
    private ActionDiskClass overallActions;
    private TrashYandexDiskPage trashYandexDiskPage;
    private TrashDiskPanelPage trashDiskPanelPage;

    @Test(description = "Remove Several Elements")
    public void removeSeveral() {
        basicDiskPage = new BsicYandexDiskPage(myDriver);
        uploadPage = new UploadPage(myDriver);
        trashYandexDiskPage = new TrashYandexDiskPage(myDriver);
        trashDiskPanelPage = new TrashDiskPanelPage(myDriver);
        overallActions = new ActionDiskClass();
        mainDiskPanelPage = new MainDiskPanelPage(myDriver);

        logOnMail();

        basicDiskPage.diskLinkShow();
        WebElement uploadFileOne = overallActions.upload(UPLOAD_FILE_ONE_DISK_LOCATOR, uploadFromOne, myDriver);
        WebElement uploadFileTwo = overallActions.upload(UPLOAD_FILE_TWO_DISK_LOCATOR, uploadFromTwo, myDriver);

        uploadFileOne.click();
        Action severalElements = new Actions(myDriver.getDriver())
                .sendKeys(Keys.CONTROL).sendKeys(Keys.SHIFT).build();
        severalElements.perform();
        uploadFileTwo.click();
        severalElements.perform();

        mainDiskPanelPage.deleteSeveralFiles();
        overallActions.goToTrash(myDriver);

        WebElement uploadFileInTrashOne = myDriver.waitForShow(UPLOAD_FILE_ONE_TRASH_LOCATOR, 20);
        WebElement uploadFileInTrashTwo = myDriver.waitForShow(UPLOAD_FILE_TWO_TRASH_LOCATOR, 20);

        Assert.assertNotNull(uploadFileInTrashOne);
        Assert.assertNotNull(uploadFileInTrashTwo);

        System.out.println("Remove few files test success");

    }

    @AfterMethod(description = "Delete the test data")
    public void deletedTestData() throws IOException, InterruptedException {
        overallActions = new ActionDiskClass();

        overallActions.removeFromTrashPermanently(myDriver, UPLOAD_FILE_ONE_TRASH_LOCATOR);
        overallActions.removeFromTrashPermanently(myDriver, UPLOAD_FILE_TWO_TRASH_LOCATOR);

    }
}