package com.gomel.tat.home9.ui.page.yandexmail;

import com.gomel.tat.home9.util.MyWebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
/**
 * Created by Pavel on 09.01.2016.
 */
public class TrashYandexDiskPage extends BeforePreparePage {
    protected MyWebDriver myDriver;

    public TrashYandexDiskPage(MyWebDriver myDriver) {
        this.myDriver = myDriver;
    }

    public WebElement findUploadFile(By path) {
        WebElement uploadFile = myDriver.waitForShow(path, 30);
        return uploadFile;

    }

    public WebElement fileDeletedNotification() {
        WebElement deletedNotofication = myDriver.waitForShow(FILE_DELETED_NOTIFICATION_LOCATOR, 20);
        return deletedNotofication;

    }
}