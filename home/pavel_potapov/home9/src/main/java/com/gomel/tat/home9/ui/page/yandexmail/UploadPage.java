package com.gomel.tat.home9.ui.page.yandexmail;

import com.gomel.tat.home9.util.MyWebDriver;
import org.openqa.selenium.*;
/**
 * Created by Pavel on 09.01.2016.
 */
public class UploadPage extends BeforePreparePage {

    protected MyWebDriver myDriver;

    public UploadPage(MyWebDriver myDriver) {
        this.myDriver = myDriver;
    }

    public void uploadCheckComplete() {
        myDriver.waitForShow(CHECK_COMPLETE_LOCATOR, 90);
    }

    public void closeWindow() {
        WebElement closeUploadModalWindow = myDriver.waitForShow(CLOSE_BUTTON_LOCATOR);
        closeUploadModalWindow.click();
    }


}