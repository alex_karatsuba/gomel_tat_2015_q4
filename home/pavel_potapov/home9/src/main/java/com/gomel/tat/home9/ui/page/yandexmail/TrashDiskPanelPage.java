package com.gomel.tat.home9.ui.page.yandexmail;

import com.gomel.tat.home9.util.MyWebDriver;
import org.openqa.selenium.WebElement;
/**
 * Created by Pavel on 09.01.2016.
 */
public class TrashDiskPanelPage extends BeforePreparePage {
    protected MyWebDriver myDriver;

    public TrashDiskPanelPage(MyWebDriver myDriver) {
        this.myDriver = myDriver;
    }

    public void deleteFile() {
        WebElement delete = myDriver.waitForShow(DELETE_ONE_WITHOUT_PREVIEW_BUTTON_TRASH_LOCATOR, 20);
        delete.click();
    }

    public void restoreFile() {
        WebElement delete = myDriver.waitForShow(RESTORE_BUTTON_LOCATOR);
        delete.click();
    }
}