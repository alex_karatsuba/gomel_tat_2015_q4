package com.gomel.tat.home9.tests;

import com.gomel.tat.home9.ui.page.yandexmail.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;
/**
 * Created by Pavel on 09.01.2016.
 */
public class TrashFileTest extends BaseClass {
    public static final By UPLOAD_FILE_DISK_LOCATOR = By.xpath("//div[contains(@data-id,'disk/HomeTask9.txt')]");
    public static final By UPLOAD_FILE_TRASH_LOCATOR = By.xpath("//div[contains(@data-id,'trash/HomeTask9.txt')]");
    public static String uploadFrom = "D:\\TAThome9\\upload\\HomeTask9.txt";

    private BsicYandexDiskPage basicDiskPage;
    private UploadPage uploadPage;
    private TrashYandexDiskPage trashYandexDiskPage;
    private TrashDiskPanelPage trashDiskPanelPage;
    private ActionDiskClass overallActions;

    @Test(description = "Trash test")
    public void trash() throws InterruptedException, IOException {
        basicDiskPage = new BsicYandexDiskPage(myDriver);
        uploadPage = new UploadPage(myDriver);
        trashYandexDiskPage = new TrashYandexDiskPage(myDriver);
        trashDiskPanelPage = new TrashDiskPanelPage(myDriver);
        overallActions = new ActionDiskClass();

        logOnMail();

        basicDiskPage.diskLinkShow();
        WebElement uploadFile = overallActions.upload(UPLOAD_FILE_DISK_LOCATOR, uploadFrom, myDriver);

        WebElement trash = basicDiskPage.trash();
        Action actionDelete = new Actions(myDriver.getDriver()).dragAndDrop(uploadFile, trash).build();
        actionDelete.perform();
        Action goToTrash = new Actions(myDriver.getDriver()).doubleClick(trash).build();
        goToTrash.perform();

        WebElement uploadFileInTrash = trashYandexDiskPage.findUploadFile(UPLOAD_FILE_TRASH_LOCATOR);
        uploadFileInTrash.click();
        trashDiskPanelPage.deleteFile();
        WebElement notification = trashYandexDiskPage.fileDeletedNotification();

        Assert.assertNotNull(uploadFile);
        Assert.assertNotNull(uploadFileInTrash);
        Assert.assertNotNull(notification);

        System.out.println("Trash test success");

    }
}