package com.gomel.tat.home9.tests;

import com.gomel.tat.home9.ui.page.yandexmail.*;
import com.gomel.tat.home9.util.MyWebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.*;
/**
 * Created by Pavel on 09.01.2016.
 */
public class ActionDiskClass {
    private BsicYandexDiskPage basicDiskPage;
    private UploadPage uploadPage;
    private TrashYandexDiskPage trashYandexDiskPage;
    private TrashDiskPanelPage trashDiskPanelPage;

    public WebElement upload(By bypath, String path, MyWebDriver webDriver) {
        basicDiskPage = new BsicYandexDiskPage(webDriver);
        uploadPage = new UploadPage(webDriver);

        basicDiskPage.uploadFiles().sendKeys(path);
        uploadPage.uploadCheckComplete();
        uploadPage.closeWindow();
        WebElement uploadFile = basicDiskPage.findUploadFile(bypath);
        return uploadFile;
    }

    public void goToTrash(MyWebDriver webDriver) {
        basicDiskPage = new BsicYandexDiskPage(webDriver);
        WebElement trash = basicDiskPage.trash();
        Action goToTrash = new Actions(webDriver.getDriver()).doubleClick(trash).build();
        goToTrash.perform();

    }

    public void removeToTrash(WebElement uploadFile, MyWebDriver webDriver) {
        basicDiskPage = new BsicYandexDiskPage(webDriver);
        WebElement trash = basicDiskPage.trash();
        Action actionDelete = new Actions(webDriver.getDriver()).dragAndDrop(uploadFile, trash).build();
        actionDelete.perform();

    }

    public WebElement removePermanently(WebElement uploadFile, MyWebDriver webDriver, By bypath) throws InterruptedException {
        trashYandexDiskPage = new TrashYandexDiskPage(webDriver);
        trashDiskPanelPage = new TrashDiskPanelPage(webDriver);

        removeToTrash(uploadFile, webDriver);
        goToTrash(webDriver);

        WebElement uploadFileInTrash = trashYandexDiskPage.findUploadFile(bypath);
        uploadFileInTrash.click();

        trashDiskPanelPage.deleteFile();
        WebElement notification = trashYandexDiskPage.fileDeletedNotification();
        return notification;

    }

    public void removeFromTrashPermanently(MyWebDriver webDriver, By bypath) throws InterruptedException {
        trashYandexDiskPage = new TrashYandexDiskPage(webDriver);
        trashDiskPanelPage = new TrashDiskPanelPage(webDriver);

        WebElement uploadFileInTrash = trashYandexDiskPage.findUploadFile(bypath);
        uploadFileInTrash.click();

        trashDiskPanelPage.deleteFile();
        trashYandexDiskPage.fileDeletedNotification();

    }

}