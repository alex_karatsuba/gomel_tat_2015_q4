package com.gomel.tat.home9.util;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.*;
import org.openqa.selenium.support.ui.*;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class MyWebDriver {

    public static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 20;

    private WebDriver driver;
    private String driverName;
    Actions action;

    public WebDriver getDriver() {
        return driver;
    }

    public WebElement waitForDisappear(By locator, long timeoutInSeconds) {
        new WebDriverWait(driver, timeoutInSeconds)
                .until(ExpectedConditions.invisibilityOfElementLocated(locator));
        return driver.findElement(locator);
    }

    public WebElement waitForShow(By locator) {
        return waitForShow(locator, 7);
    }

    public WebElement waitForShow(By locator, long timeoutInSeconds) {
        new WebDriverWait(driver, timeoutInSeconds)
                .until(ExpectedConditions.visibilityOfElementLocated(locator));
        return driver.findElement(locator);
    }

    public WebElement waitForElementIsClickable(By locator) {
        new WebDriverWait(driver, 5000).until(ExpectedConditions.elementToBeClickable(locator));
        return driver.findElement(locator);
    }

    public MyWebDriver(String webDriverName) throws MalformedURLException {
        driverName = webDriverName;

        if (webDriverName.equalsIgnoreCase("firefox")) {
            FirefoxProfile profile = new FirefoxProfile();
            profile.setPreference("browser.download.folderList", 2);
            profile.setPreference("browser.download.dir", "D:\\TAThome9\\download");
            profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "text/php, text/plain, text/txt");
            DesiredCapabilities dcFirefox = DesiredCapabilities.firefox();
            dcFirefox.setCapability(FirefoxDriver.PROFILE, profile);
            driver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), dcFirefox);

        } else if (webDriverName.equalsIgnoreCase("chrome")) {
            ChromeOptions options = new ChromeOptions();
            String downloadFilepath = "D:\\TAThome9\\download";
            HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
            chromePrefs.put("download.default_directory", downloadFilepath);
            options.setExperimentalOption("prefs", chromePrefs);
            DesiredCapabilities dcChrome = DesiredCapabilities.chrome();
            dcChrome.setCapability(ChromeOptions.CAPABILITY, options);
            driver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), dcChrome);
        }

        driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        action = new Actions(driver);
    }

}