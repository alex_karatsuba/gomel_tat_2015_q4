package com.gomel.tat.home9.tests;

import org.openqa.selenium.*;
import org.testng.Assert;
import org.testng.annotations.Test;
/**
 * Created by Pavel on 27.12.2015.
 */
public class LoginTestNegativ extends BaseClass {

    @Test(description = "Negativ Test Yandex Disk login")
    public void loginMailNegativ(){
        wrongLogOnMail("12345678asd");
        clickButtonEnter();
        myDriver.waitForShow(ERROR_MESSAGE_LOCATOR);
        WebElement errorMessage = myDriver.getDriver().findElement(ERROR_MESSAGE_LOCATOR);
        Assert.assertNotNull(errorMessage);
        System.out.println("Success test negativ Yandex Disk login");
    }
}
