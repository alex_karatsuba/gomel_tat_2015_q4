package com.gomel.tat.home9.tests;

import com.gomel.tat.home9.util.MyWebDriver;

import org.openqa.selenium.By;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by Pavel on 09.01.2016.
 */
public class BeforePrepare {


    /* BaseClass */
    public static final String BASE_URL = "https://disk.yandex.ru/";
    public static final By ENTER_BUTTON_LOCATOR = By.xpath("//*[@type='submit']");
    public static final By LOGIN_INPUT_LOCATOR = By.xpath("//input[@name='login']");
    public static final By PASSWORD_INPUT_LOCATOR = By.xpath("//input[@name='password']");

    protected String userLogin = "tat-test-user@yandex.ru";
    protected String userPassword = "tat-123qwe";

    public static final By DISK_LINK_LOCATOR = By.xpath("//a[contains(@class,'item_disk')]");
    public static final By CLOSE_NOTIFICATION_LOCATOR = By.xpath("//a[contains(@data-metrika-params,'x-closed')]");
    protected MyWebDriver myDriver;


    /* LoginTestNegativ */
    public static final By ERROR_MESSAGE_LOCATOR = By.xpath("//*[@id='nb-input_error1']");


    /* RemoveFewFilesTest */
    public static final By UPLOAD_FILE_ONE_DISK_LOCATOR = By.xpath("//div[contains(@data-id,'disk/HomeTask9.txt')]");
    public static final By UPLOAD_FILE_TWO_DISK_LOCATOR = By.xpath("//div[contains(@data-id,'disk/HomeTask9copy.txt')]");
    public static final By UPLOAD_FILE_ONE_TRASH_LOCATOR = By.xpath("//div[contains(@data-id,'trash/HomeTask9.txt')]");
    public static final By UPLOAD_FILE_TWO_TRASH_LOCATOR = By.xpath("//div[contains(@data-id,'trash/HomeTask9copy.txt')]");
    public static String uploadFromOne = "D:\\TAThome9\\upload\\HomeTask9.txt";
    public static String uploadFromTwo = "D:\\TAThome9\\upload\\HomeTask9copy.txt";


    /* RestoreFileTest */
    public static final By UPLOAD_FILE_DISK_LOCATOR = By.xpath("//div[contains(@data-id,'disk/HomeTask9.txt')]");
    public static final By UPLOAD_FILE_TRASH_LOCATOR = By.xpath("//div[contains(@data-id,'trash/HomeTask9.txt')]");
    public static String uploadFrom = "D:\\TAThome9\\upload\\HomeTask9.txt";

    /* UploadDownloadFileTest */
//    public static final By UPLOAD_FILE_DISK_LOCATOR = By.xpath("//div[contains(@data-id,'disk/HomeTask9.txt')]");
//    public static final By UPLOAD_FILE_TRASH_LOCATOR = By.xpath("//div[contains(@data-id,'trash/HomeTask9.txt')]");
    public static final Path PATH_DOWNLOAD = Paths.get("D:\\TAThome9\\upload\\HomeTask9.txt");
//    public static String uploadFrom = "D:\\TAThome9\\upload\\HomeTask9.txt";
}
