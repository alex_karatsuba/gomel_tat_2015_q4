package com.gomel.tat.home9.ui.page.yandexmail;

import com.gomel.tat.home9.util.MyWebDriver;
import org.openqa.selenium.*;
/**
 * Created by Pavel on 09.01.2016.
 */
public class MainDiskPanelPage extends BeforePreparePage {

    protected MyWebDriver myDriver;

    public MainDiskPanelPage(MyWebDriver myDriver) {
        this.myDriver = myDriver;
    }

    public void downloadFile() {
        WebElement download = myDriver.waitForShow(DOWNLOAD_ONE_WITHOUT_PREVIEW_LOCATOR);
        download.click();
    }

    public void deleteSeveralFiles() {
        WebElement delete = myDriver.waitForShow(DELETE_SEVERAL_FILES_BUTTON_LOCATOR);
        delete.click();
    }

    public void deleteFile() {
        WebElement delete = myDriver.waitForShow(DELETE_ONE_WITHOUT_PREVIEW_BUTTON_LOCATOR);
        delete.click();
    }

    public void emptyTrash() throws InterruptedException {
        WebElement empty = myDriver.waitForElementIsClickable(CLEAR_TRASH_BUTTON_LOCATOR);
        empty.click();
    }
}