package com.gomel.tat.home9.tests;


import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.nio.file.Path;
import java.util.List;

/**
 * Created by Pavel on 09.01.2016.
 */
public class DowmloadFilesClass {

    public String fileContent(Path path) throws IOException {
        Charset charset = Charset.forName("UTF-8");
        List<String> content = Files.readAllLines(path, charset);
        StringBuilder sbuilder = new StringBuilder();
        for (String s : content) {
            sbuilder.append(s);
        }
        return sbuilder.toString();

    }
}