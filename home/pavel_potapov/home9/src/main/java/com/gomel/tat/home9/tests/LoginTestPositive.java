package com.gomel.tat.home9.tests;

import com.gomel.tat.home9.ui.page.yandexmail.BsicYandexDiskPage;
import org.testng.Assert;
import org.testng.annotations.Test;
/**
 * Created by Pavel on 27.12.2015.
 */
public class LoginTestPositive extends BaseClass {
    private BsicYandexDiskPage basicDiskPage;

    @Test(description = "Positive test Yandex Disk login")
    public void loginMailPositive() {
        basicDiskPage = new BsicYandexDiskPage(myDriver);
        correctLogOnMail();
        clickButtonEnter();
        Assert.assertEquals(basicDiskPage.getIdentifUser(), userLogin.substring(0,userLogin.length()-10));
        System.out.println("Success test positive Yandex Disk login");
    }

}
