package com.gomel.tat.home9.tests;


import com.gomel.tat.home9.ui.page.yandexmail.BsicYandexDiskPage;
import com.gomel.tat.home9.ui.page.yandexmail.TrashYandexDiskPage;
import com.gomel.tat.home9.ui.page.yandexmail.TrashDiskPanelPage;
import com.gomel.tat.home9.ui.page.yandexmail.UploadPage;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import java.io.IOException;


/**
 * Created by Pavel on 09.01.2016.
 */
public class RestoreFileTest extends BaseClass {


    private BsicYandexDiskPage basicDiskPage;
    private UploadPage uploadPage;
    private TrashYandexDiskPage trashYandexDiskPage;
    private TrashDiskPanelPage trashDiskPanelPage;
    private ActionDiskClass overallActions;

    @Test(description = "Restore test")
    public void restore() throws InterruptedException, IOException {
        basicDiskPage = new BsicYandexDiskPage(myDriver);
        uploadPage = new UploadPage(myDriver);
        trashYandexDiskPage = new TrashYandexDiskPage(myDriver);
        trashDiskPanelPage = new TrashDiskPanelPage(myDriver);
        overallActions = new ActionDiskClass();

        logOnMail();

        basicDiskPage.diskLinkShow();
        WebElement uploadFile = overallActions.upload(UPLOAD_FILE_DISK_LOCATOR, uploadFrom, myDriver);
        overallActions.removeToTrash(uploadFile, myDriver);
        overallActions.goToTrash(myDriver);
        WebElement uploadFileInTrash = trashYandexDiskPage.findUploadFile(UPLOAD_FILE_TRASH_LOCATOR);

        uploadFileInTrash.click();
        trashDiskPanelPage.restoreFile();

        myDriver.getDriver().navigate().back();
        WebElement restoreFile = basicDiskPage.findUploadFile(UPLOAD_FILE_DISK_LOCATOR);

        Assert.assertNotNull(uploadFile);
        Assert.assertNotNull(uploadFileInTrash);
        Assert.assertNotNull(restoreFile);

        System.out.println("Restore test success");

    }

    @AfterMethod(description = "Delete the test data")
    public void deletedTestData() throws IOException, InterruptedException {
        basicDiskPage = new BsicYandexDiskPage(myDriver);
        overallActions = new ActionDiskClass();

        WebElement uploadedFile = basicDiskPage.findUploadFile(UPLOAD_FILE_DISK_LOCATOR);
        overallActions.removePermanently(uploadedFile, myDriver, UPLOAD_FILE_TRASH_LOCATOR);
    }
}