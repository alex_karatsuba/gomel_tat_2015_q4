package com.gomel.tat.home9.ui.page.yandexmail;

import org.openqa.selenium.By;

/**
 * Created by Pavel on 11.01.2016.
 */
public class BeforePreparePage {
    /* BsicYandexDiskPage */
    public static final By UPLOAD_BUTTON_LOCATOR = By.xpath("//div[@class='header']//input[@type='file']");
    public static final By DISK_LINK_LOCATOR = By.xpath("//div[@class='header__side-left']/a[2]");
    public static final By TRASH_ICON_LOCATOR = By.xpath("//div[@data-id='/trash']/div");

    protected By USER_NAME_HEADER_LOCATOR = By.xpath("//span[@class='header__username']");

    /* MainDiskPanelPage */
    public static final By DOWNLOAD_ONE_WITHOUT_PREVIEW_LOCATOR = By.xpath("//div[@data-key='box=boxAside']//button[@data-click-action='resource.download']");
    public static final By DELETE_SEVERAL_FILES_BUTTON_LOCATOR = By.xpath("//div[@data-key='box=boxAside']/div[contains(@class,'visible')]//button[contains(@data-metrika-params,'delete')]");
    public static final By DELETE_ONE_WITHOUT_PREVIEW_BUTTON_LOCATOR = By.xpath("//div[@data-key='box=boxAside']/div[contains(@class,'visible')]//button[@data-click-action='resource.delete']");
    public static final By CLEAR_TRASH_BUTTON_LOCATOR = By.xpath("//button[@data-click-action='trash.clean']");

    /* TrashDiskPanelPage */
    public static final By DELETE_ONE_WITHOUT_PREVIEW_BUTTON_TRASH_LOCATOR = By.xpath("//div[@data-key='box=boxAside']//div[contains(@class,'visible')]//button[@data-click-action='resource.delete' and contains(@data-params,'trash')]");
    public static final By RESTORE_BUTTON_LOCATOR = By.xpath("//div[@data-key='box=boxAside']//button[@data-click-action='resource.restore' and contains(@data-params,'trash')]");

    /* TrashYandexDiskPage */
    public static final By FILE_DELETED_NOTIFICATION_LOCATOR = By.xpath("//div[@data-key='box=layerWdg']//div[contains(@class,'item_moved')]");

    /* UploadPage */
    public static final By CHECK_COMPLETE_LOCATOR = By.xpath("//div[contains(@class,'icon_done')]");
    public static final By CLOSE_BUTTON_LOCATOR = By.xpath("//div[@class='b-dialog-upload__actions']/button[4]");

}
