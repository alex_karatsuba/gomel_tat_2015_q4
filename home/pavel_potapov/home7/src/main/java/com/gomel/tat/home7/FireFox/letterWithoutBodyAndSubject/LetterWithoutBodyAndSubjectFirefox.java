package com.gomel.tat.home7.FireFox.letterWithoutBodyAndSubject;

import com.gomel.tat.home7.BeforePrepareBrowserFirefox;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;


/**
 * Created by Pavel on 24.12.2015.
 */
public class LetterWithoutBodyAndSubjectFirefox extends BeforePrepareBrowserFirefox {


    @Test(groups = "LetterWithoutBodyAndSubject", description = "Success mail without subject and body is present in Inbox folders")
    public void WithoutSubjectBodyTest() {
        driver.get(BASE_URL);
        WebElement enterButton = driver.findElement(ENTER_BUTTON_LOCATOR);
        enterButton.click();
        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(userLogin);
        WebElement passInput = driver.findElement(PASSWORD_INPUT_LOCATOR);
        passInput.sendKeys(userPassword);
        passInput.submit();
        WebElement composeButton = driver.findElement(COMPOSE_BUTTON_LOCATOR);
        composeButton.click();
        WebElement toInput = driver.findElement(TO_INPUT_LOCATOR);
        toInput.sendKeys(mailTo);


        WebElement sendMailButton = driver.findElement(SEND_MAIL_BUTTON_LOCATOR);
        sendMailButton.click();

        waitForDisappear(SEND_MAIL_BUTTON_LOCATOR);

        String sentId = getIdMail();

        driver.findElement(INPOX_LINK_LOCATOR).click();
        boolean isMailInInbox = driver.findElement(By.xpath(String.format(MAIL_DATA_ID, sentId))).isDisplayed();

        sentId.substring(0, sentId.length() - 2);
        driver.findElement(OUTPOX_LOCATOR).click();
        boolean isMailInSent = driver.findElement(By.xpath(String.format(MAIL_DATA_ID, sentId))).isDisplayed();

        Assert.assertTrue(isMailInInbox && isMailInSent);
        System.out.println("Success mail without subject and body is present in Inbox folders");
    }

    private String getIdMail(){
        String idLetter = driver.findElement(By.className("b-statusline__link")).getAttribute("href");
        int numberLastSlash = idLetter.lastIndexOf("/");
        idLetter = idLetter.substring(numberLastSlash+1);
        return idLetter;
    }

}
