package com.gomel.tat.home7.Chrome.letterWithoutAddress;


import com.gomel.tat.home7.BeforePrepareBrowserChrome;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LetterWrongAddressChrome extends BeforePrepareBrowserChrome {

    @Test(groups = "letterWithoutAddress", description = "Success check the appearance wrong adresses letter")
    public void letterWithoutAddress() {

        driver.get(BASE_URL);
        WebElement enterButton = driver.findElement(ENTER_BUTTON_LOCATOR);
        enterButton.click();
        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(userLogin);
        WebElement passInput = driver.findElement(PASSWORD_INPUT_LOCATOR);
        passInput.sendKeys(userPassword);
        passInput.submit();

        WebElement composeButton = driver.findElement(COMPOSE_BUTTON_LOCATOR);
        composeButton.click();
        WebElement subjectInput = driver.findElement(SUBJECT_INPUT_LOCATOR);
        subjectInput.sendKeys(mailSubject);
        WebElement mailContentText = driver.findElement(MAIL_TEXT_LOCATOR);
        mailContentText.sendKeys(mailContent);

        WebElement toInput = driver.findElement(TO_INPUT_LOCATOR);
        toInput.sendKeys(wrongUserMail);

        WebElement sendMailButton = driver.findElement(SEND_MAIL_BUTTON_LOCATOR);
        sendMailButton.click();

        WebElement searchError = driver.findElement(ERROR_WRONG_ADDRESSES_LETTER);
        Assert.assertTrue(searchError.isDisplayed());

        System.out.println(" -- Success check the appearance wrong adresses letter");
    }

}
