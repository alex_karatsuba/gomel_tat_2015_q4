package com.gomel.tat.home5.test.testng;

import org.testng.Assert;
import org.testng.annotations.*;

public class FilesToStringTest extends GetFileListTest {
    @Test(groups = "get words", dependsOnMethods = "getFileList")
    public void filesToString() {
        String files_content = wordList.filesToString(files);
        String expectedFilesContent =

                "Pavel Potapov\r\n" +
                        "\r\n" +
                        " Terry Pratchett \"Discworld\"\r\n" +
                        " Duglas Adams \"The Hitchhiker's Guide to the Galaxy\"\r\n" +
                        " Jonathan Maberry \"Фабрика драконов\" и \"Вирус\"\r\n" +
                        " Hasekura Isuna \"Volchica i prjanosti\"\r\n" +
                        " Eoin Colfer \"Artemis Faul\" ";
        Assert.assertEquals(files_content, expectedFilesContent, "Invalid result array");
    }
}
