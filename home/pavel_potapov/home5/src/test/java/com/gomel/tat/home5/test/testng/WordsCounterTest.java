package com.gomel.tat.home5.test.testng;

import org.testng.Assert;
import org.testng.annotations.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


public class WordsCounterTest extends BaseWordListTest {
    private List<String> values = new ArrayList<String>();
    private HashMap<String, Integer> expectedLibrary;

    @Factory(dataProvider = "dataProviderWordsCounter")
    public WordsCounterTest(List<String> values, HashMap<String, Integer> expectedLibrary) {
        this.values = values;
        this.expectedLibrary = expectedLibrary;
    }

    @Test
    public void wordsCounter() {
        HashMap<String, Integer> library = wordList.wordsCounter(values);
        Assert.assertEquals(library, this.expectedLibrary, "Invalid result");
    }

    @DataProvider(name = "dataProviderWordsCounter")
    public static Object[][] dataProvider() {
        List<String> values = Arrays.asList("konstantsin", "simanenka", "pc", "for", "dummies");
        HashMap<String, Integer> expectedLibrary = new HashMap<String, Integer>();
        expectedLibrary.put("konstantsin", 1);
        expectedLibrary.put("simanenka", 1);
        expectedLibrary.put("pc", 1);
        expectedLibrary.put("for", 1);
        expectedLibrary.put("dummies", 1);
        return new Object[][]{
                {values, expectedLibrary},
        };
    }
}
