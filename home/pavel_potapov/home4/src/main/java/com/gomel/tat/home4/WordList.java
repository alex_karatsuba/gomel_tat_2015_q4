package com.gomel.tat.home4;


import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;


public class WordList {

    private List<String> values = new ArrayList<String>();
    Map<String, Integer> originalWords = new TreeMap<String, Integer>();

    public void getWords(String baseDirectory, String fileNamePattern) {
        final PathMatcher helloTxtMatcher = FileSystems.getDefault().getPathMatcher(fileNamePattern);
        final String ENCOD = "Cp1251";
        final String SPLIT = "[^а-яa-z]";

        try {
            Files.walkFileTree(Paths.get(baseDirectory), new SimpleFileVisitor<Path>() {

                public FileVisitResult visitFile(Path path, BasicFileAttributes attributes) {
                    if (!attributes.isDirectory() && helloTxtMatcher.matches(path.getFileName())) {

                        String PATH_ABSOLUTE = path.toAbsolutePath().toString();

                        BufferedReader br;
                        try {
                            br = new BufferedReader(new InputStreamReader(new FileInputStream(PATH_ABSOLUTE), ENCOD));

                            String STR;
                            while ((STR = br.readLine()) != null) {

                                STR = STR.toLowerCase();
                                String[] kit_words = STR.split(SPLIT);

                                for (String ONE_WORDS : kit_words) {
                                    if (!ONE_WORDS.isEmpty()) {
                                        values.add(ONE_WORDS);
                                    }
                                }
                            }


                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }

                    return FileVisitResult.CONTINUE;
                }
            });
        } catch (IOException exc) {
            exc.printStackTrace();
        }

        for (String WD : values) {
            int i = originalWords.containsKey(WD) ? originalWords.get(WD) : 0;
            originalWords.put(WD, ++i);
        }
    }

    public void printStatistics() {

        final String DISPLAY_FORMAT = "%15s  - %s";
        System.out.println(String.format(DISPLAY_FORMAT, "Words", "Quantity"));

        for (String word : originalWords.keySet()) {
            System.out.println(String.format(DISPLAY_FORMAT, word, originalWords.get(word)));
        }
    }


}
