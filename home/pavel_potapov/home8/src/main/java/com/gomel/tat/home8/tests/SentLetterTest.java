package com.gomel.tat.home8.tests;

import com.gomel.tat.home8.ui.page.BeforePreparePage;
import com.gomel.tat.home8.ui.page.yandexmail.FindSelectLetterPage;
import com.gomel.tat.home8.ui.page.yandexmail.LatterPage;
import org.openqa.selenium.*;
import org.testng.Assert;
import org.testng.annotations.*;
/**
 * Created by Pavel on 27.12.2015.
 */
public class SentLetterTest extends BaseClass {


    @BeforeClass
    public void loginMail() {
        goToMailBox();
    }

    @BeforeMethod
    public void gotoInboxMailPage() {
        myAction.gotoInbox();
    }

    @Test(description = "Success send message", dataProvider = "FillMailLetter")
    public void mailTest(String mailTo, String mailSubject, String mailContent) {
        LatterPage newMail = new LatterPage(myDriver);
        FindSelectLetterPage mailBox = new FindSelectLetterPage(myDriver);

        myAction.clickButtonCompose();
        newMail.fillallFieldsLetters(mailTo, mailSubject, mailContent);
        newMail.clickSendMailButton();

        myDriver.waitForShow(ANNOUNCEMENT_MAIL_SEND_LOCATOR);
        String mailId = newMail.getLetterIdfromAnnouncement(ANNOUNCEMENT_MAIL_SEND_LOCATOR);

        myAction.gotoSent();
        WebElement existOutcomingLetter = mailBox.findMailSent(mailId);
        myAction.gotoInbox();
        WebElement existInboxLetter = mailBox.findMail(mailId);

        Assert.assertNotNull(existOutcomingLetter);
        Assert.assertNotNull(existInboxLetter);
        System.out.println("Success test send message");

    }

    @DataProvider(name = "FillMailLetter")
    public Object[][] valuesLetterForMap() {
        return new Object[][]{
                {BeforePreparePage.mailTo, BeforePreparePage.mailSubject, BeforePreparePage.mailContent},
                {BeforePreparePage.mailTo, "", ""}
        };
    }

}
