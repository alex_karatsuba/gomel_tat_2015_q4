package com.gomel.tat.home8.ui.page.yandexmail;

import com.gomel.tat.home8.ui.page.BeforePreparePage;
import com.gomel.tat.home8.util.MyWebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
/**
 * Created by Pavel on 27.12.2015.
 */
public class FindSelectLetterPage extends BeforePreparePage {

    public FindSelectLetterPage(MyWebDriver myDriver) {
        this.myDriver = myDriver;
    }

    public WebElement findMail(String mailId) {
        WebElement findMailInbox =  myDriver.getDriver().findElement(By.xpath(String.format(FIND_LETTER_ID, mailId)));
        return findMailInbox;
    }

    public WebElement findMailSent(String mailId) {
        mailId = mailId.substring(0, mailId.length() - 2);
//        System.out.println(mailId);
        WebElement findMailSentbox =  myDriver.getDriver().findElement(By.xpath(String.format(FIND_LETTER_ID, mailId)));
        return findMailSentbox;
    }

    public void selectMail (String mailId){
        WebElement findMailDraft = myDriver.waitForShow(By.xpath(String.format(FIND_LETTER_ID_SELECT, mailId)));
        findMailDraft.click();

    }

}
