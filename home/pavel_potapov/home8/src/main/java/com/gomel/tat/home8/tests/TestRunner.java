package com.gomel.tat.home8.tests;

import org.testng.TestNG;
import org.testng.xml.XmlSuite;
import java.util.ArrayList;
import java.util.List;
/**
 * Created by Pavel on 27.12.2015.
 */
public class TestRunner {
    public static void main(String[] args) {

        TestNG tng = new TestNG();

        XmlSuite suite = new XmlSuite();
        suite.setName("WebDriverCheck");
        List<String> files = new ArrayList();
        files.addAll(new ArrayList<String>() {{
            add("./src/main/resources/suites/WebDrivers.xml");
        }});
        suite.setSuiteFiles(files);

        List<XmlSuite> suites = new ArrayList<XmlSuite>();
        suites.add(suite);
        tng.setXmlSuites(suites);
        tng.run();
    }
}
