package com.gomel.tat.home8.tests;

import org.openqa.selenium.*;
import org.testng.Assert;
import org.testng.annotations.Test;
/**
 * Created by Pavel on 27.12.2015.
 */
public class LoginTestNegativ extends BaseClass {

    @Test(description = "Negativ Test mail login")
    public void loginMailNegative(){
        wrongLogOnMail("12345678910");
        clickButtonEnter();
        WebElement errorMessage = myDriver.getDriver().findElement(ERROR_MESSAGE_LOCATOR);
        Assert.assertNotNull(errorMessage);
        System.out.println("Success test negativ mail login");
    }
}
