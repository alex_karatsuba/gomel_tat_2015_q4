package com.gomel.tat.home8.tests;

import com.gomel.tat.home8.ui.page.yandexmail.ActionPage;
import com.gomel.tat.home8.util.MyWebDriver;

import org.openqa.selenium.By;

/**
 * Created by Pavel on 27.12.2015.
 */
public class BeforePrepare {

    protected MyWebDriver myDriver;
    protected ActionPage myAction;


    /* BaseClass */
    public static final String BASE_URL = "https://mail.yandex.by/";
    public static final By ENTER_BUTTON_LOCATOR = By.xpath("//form[@method='POST']/div[4]/span/button");
    public static final By LOGIN_INPUT_LOCATOR = By.xpath("//label[@id='nb-1']//input");
    public static final By PASSWORD_INPUT_LOCATOR = By.xpath("//label[@id='nb-2']//input");

    protected String userLogin = "tat-test-user@yandex.ru";
    protected String userPassword = "tat-123qwe";


    /* SendLetterTest */
    public static final By ANNOUNCEMENT_MAIL_SEND_LOCATOR = By.xpath("//a[@class='b-statusline__link']");


    /* LoginTestNegativ */
    public static final By ERROR_MESSAGE_LOCATOR = By.className("error-msg");


    /* DraftMailTest */
    public static final By ANNOUNCEMENT_DRAFT_MAIL_CREATE_LOCATOR = By.xpath("//div[@data-compose-type='letter postcard']//span[contains(@data-action,'save-dropdown')]");
    public static final By ANNOUNCEMENT_MAIL_DELETED_LOCATOR = By.xpath("//div[@class ='b-statusline']");


    /*  */



    /*  */
}
