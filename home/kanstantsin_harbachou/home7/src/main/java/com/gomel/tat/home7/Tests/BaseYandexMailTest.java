package com.gomel.tat.home7.Tests;

import com.gomel.tat.home7.Locators.YandexMailLocator;
import com.gomel.tat.home7.Utils.InitialData;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.BeforeClass;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class BaseYandexMailTest {

    protected WebDriver driver;
    private List<WebElement> listForClear;
    protected static final int WAIT_ELEMENT_TIMEOUT = 5;

    public void waitForElementPresent(By locator) {
        new WebDriverWait(driver, WAIT_ELEMENT_TIMEOUT).until(ExpectedConditions.presenceOfAllElementsLocatedBy(locator));
    }

    @BeforeClass(description = "Preparing browser")
    public void preparingBrowser() throws MalformedURLException {
        if (InitialData.USING_BROWSER == "googlechrome") {
            driver = new RemoteWebDriver(new URL(InitialData.REMOTE_DRIVER_URL), DesiredCapabilities.chrome());
            driver.manage().window().maximize();
            driver.get(InitialData.YANDEX_START_PAGE);
        } else {
            driver = new RemoteWebDriver(new URL(InitialData.REMOTE_DRIVER_URL), DesiredCapabilities.firefox());
            driver.get(InitialData.YANDEX_START_PAGE);
        }
    }

    @AfterClass(description = "Finishing browser")
    public void finishingBrowser() {
        driver.quit();
    }

    @AfterGroups(groups = "letter group")
    public void afterGroup() {
        driver.findElement(YandexMailLocator.INCOMING_LINK_LOCATOR).click();
        driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        listForClear = driver.findElements(By.cssSelector(".b-messages-head__title input"));
        listForClear.get(0).click();
        driver.findElement(YandexMailLocator.DELETE_ACTION_LOCATOR).click();
        driver.findElement(YandexMailLocator.CLEAR_TRASH_LOCATOR).click();
    }
}
