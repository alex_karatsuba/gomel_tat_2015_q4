package com.gomel.tat.home7.Tests;

import com.gomel.tat.home7.Locators.YandexMailLocator;
import com.gomel.tat.home7.Utils.InitialData;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class SendLetterOnlyWithAddressTest extends LoginPositiveTest {

    boolean isLetterInInbox, isLetterInOutbox = false;

    @Test(description = "Checking sending of full letter", groups = "letter group")
    public void checkSendLetterWithAdress() throws InterruptedException {
        waitForElementPresent(YandexMailLocator.WRITE_NEW_MAIL_LOCATOR);
        driver.findElement(YandexMailLocator.WRITE_NEW_MAIL_LOCATOR).click();
        waitForElementPresent(YandexMailLocator.MAIL_ADDRESS_LOCATOR);
        driver.findElement(YandexMailLocator.MAIL_ADDRESS_LOCATOR).sendKeys(InitialData.LOGIN_YANDEX_MAIL + "@yandex.ru");
        String subject = "(Без темы)";
        waitForElementPresent(YandexMailLocator.MAIL_SUBMIT_LOCATOR);
        driver.findElement(YandexMailLocator.MAIL_SUBMIT_LOCATOR).click();
        driver.findElement(YandexMailLocator.INCOMING_LINK_LOCATOR).click();
        driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        String letterLocatorPattern = "span[title='" + subject + "']";
        isLetterInInbox = driver.findElement(By.cssSelector(letterLocatorPattern)).isEnabled();
        driver.findElement(YandexMailLocator.OUTCOMING_LINK_LOCATOR).click();
        driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        isLetterInOutbox = driver.findElement(By.cssSelector(letterLocatorPattern)).isEnabled();

        Assert.assertTrue(isLetterInInbox && isLetterInOutbox);
    }
}
