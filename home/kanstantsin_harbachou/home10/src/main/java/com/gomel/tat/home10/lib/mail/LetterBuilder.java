package com.gomel.tat.home10.lib.mail;

import org.apache.commons.lang3.RandomStringUtils;
import static com.gomel.tat.home10.lib.common.CommonConstants.DEFAULT_MAIL_TO_SEND;

public class LetterBuilder {

    public static Letter getLetter() {
        Letter letter = new Letter();
        letter.setAddress(DEFAULT_MAIL_TO_SEND);
        letter.setSubject(RandomStringUtils.randomAlphabetic(10));
        letter.setPost(RandomStringUtils.randomAlphabetic(100));
        return letter;
    }

}