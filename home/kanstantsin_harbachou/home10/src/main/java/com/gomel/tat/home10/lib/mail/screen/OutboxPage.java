package com.gomel.tat.home10.lib.mail.screen;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import static com.gomel.tat.home10.lib.common.service.WebDriverHelper.waitForElementIsClickable;

public class OutboxPage {

    @FindBy(css = "label.b-messages-head__title input")
    WebElement selectAllLetter;

    @FindBy(css = "[data-action=delete]")
    WebElement deleteSelected;

    @FindBy(css = "a[href$='#sent']")
    WebElement outboxPageLink;

    @FindBy(className = "b-folders__folder__counters__total")
    WebElement itemCounter;

    public void visitOutboxPage(WebDriver driver) {
       if(itemCounter.isEnabled())
           waitForElementIsClickable(outboxPageLink, driver).click();
    }

    public boolean isOutboxFull(WebDriver driver) {
           return waitForElementIsClickable(selectAllLetter, driver).isEnabled();
    }

    public void selectAll(WebDriver driver) {
        waitForElementIsClickable(selectAllLetter, driver).click();
    }

    public void deleteAll(WebDriver driver) {
       waitForElementIsClickable(deleteSelected, driver).click();
    }
}
