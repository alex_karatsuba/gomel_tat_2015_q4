package com.gomel.tat.home10.lib.mail.screen;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage {

    @FindBy(name = "login")
    WebElement loginInput;

    @FindBy(name = "passwd")
    WebElement passwordInput;

    @FindBy(css = ".error-msg")
    WebElement errorMessage;

    @FindBy(css = ".domik2__submit button")
    WebElement enterButton;

    public void enterLogin(String email) {
        loginInput.sendKeys(email);
    }

    public void enterPassword(String password) {
        passwordInput.sendKeys(password);
    }

    public void submitButtonClick() {
        enterButton.click();
    }

    public boolean isErrorPresent() {
        return errorMessage.isDisplayed();
    }
}
