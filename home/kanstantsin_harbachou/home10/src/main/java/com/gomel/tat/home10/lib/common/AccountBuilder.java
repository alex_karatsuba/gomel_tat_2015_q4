package com.gomel.tat.home10.lib.common;

import org.apache.commons.lang3.RandomStringUtils;
import static com.gomel.tat.home10.lib.common.CommonConstants.*;

public class AccountBuilder {

    public static Account getDefaultAccount() {
        Account account = new Account();
        account.setLogin(LOGIN_YANDEX);
        account.setPassword(PASSWORD_YANDEX);
        account.setEmail(DEFAULT_MAIL_TO_SEND);
        return account;
    }

    public static Account getAccountWithWrongPass() {
        Account account = getDefaultAccount();
        account.setLogin(account.getEmail() + RandomStringUtils.randomAlphabetic(3));
        account.setPassword(account.getPassword() + RandomStringUtils.randomAlphabetic(3));
        return account;
    }
}
