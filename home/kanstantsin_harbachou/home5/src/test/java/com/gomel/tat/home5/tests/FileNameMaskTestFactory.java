package com.gomel.tat.home5.tests;

import org.testng.annotations.Factory;

public class FileNameMaskTestFactory {

    @Factory
    public static Object[][] valuesForCheck() {
        return new Object[][]{
                {"e:/gomel_tat_2015_q4/home", "home3/hello.txt"},
                {"e:/gomel_tat_2015_q4/home", "*.txt"},
                {"e:/gomel_tat_2015_q4/home", "*home*/*.txt"}
        };
    }
}
