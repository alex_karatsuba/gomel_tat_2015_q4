package com.gomel.tat.home5.tests;

import com.gomel.tat.home5.tests.utils.PrepareData;
import org.testng.Assert;
import org.testng.annotations.Test;

public class FileReaderTests extends BaseWordListTests {

    @Test(description = "Checking fill file", groups = "Checking file's entry")
    public void checkFillFile() {
        System.out.println("checkFillFile");
        wordList.getWords(PrepareData.getDirectoryPath(), PrepareData.fileName);
        Assert.assertEquals(wordList.getValues(), PrepareData.getTestData());
    }

    @Test(description = "Checking empty file", groups = "Checking file's entry")
    public void checkEmptyFile() {
        System.out.println("checkEmptyFile");
        wordList.getWords(PrepareData.getDirectoryPath(), PrepareData.getEmptyFileName());
        Assert.assertEquals(true, wordList.getValues().isEmpty());

    }
}