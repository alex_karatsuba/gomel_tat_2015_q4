package com.gomel.tat.home8.runner;

import org.testng.TestNG;
import org.testng.xml.XmlSuite;

import java.util.ArrayList;
import java.util.List;

public class Runner {

    public static void main(String[] args) {

        TestNG tng = new TestNG();
        XmlSuite suite = new XmlSuite();
        suite.setName("Classes");
        List<String> files = new ArrayList();
        files.addAll(new ArrayList<String>() {{
            add("./src/main/resources/suit.xml");
        }});
        suite.setSuiteFiles(files);
        suite.setThreadCount(3);
        List<XmlSuite> suites = new ArrayList<>();
        suites.add(suite);
        tng.setXmlSuites(suites);
        tng.run();
    }
}

