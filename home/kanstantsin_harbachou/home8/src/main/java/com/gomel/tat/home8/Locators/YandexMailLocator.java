package com.gomel.tat.home8.Locators;


import org.openqa.selenium.By;

public class YandexMailLocator {

    //Index Page
    public static final By LOGIN_INPUT_LOCATOR = By.xpath("//input[@name='login']");
    public static final By PASSWORD_INPUT_LOCATOR = By.xpath("//input[@name='passwd']");
    public static final By ENTER_BUTTON_LOCATOR = By.cssSelector(".domik2__submit button");
    public static final By INCOMING_LINK_LOCATOR = By.cssSelector("div.b-header div a[href$='#inbox']");

    //Mail Page
    public static final By ERROR_MESSAGE = By.cssSelector(".error-msg");
    public static final By USER_EMAIL_LOCATOR = By.cssSelector(".header-user-name.js-header-user-name");
    public static final By WRITE_NEW_MAIL_LOCATOR = By.cssSelector("span.b-toolbar__item__label.js-toolbar-item-title-compose");
    public static final By INBOX_LINK_LOCATOR = By.cssSelector("span.b-folders__folder__name a[href$='#inbox']");
    public static final By OUTCOMING_LINK_LOCATOR = By.cssSelector("a[href$='#sent']");
    public static final By DELETED_LINK_LOCATOR = By.cssSelector("a[href$='#trash']");
    public static final By DRAFT_LINK_LOCATOR = By.cssSelector("a[href$='#draft']");
    public static final By EXISTING_MAIL_SUBJECT = By.cssSelector("span.b-messages__subject");
    public static final By CONFIRMING_SAVING_CHANGES = By.cssSelector("span._nb-button-content");

    //Creating mail Page
    public static final By MAIL_ADDRESS_LOCATOR = By.cssSelector("tr.b-compose-head__field.b-compose-head__field_to.js-compose-field-wrapper__to td.b-compose-head__field__value div.b-yabble.b-yabble_focused input");
    public static final By MAIL_SUBJECT_LOCATOR = By.name("subj");
    public static final By MAIL_POST_LOCATOR = By.cssSelector("body#tinymce div");
    public static final By MAIL_SUBMIT_LOCATOR = By.cssSelector("td.b-compose-head__title span span button");
    public static final By NOTIFICATION_LOCATOR = By.cssSelector("span.b-notification__i");

    //Draft
    public static final By DRAFT_MAIL_CHECKBOX = By.cssSelector("div.b-messages div input");
    public static final By DELETE_ACTION_LOCATOR = By.cssSelector("[data-action=delete]");
    public static final By CLEAR_TRASH_LOCATOR = By.cssSelector(".b-folders__folder__info img");
}
