package com.gomel.tat.home8.page.Yandexmail;

import com.gomel.tat.home8.page.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import static com.gomel.tat.home8.utils.WebDriverHelper.waitForElementIsClickable;

public class TrashPage extends Page {

    public static final By DELETED_LINK_LOCATOR = By.cssSelector("a[href$='#trash']");

    public TrashPage(WebDriver driver) {
        super(driver);
    }

    public void open() {
        waitForElementIsClickable(DELETED_LINK_LOCATOR).click();
    }

    public boolean isEnabled(String emailId) {

        try {
            WebElement findMailTrash = driver.findElement(By.xpath("//a[contains(@href,'" + emailId + "')]//ancestor::div[@data-action='mail.message.show-or-select']//input"));
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
