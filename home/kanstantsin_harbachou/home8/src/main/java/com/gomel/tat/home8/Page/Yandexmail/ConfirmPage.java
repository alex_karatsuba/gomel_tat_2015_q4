package com.gomel.tat.home8.page.Yandexmail;

import com.gomel.tat.home8.page.Page;
import com.gomel.tat.home8.utils.WebDriverHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ConfirmPage extends Page {

    public ConfirmPage(WebDriver driver) {
        super(driver);
    }

    public static final By CONFIRMATION_MAIL_SEND_LOCATOR = By.xpath("//a[@class='b-statusline__link']");

    public String getEmailId() {

        WebDriverHelper.waitForElementIsClickable(CONFIRMATION_MAIL_SEND_LOCATOR);
        WebElement notificationMailSend = driver.findElement(CONFIRMATION_MAIL_SEND_LOCATOR);
        String mail = notificationMailSend.getAttribute("href");
        int index = mail.indexOf("#");
        String mailId = mail.substring(index);
        return mailId;
    }
}
