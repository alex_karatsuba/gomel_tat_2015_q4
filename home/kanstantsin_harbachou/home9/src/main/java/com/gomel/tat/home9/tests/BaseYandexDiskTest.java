package com.gomel.tat.home9.tests;

import com.gomel.tat.home9.utils.InitialData;
import com.gomel.tat.home9.utils.WebDriverHelper;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Hashtable;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class BaseYandexDiskTest {

    protected static WebDriver driver;

    @BeforeClass(description = "Preparing browser")
    @Parameters({"browserName"})
    public void preparingBrowser(@Optional String browserName) throws MalformedURLException {
        if (driver == null) {
            if (browserName.equals("firefox")) {
                FirefoxProfile profile = new FirefoxProfile();
                profile.setPreference("browser.download.dir", InitialData.PATH_FOR_DOWNLOADING);
                profile.setPreference("browser.download.manager.showWhenStarting", false);
                profile.setPreference("browser.download.folderList", 2);
                profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "text/plain");
                DesiredCapabilities capabilities = DesiredCapabilities.firefox();
                capabilities.setCapability(FirefoxDriver.PROFILE, profile);
                driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), capabilities);
            } else {
                Map<String, Object> prefs = new Hashtable<>();
                prefs.put("profile.default_content_settings.popups", 0);
                prefs.put("download.prompt_for_download", "false");
                prefs.put("download.default_directory", InitialData.PATH_FOR_DOWNLOADING);
                prefs.put("download.directory_upgrade", true);
                ChromeOptions options = new ChromeOptions();
                options.setExperimentalOption("prefs", prefs);
                DesiredCapabilities capabilities = DesiredCapabilities.chrome();
                capabilities.setCapability(ChromeOptions.CAPABILITY, options);
                driver = new RemoteWebDriver(new URL(InitialData.REMOTE_DRIVER_URL), capabilities);
            }
            driver.manage().window().maximize();
            driver.manage().timeouts().pageLoadTimeout(WebDriverHelper.DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        }
    }

    @AfterClass(description = "Finishing browser")
    public void finishingBrowser() {
        try {
            driver.quit();
        } catch (Exception e) {
            System.err.println("Problem with shutting down driver: " + e.getMessage());
        } finally {
            driver = null;
        }
    }
}
