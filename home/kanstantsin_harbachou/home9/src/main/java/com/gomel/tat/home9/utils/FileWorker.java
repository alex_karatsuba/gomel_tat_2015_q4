package com.gomel.tat.home9.utils;

import org.apache.commons.lang3.RandomStringUtils;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.TreeMap;

public class FileWorker {


    public static String getDirectoryPath() {
        return directoryPath.toAbsolutePath().toString();
    }

    public static String getEmptyFileName() {
        return pathToEmptyFile.toAbsolutePath().toString();
    }

    public static String getFillFileName() {
        return pathToFillFile.toAbsolutePath().toString();
    }

    public static Map<String, Integer> getTestData() {
        return testData;
    }

    public static String fileName = "filltextfile.txt";
    private static Map<String, Integer> testData = new TreeMap<>();
    public static Path directoryPath = Paths.get(System.getProperty("java.io.tmpdir"));
    public static Path pathToEmptyFile = directoryPath.resolve("emptytextfile.txt");
    public static Path pathToFillFile = directoryPath.resolve(fileName);

    public static void createFolderAndFile() {

        try {
            if (!Files.exists(pathToEmptyFile)) {
                Files.createFile(pathToEmptyFile);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void createFolderAndFillingFile() {
        for (int i = 1; i < 6; i++) {
            testData.put(RandomStringUtils.randomAlphabetic(8), i);
        }

        Path file = Paths.get(getFillFileName());
        try (PrintWriter writer = new PrintWriter(Files.newBufferedWriter(file, StandardCharsets.UTF_8))) {
            for (Map.Entry entry : testData.entrySet()) {
                for (int i = 0; i < Integer.parseInt(entry.getValue().toString()); i++) {
                    writer.println(entry.getKey().toString());
                }
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
