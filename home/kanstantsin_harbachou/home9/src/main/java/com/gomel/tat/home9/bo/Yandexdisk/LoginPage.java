package com.gomel.tat.home9.bo.Yandexdisk;

import com.gomel.tat.home9.bo.AbstractPage;
import com.gomel.tat.home9.utils.InitialData;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage extends AbstractPage {

    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSWORD_INPUT_LOCATOR = By.name("password");
    public static final By ENTER_BUTTON_LOCATOR = By.cssSelector(".nb-button._nb-normal-button._init.ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only");

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public void open() {
        driver.get(InitialData.YANDEX_DISK_START_PAGE);
    }

    public DiskPage login(String userLogin, String userPassword) {
        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(userLogin);
        WebElement passInput = driver.findElement(PASSWORD_INPUT_LOCATOR);
        passInput.sendKeys(userPassword);
        WebElement submitButton = driver.findElement(ENTER_BUTTON_LOCATOR);
        submitButton.click();
        return new DiskPage(driver);
    }
}
