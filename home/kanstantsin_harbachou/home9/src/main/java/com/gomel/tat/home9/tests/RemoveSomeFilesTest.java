package com.gomel.tat.home9.tests;

import com.gomel.tat.home9.bo.Yandexdisk.DiskPage;
import com.gomel.tat.home9.bo.Yandexdisk.TrashPage;
import com.gomel.tat.home9.utils.FileService;
import com.gomel.tat.home9.utils.InitialData;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import java.util.LinkedList;
import java.util.List;

public class RemoveSomeFilesTest extends LoginTest {

    List<String> fileNameList = new LinkedList<>();
    DiskPage diskPage;

    @BeforeClass(description = "Create file")
    public void createFile() {
        for (int i = 0; i < 3; i++) {
            fileNameList.add(FileService.createFile());
        }
    }

    @Test(description = "Remove Some Files Test", priority = 1)
    public void checkRemovingSomeFiles() throws InterruptedException {
        diskPage = new DiskPage(driver);
        diskPage.open();
        for (int i = 0; i < 3; i++) {
            diskPage.uploadFile(InitialData.PATH_FOR_UPLOADING + fileNameList.get(i));
            Thread.sleep(2000);
            Assert.assertTrue(diskPage.isFilePresent(fileNameList.get(i)));
        }
        diskPage.deleteFiles(fileNameList);
        TrashPage trashPage = new TrashPage(driver);
        trashPage.open();
        for (int i = 0; i < 3; i++) {
            Assert.assertTrue(trashPage.isFilePresent(fileNameList.get(i)));
        }
    }
}
