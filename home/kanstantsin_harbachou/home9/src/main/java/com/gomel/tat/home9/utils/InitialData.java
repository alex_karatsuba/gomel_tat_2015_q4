package com.gomel.tat.home9.utils;


public class InitialData {

    public static final String LOGIN_YANDEX_DISK = "mailfortest3";
    public static final String PASSWORD_YANDEX_DISK = "passwordfortest3";
    public static final String REMOTE_DRIVER_URL = "http://127.0.0.1:4444/wd/hub";
    public static final String YANDEX_DISK_START_PAGE = "https://disk.yandex.ru";
    public static final String PATH_FOR_DOWNLOADING = "D:\\";
    public static final String PATH_FOR_UPLOADING = "E:\\";
}
