package com.gomel.tat.home9.utils;

import com.gomel.tat.home9.tests.BaseYandexDiskTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WebDriverHelper extends BaseYandexDiskTest {

    public static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 20;

    public static WebElement waitForElementIsClickable(By locator) {
        new WebDriverWait(driver, 5000).until(ExpectedConditions.elementToBeClickable(locator));
        return driver.findElement(locator);
    }
}
