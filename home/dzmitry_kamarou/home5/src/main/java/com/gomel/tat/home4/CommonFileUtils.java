package com.gomel.tat.home4;

import org.mozilla.universalchardet.UniversalDetector;

import java.io.FileInputStream;

public class CommonFileUtils {
    public static String getFileEncodingType(String filePath) {
        byte[] buf = new byte[8];
        UniversalDetector detector = new UniversalDetector(null);
        int bufNum;
        try {
            FileInputStream fileInputStream = new FileInputStream(filePath);
            while ((bufNum = fileInputStream.read(buf)) > 0 && !detector.isDone()) {
                detector.handleData(buf, 0, bufNum);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        detector.dataEnd();
        String encoding = detector.getDetectedCharset();
        detector.reset();
        return encoding;
    }
}
