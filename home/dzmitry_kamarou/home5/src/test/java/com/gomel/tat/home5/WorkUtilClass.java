package com.gomel.tat.home5;

import java.io.*;

class WorkUtilClass {

    public static File createDirectory(File whereDir, String name) {
        File directory = new File(whereDir.getAbsolutePath() + "/" + name);
        directory.mkdir();
        return directory;
    }

    public static File createFile(File whereDir, String name) {
        File file = new File(whereDir.getAbsolutePath() + "/" + name);
        try {
            file.createNewFile();
        } catch (IOException e) {
            System.out.println("Exception Message: " + e);
        }
        return file;
    }

    public static void removeDir(File directory) {
        File[] elements = directory.listFiles();
        if (elements != null) {
            for (File file : elements) {
                if (file.isDirectory()) {
                    removeDir(file);
                } else {
                    file.delete();
                }
            }
        }
        directory.delete();
    }

    public static void createEmptyFiles(File where) {
        int countDirectories = 1 + (int) (Math.random() * ((10 - 5) + 1));
        for (int i = 0; i < countDirectories; i++) {
            File newDir = WorkUtilClass.createDirectory(where, Integer.toString(i + 1));
            int countFiles = 1 + (int) (Math.random() * ((10 - 5) + 1));
            for (int j = 0; j < countFiles; j++) {
                WorkUtilClass.createFile(newDir, Integer.toString(j + 1) + ".txt");
            }
        }
    }
}