import org.testng.Assert;
import org.testng.annotations.Test;

public class CTest extends BaseTest {

    @Test
    public void loginFirefox() {
        driverFirefox = helper.createDriver("firefox");
        pageMail = new PageMail(driverFirefox);
        pageMail.open();
    }

    @Test(dependsOnMethods = "loginFirefox")
    public void createInvalidLetterFirefox() {
        pageMail.createLetter("invalid");
        pageMail.send();
        Assert.assertEquals(pageMail.message, "The letter wasn't sent.", "Invalid letter checking exception.");
    }

    @Test
    public void loginChrome() {
        driverChrome = helper.createDriver("chrome");
        pageMail = new PageMail(driverChrome);
        pageMail.open();
    }

    @Test(dependsOnMethods = "loginChrome")
    public void createInvalidLetterChrome() {
        pageMail.createLetter("invalid");
        pageMail.send();
        Assert.assertEquals(pageMail.message, "The letter wasn't sent.", "Invalid letter checking exception.");
    }
}
