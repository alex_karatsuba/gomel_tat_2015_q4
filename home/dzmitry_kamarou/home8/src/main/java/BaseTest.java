import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;

public class BaseTest {
    WebDriver driverFirefox, driverChrome;
    PageLogin pageLogin;
    PageMail pageMail;
    Helper helper;

    @BeforeClass
    public void preparePage() {
        helper = new Helper();
    }

    @AfterClass
    public void closeDriver() {
        try {
            driverFirefox.close();
            driverChrome.close();
        } catch (Exception e) {
            System.out.println("It's ok.");
        }
    }

}
