import org.testng.Assert;
import org.testng.annotations.Test;

public class DTest extends BaseTest {
    @Test
    public void loginFirefox() {
        driverFirefox = helper.createDriver("firefox");
        pageMail = new PageMail(driverFirefox);
        pageMail.open();
    }

    @Test(dependsOnMethods = "loginFirefox")
    public void checkInFirefox() {
        pageMail.createLetter("dzmitry.kamarou@yandex.ru","","");
        pageMail.send();
        Assert.assertNotNull(pageMail.checkMailInFolder("Inbox"));
        System.out.println("Mail exists in Inbox.");
        Assert.assertNotNull(pageMail.checkMailInFolder("Sent"));
        System.out.println("Mail exists in Sent.");
        System.out.println("Checking mail is ok.");
    }
    @Test
    public void loginChrome() {
        driverChrome = helper.createDriver("chrome");
        pageMail = new PageMail(driverChrome);
        pageMail.open();
    }

    @Test(dependsOnMethods = "loginChrome")
    public void checkInChrome() {
        pageMail.createLetter("dzmitry.kamarou@yandex.ru","","");
        pageMail.send();
        Assert.assertNotNull(pageMail.checkMailInFolder("Inbox"));
        System.out.println("Mail exists in Inbox.");
        Assert.assertNotNull(pageMail.checkMailInFolder("Sent"));
        System.out.println("Mail exists in Sent.");
        System.out.println("Checking mail is ok.");
    }
}
