package com.epam.gomel.tat2015.home10.lib.mail.screen;

import com.epam.gomel.tat2015.home10.lib.mail.business.Letter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WebDriver;

public class MailPage {
    public static final By INBOX_LINK = By.xpath("//a[contains(text(),'Inbox')]");
    public static final By COMPOSE_BUTTON = By.xpath("//a[@href='#compose']");
    public static final By SEND_BUTTON = By.xpath("//button[@id='compose-submit']");
    public static final By TO_INPUT = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    public static final By SUBJECT_INPUT = By.xpath("//input[@id='compose-subj']");
    public static final By TEXT_AREA = By.xpath("//textarea[@id='compose-send']");
    public static final By SENT_LINK = By.xpath("//a[@href='#sent']");
    public static final By SAVE_AND_GO_BUTTON = By.xpath("//span[contains(text(),'Save and go')]");
    public static final By STATUS_LINE = By.xpath("//span[@class='b-statusline__content']/a[@href]");
    public static final By SUCCESS_MESSAGE = By.xpath("//div[contains(text(),'Message sent successfully.')]");
    public static final By TRASH = By.xpath("//a[@href='#trash']");
    public static final By DELETE = By.xpath("//a[@title='Delete (Delete)']");
    public By MAIL_LINK_LOCATOR_PATTERN, MAIL_ROW;

    WebDriver driver;
    public WebElement composeButton, sendButton, toInput,
            subjectInput, textArea, folder, mail, sentLink, inboxLink,
            saveAndGoButton, statusLine, mailRow, delete, read;
    public Letter letter;
    public String id = "";

    public MailPage(WebDriver driver) {
        this.driver = driver;
    }

    public MailPage() {
    }

    public MailPage clickComposeLetterButton() {
        composeButton = driver.findElement(COMPOSE_BUTTON);
        composeButton.click();
        System.out.println("Compose letter button clicked.");
        return this;
    }

    public MailPage fillToInput(Letter letter) {

        toInput = driver.findElement(TO_INPUT);
        toInput.sendKeys(letter.getRecipient());
        System.out.println("To input filled.");
        return this;
    }

    public MailPage fillSubjectInput(Letter letter) {
        subjectInput = driver.findElement(SUBJECT_INPUT);
        subjectInput.sendKeys(letter.getSubject());
        System.out.println("Subject input filled.");
        return this;
    }

    public MailPage fillTextArea(Letter letter) {
        textArea = driver.findElement(TEXT_AREA);
        textArea.sendKeys(letter.getBody());
        System.out.println("Text area filled.");
        return this;
    }

    public MailPage clickSendButton() {
        sendButton = driver.findElement(SEND_BUTTON);
        sendButton.click();
        System.out.println("Send button clicked.");
        return this;
    }

    public MailPage clickSentLink() {
        sentLink = driver.findElement(SENT_LINK);
        sentLink.click();
        System.out.println("Sent link clicked.");
        return this;
    }

    public MailPage clickInboxLink() {
        inboxLink = driver.findElement(INBOX_LINK);
        inboxLink.click();
        System.out.println("Inbox link clicked.");
        return this;
    }
}
