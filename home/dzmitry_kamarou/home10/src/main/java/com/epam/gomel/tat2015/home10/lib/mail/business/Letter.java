package com.epam.gomel.tat2015.home10.lib.mail.business;

import java.io.File;

public class Letter {
    String recipient;
    public String subject;
    String body;
    File attachment;

    public String mailTo, mailContent, href, letterID, data_params;

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public File getAttachment() {
        return attachment;
    }

    public void setAttachment(File attachment) {
        this.attachment = attachment;
    }

    public boolean containsAttach() {
        return false;
    }
}
