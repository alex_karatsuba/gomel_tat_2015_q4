package com.epam.gomel.tat2015.home10.tests.mail;

import com.epam.gomel.tat2015.home10.lib.common.Account;
import com.epam.gomel.tat2015.home10.lib.common.AccountBuilder;
import com.epam.gomel.tat2015.home10.lib.login.service.LoginService;
import com.epam.gomel.tat2015.home10.lib.mail.business.Letter;
import com.epam.gomel.tat2015.home10.lib.mail.business.LetterBuilder;
import com.epam.gomel.tat2015.home10.lib.mail.screen.MailPage;
import com.epam.gomel.tat2015.home10.lib.mail.service.MailService;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

public class MailWithoutProperlyFilledAddressTest extends BaseTest {
    Account account;
    Letter letter;
    LoginService loginService;
    MailService mailService;
    boolean isSend;

    @Test
    public void loginIntoMail() {
        loginService = new LoginService(driver);
        account = AccountBuilder.getValidAccount();
        loginService.loginToMailbox(account);
        System.out.println("Waiting for mail page inbox link.");
        new WebDriverWait(driver, 10000).until(ExpectedConditions.visibilityOfElementLocated(MailPage.INBOX_LINK));
    }

    @Test(dependsOnMethods = "loginIntoMail")
    public void createInvalidLetter() {
        System.out.println("Create invalid letter started.");
        letter = LetterBuilder.buildInvalidAddressLetter();
        System.out.println("Invalid letter was created.");
        mailService = new MailService(driver);
        isSend = mailService.sendInvalidLetter(letter);
        Assert.assertEquals(isSend, false, "Invalid letter checking exception.");
    }
}
