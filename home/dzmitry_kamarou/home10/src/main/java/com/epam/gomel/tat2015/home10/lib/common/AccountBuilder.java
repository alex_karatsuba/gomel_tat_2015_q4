package com.epam.gomel.tat2015.home10.lib.common;

import org.apache.commons.lang3.RandomStringUtils;

import static com.epam.gomel.tat2015.home10.lib.common.CommonConstants.*;

public class AccountBuilder {

    public static Account getBlankAccount() {
        Account account = new Account();
        account.setLogin(DEFAULT_MAIL_USER_LOGIN);
        account.setPassword(DEFAULT_MAIL_USER_PASSWORD);
        account.setMark("blank account");
        return account;
    }

    public static Account getAccountWithWrongPass() {
        Account account = getBlankAccount();
        account.setPassword(account.getPassword() + RandomStringUtils.randomAlphabetic(3));
        account.setMark("account with wrong pass");
        return account;
    }

    public static Account getValidAccount() {
        Account account = new Account();
        account.setLogin(VALID_USER_LOGIN);
        account.setPassword(VALID_USER_PASSWORD);
        account.setMark("valid account");
        return account;
    }
}
