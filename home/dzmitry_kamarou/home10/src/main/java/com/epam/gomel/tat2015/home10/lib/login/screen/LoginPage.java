package com.epam.gomel.tat2015.home10.lib.login.screen;

import com.epam.gomel.tat2015.home10.lib.common.Account;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.epam.gomel.tat2015.home10.lib.mail.screen.MailPage;

public class LoginPage {
    public static final By LOGIN_BUTTON = By.xpath("//div[@class='new-left']//button[@type='submit']");
    public static final By LOGIN_INPUT = By.xpath("//label[@id='nb-1']//input[@name='login']");
    public static final By PASSWORD_INPUT = By.xpath("//label[@id='nb-2']//input[@name='passwd']");

    WebElement loginButton, loginInput, passwordInput;

    public LoginPage(WebDriver driver) {
        loginButton = driver.findElement(LOGIN_BUTTON);
        loginInput = driver.findElement(LOGIN_INPUT);
        passwordInput = driver.findElement(PASSWORD_INPUT);
    }

    public LoginPage fillLoginInput(Account account) {
        loginInput.sendKeys(account.getLogin());
        return this;
    }

    public LoginPage fillPasswordInput(Account account) {
        passwordInput.sendKeys(account.getPassword());
        return this;
    }

    public MailPage clickEnterButton() {
        loginButton.click();
        return new MailPage();
    }
}
