package com.epam.gomel.tat2015.home10.lib.mail.service;

import com.epam.gomel.tat2015.home10.lib.mail.business.Letter;
import com.epam.gomel.tat2015.home10.lib.mail.screen.MailPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Scanner;


public class MailService {
    MailPage mailPage;
    WebDriver driver;

    public MailService(WebDriver driver) {
        this.driver = driver;
        mailPage = new MailPage(driver);
    }

    public void sendLetter(Letter letter) {
        mailPage.clickComposeLetterButton();
        System.out.println("Waiting for to input");
        new WebDriverWait(driver, 10000).until(ExpectedConditions.visibilityOfElementLocated(mailPage.TO_INPUT));
        mailPage.fillToInput(letter);
        mailPage.fillSubjectInput(letter);
        mailPage.fillTextArea(letter);
        mailPage.clickSendButton();
        mailPage.MAIL_LINK_LOCATOR_PATTERN = By.xpath("//*[@class='block-messages']//a[contains(.,'" + letter.subject + "')]");
        System.out.println("//*[@class='block-messages']//a[contains(., '" + letter.subject + "')]");
    }

    public boolean sendInvalidLetter(Letter letter) {
        mailPage.clickComposeLetterButton();
        System.out.println("Waiting for to input");
        new WebDriverWait(driver, 10000).until(ExpectedConditions.visibilityOfElementLocated(mailPage.TO_INPUT));
        mailPage.fillToInput(letter);
        mailPage.fillSubjectInput(letter);
        mailPage.fillTextArea(letter);
        mailPage.clickSendButton();
        String url = "";
        url = driver.getCurrentUrl();
        int includeWord = url.indexOf("compose");
        if (includeWord > -1) {
            return false;
        } else {
            return true;
        }
    }

    public WebElement sendBlankLetter(Letter letter) {
        mailPage.statusLine = driver.findElement(mailPage.STATUS_LINE);
        String href = mailPage.statusLine.getAttribute("href");
        letter.href = href;
        Scanner scanner = new Scanner(href);
        scanner.useDelimiter("/");
        while (scanner.hasNext()) {
            mailPage.id = scanner.next();
        }
        letter.letterID = mailPage.id;
        System.out.println("Letters id=" + mailPage.id);
        mailPage.MAIL_ROW = By.xpath("//div[@data-id='" + mailPage.id + "']");
        mailPage.mailRow = driver.findElement(mailPage.MAIL_ROW);
        System.out.println("mailRow: " + mailPage.mailRow);
        return mailPage.mailRow;
    }

    public boolean isLetterExistInSent() {
        System.out.println(mailPage.MAIL_LINK_LOCATOR_PATTERN);
        System.out.println("Waiting for mail row.");
        new WebDriverWait(driver, 10000).until(ExpectedConditions.visibilityOfElementLocated(mailPage.MAIL_LINK_LOCATOR_PATTERN));
        mailPage.mail = driver.findElement(mailPage.MAIL_LINK_LOCATOR_PATTERN);
        if (mailPage != null) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isLetterExistInInbox() {
        System.out.println(mailPage.MAIL_LINK_LOCATOR_PATTERN);
        System.out.println("Waiting for mail row.");
        new WebDriverWait(driver, 10000).until(ExpectedConditions.visibilityOfElementLocated(mailPage.MAIL_LINK_LOCATOR_PATTERN));
        mailPage.mail = driver.findElement(mailPage.MAIL_LINK_LOCATOR_PATTERN);
        if (mailPage != null) {
            return true;
        } else {
            return false;
        }
    }

    public void goToSent() {
        mailPage.clickSentLink();
    }

    public void goToInbox() {
        mailPage.clickInboxLink();
    }
}
