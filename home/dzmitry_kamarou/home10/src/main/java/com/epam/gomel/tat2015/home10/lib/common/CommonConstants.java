package com.epam.gomel.tat2015.home10.lib.common;

public interface CommonConstants {
    String MAILBOX_URL = "";
    String LOGIN_PAGE_URL = "https://mail.yandex.by/";
    String DEFAULT_MAIL_USER_LOGIN = "";
    String DEFAULT_MAIL_USER_PASSWORD = "";
    String DEFAULT_MAIL_TO_SEND = "";
    String DEFAULT_MAIL_SUBJECT = "";
    String DEFAULT_MAIL_BODY = "";
    String VALID_USER_LOGIN = "dzmitry.kamarou";
    String VALID_USER_PASSWORD = "q1w1e1r1";
}
