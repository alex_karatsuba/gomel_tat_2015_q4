import org.testng.Assert;
import org.testng.TestNG;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;

public class ATest extends BaseTest {
    @Test
    public void testInFirefoxPositive() {
        webDriverFirefox=UtilWebDriverClass.createFirefoxWebDriver();
        webDriverFirefox.get(YANDEX_START_PAGE);
        WebElement loginButton = webDriverFirefox.findElement(LOGIN_BUTTON);
        WebElement loginInput = webDriverFirefox.findElement(LOGIN_INPUT);
        WebElement passwordInput = webDriverFirefox.findElement(PASSWORD_INPUT);
        loginInput.sendKeys("dzmitry.kamarou");
        passwordInput.sendKeys("q1w1e1r1");
        loginButton.click();
        WebElement inboxLink = webDriverFirefox.findElement(INBOX_LINK);
        System.out.println(inboxLink);
        Assert.assertNotNull(inboxLink);
        webDriverFirefox.close();
        webDriverFirefox.quit();
    }
    @Test
    public void testInChromePositive() {
        webDriverChrome=UtilWebDriverClass.createChromeWebDriver();
        webDriverChrome.get(YANDEX_START_PAGE);
        WebElement loginButton = webDriverChrome.findElement(LOGIN_BUTTON);
        WebElement loginInput = webDriverChrome.findElement(LOGIN_INPUT);
        WebElement passwordInput = webDriverChrome.findElement(PASSWORD_INPUT);
        loginInput.sendKeys("dzmitry.kamarou");
        passwordInput.sendKeys("q1w1e1r1");
        loginButton.click();
        new WebDriverWait(webDriverChrome, 5000).until(ExpectedConditions.elementToBeClickable(INBOX_LINK));
        WebElement inboxLink = webDriverChrome.findElement(INBOX_LINK);
        System.out.println(inboxLink);
        Assert.assertNotNull(inboxLink);
        webDriverChrome.close();
        webDriverChrome.quit();
    }
    @Test
    public void testInFirefoxNegative() {
        webDriverFirefox=UtilWebDriverClass.createFirefoxWebDriver();
        webDriverFirefox.get(YANDEX_START_PAGE);
        //new WebDriverWait(webDriverFirefox, 5000).until(ExpectedConditions.elementToBeClickable(LOGIN_BUTTON));
        WebElement loginButton = webDriverFirefox.findElement(LOGIN_BUTTON);
        loginButton.click();
        WebElement domikContent = webDriverFirefox.findElement(DOMIK_CONTENT);
        System.out.println(domikContent);
        Assert.assertNotNull(domikContent);
        webDriverFirefox.close();
        webDriverFirefox.quit();
    }
    @Test
    public void testInChromeNegative() {
        webDriverChrome=UtilWebDriverClass.createChromeWebDriver();
        webDriverChrome.get(YANDEX_START_PAGE);
        WebElement loginButton = webDriverChrome.findElement(LOGIN_BUTTON);
        loginButton.click();
        new WebDriverWait(webDriverChrome, 5000).until(ExpectedConditions.visibilityOfElementLocated(DOMIK_CONTENT));
        WebElement domikContent = webDriverChrome.findElement(DOMIK_CONTENT);
        System.out.println(domikContent);
        Assert.assertNotNull(domikContent);
        webDriverChrome.close();
        webDriverChrome.quit();
    }
}
