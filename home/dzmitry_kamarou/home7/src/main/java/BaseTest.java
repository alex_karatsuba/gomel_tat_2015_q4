import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.*;

public class BaseTest {

    public static final String YANDEX_START_PAGE = "https://mail.yandex.by/";
    public static final By LOGIN_BUTTON = By.xpath("//div[@class='new-left']//button[@type='submit']");
    public static final By LOGIN_INPUT = By.xpath("//label[@id='nb-1']//input[@name='login']");
    public static final By PASSWORD_INPUT = By.xpath("//label[@id='nb-2']//input[@name='passwd']");
    public static final By INBOX_LINK=By.xpath("//a[contains(text(),'Inbox')]");
    //public static final By SENT_LINK=By.xpath("//a[@href='#sent']");
    public static final By DOMIK_CONTENT=By.xpath("//div[@class='domik-content']");
    public static final By COMPOSE_BUTTON=By.xpath("//a[@href='#compose']");
    public static final By TO_INPUT=By.xpath("//div[contains(@class,'b-mail-input')]/input[@type='text']");//input[@name='to']
    public static final By SUBJECT_INPUT=By.xpath("//input[@id='compose-subj']");
    public static final By LETTER_TEXT_FIELD=By.xpath("//body[@id='tinymce']/div[1]");
    public static final By SEND_BUTTON=By.xpath("//button[@id='compose-submit']");
    public static final By LETTER_TEXT_AREA=By.xpath("//input[@id='nb-checkbox_2']");
    public static final By SENT_RESULT=By.xpath("//div[contains(text(),'Message sent successfully.')]");

    WebDriver webDriver, webDriverFirefox,webDriverChrome;


    @BeforeSuite
    public void beforeSuite() {




    }

    @BeforeClass
    public void beforeClass() {

    }

    @BeforeGroups
    public void beforeGroups() {

    }

    @BeforeMethod
    public void beforeMethod() {

    }

    @AfterSuite
    public void afterSuite() {

    }

    @AfterClass
    public void afterClass() {

    }

    @AfterGroups
    public void afterGroups() {

    }

    @AfterMethod
    public void afterMethod() {

    }
}
