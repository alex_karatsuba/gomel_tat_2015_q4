//package com.gomel.tat.home5;

import org.testng.TestNG;
import org.testng.xml.XmlSuite;

import java.io.FileWriter;
import java.util.*;

public class TestRunner {
    public static void main(String args[]) {
        try {
            //FileWriter fileWriter = new FileWriter("C:/Users/HozWin/Desktop/TestInformation.txt");
            TestNG testNG = new TestNG();
            //testNG.addListener(new TestListener1(fileWriter));
            XmlSuite suite = new XmlSuite();
            //XmlSuite suite1 = new XmlSuite();
            suite.setName("Suite1");
            //suite1.setName("Suite2");
            List<String> files = new ArrayList<String>();
            files.addAll(new ArrayList<String>() {{
                add("./src/test/resources/SuiteFirst.xml");
            }});
            /*files.addAll(new ArrayList<String>() {{
                add("./src/test/resources/SuiteSecond.xml");
            }});*/
            suite.setSuiteFiles(files);
            //suite1.setSuiteFiles(files);
            List<XmlSuite> suites = new ArrayList<XmlSuite>();
            suites.add(suite);
            //suites.add(suite1);
            testNG.setXmlSuites(suites);
            testNG.run();
            //fileWriter.close();
        } catch (Exception e) {
            System.out.println("Exception: " + e);
        }
    }
}
