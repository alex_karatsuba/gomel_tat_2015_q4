import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;

public class PageDisk extends Page {
    public static final By UPLOAD_BUTTON = By.xpath("//input[@class='button__attach']");
    public static final By UPLOAD_CLOSE_BUTTON = By.xpath("//button[contains(@class,'button-close')]");
    public static final By FILES_TAB = By.xpath("//a[@href='/client/disk']");
    public static final By TRASH = By.xpath("//div[@data-id='/trash']");
    public static final By RESTORE_BUTTON = By.xpath("//button[@data-click-action='resource.restore']");
    public static final String trashUrl = "https://disk.yandex.ru/client/trash";
    public static final String diskUrl = "https://disk.yandex.ru/client/disk";
    public static By DOWNLOAD_BUTTON, DELETE_PERMANENTLY_BUTTON;

    JavascriptExecutor jsExecutor;
    PageMail pageMail;
    WebElement uploadButton, uploadCloseButton, downloadButton, filesTab, diskFile, trash, deletePermanentlyButton, restoreButton;
    File file;
    String fileFromPCContent = "null";
    String fileName = "null";
    String deletedFrom;

    PageDisk(WebDriver driver, String driverName) {
        super(driver, driverName);
    }

    void open() {
        pageMail = new PageMail(driver, driverName);
        pageMail.open();
        pageMail.diskClick();
        System.out.println("Disk page is open.");
    }

    File uploadFile() {
        System.out.println("Upload file started.");
        file = Helper.fileGenerate(this);
        System.out.println("File was generated.");
        uploadButton = driver.findElement(UPLOAD_BUTTON);
        uploadButton.sendKeys(file.getAbsolutePath());
        new WebDriverWait(driver, 15000).until(ExpectedConditions.elementToBeClickable(UPLOAD_CLOSE_BUTTON));
        uploadCloseButton = driver.findElement(UPLOAD_CLOSE_BUTTON);
        uploadCloseButton.click();
        filesTab = driver.findElement(FILES_TAB);
        new WebDriverWait(driver, 15000).until(ExpectedConditions.elementToBeClickable(FILES_TAB));
        filesTab.click();
        return file;
    }

    void downloadFile(String fileDiskName) {
        By UPLOADED_FILE = By.xpath("//div[@title='" + fileDiskName + "']");
        String loadedFileContent = "";
        while (loadedFileContent.equals("")) {
            System.out.println();
            try {
                System.out.println("Uploading disk page.");
                driver.get("https://disk.yandex.ru/client/disk");
                System.out.println("Waiting for disk file.");
                Thread.sleep(4000);
                diskFile = driver.findElement(UPLOADED_FILE);
                boolean find = false;
                while (!find) {
                    try {
                        diskFile.click();
                        System.out.println("Disk file is clicked.");
                        Thread.sleep(4000);
                        find = true;
                    } catch (NoSuchElementException e) {
                        System.out.println("Disk file not found, scrolling down.");
                        jsExecutor = (JavascriptExecutor) driver;
                        jsExecutor.executeScript("window.scrollBy(0,150)", "");
                    }
                }
                DOWNLOAD_BUTTON = By.xpath("//button[contains(@data-params,'" + fileDiskName + "')][2]");
                downloadButton = driver.findElement(DOWNLOAD_BUTTON);
                System.out.println("Download button is find.");
                downloadButton.click();
                System.out.println("Download button clicked.");
                File file = new File("c:\\Download\\" + fileDiskName);
                Thread.sleep(5000);
                if (file.exists()) {
                    System.out.println("File loaded.");
                    loadedFileContent = Helper.getFileContent(file);
                    System.out.println("Loaded file content: " + loadedFileContent);
                    if (loadedFileContent.equals("")) {
                        System.out.println("Loaded file content is blank.");
                        file.delete();
                    }
                } else {
                    System.out.println("File not loaded.");
                }
            } catch (Exception e) {
                System.out.println("Some download file exception: " + e);
                if (!(driver.getCurrentUrl()).endsWith("disk")) {
                    try {
                        filesTab = driver.findElement(FILES_TAB);
                        filesTab.click();
                        System.out.println("Files tab clicked.");
                        Thread.sleep(5000);
                    } catch (Exception e1) {
                        System.out.println("Switch on files tab problem: " + e1);
                    }
                }
            }
        }
    }

    String removeToTrash(WebElement diskFile) {
        deletedFrom = driver.getCurrentUrl();
        trash = driver.findElement(TRASH);
        Action dragToTrashAction = new Actions(driver).dragAndDrop(diskFile, trash).build();
        dragToTrashAction.perform();
        System.out.println("Disk file removed to trash by drag&drop.");
        return deletedFrom;
    }

    boolean checkFileInTrash(String fileName) {
        System.out.println("Checking removed file in trash.");
        String URL = driver.getCurrentUrl();
        if (URL.equals("https://disk.yandex.ru/client/trash")) {
            System.out.println("Current page is already trash page.");
        } else {
            trash = driver.findElement(TRASH);
            Action doubleClickTrashAction = new Actions(driver).doubleClick(trash).build();
            doubleClickTrashAction.perform();
            System.out.println("Going to trash by double click.");
            System.out.println("Waiting for trash content.");
            try {
                Thread.sleep(9000);
            } catch (Exception e) {
            }
        }
        try {
            String xPath = "//div[@data-id='/trash/" + fileName + "']";
            By DELETED_FILE = By.xpath(xPath);
            WebElement deletedFile = driver.findElement(DELETED_FILE);
            deletedFile.click();
            System.out.println("Deleted file " + fileName + " exists in trash and selected.");
            return true;
        } catch (NoSuchElementException e) {
            System.out.println("Deleted file is not found in trash.");
        } catch (ElementNotVisibleException e) {
            System.out.println("Deleted file is not visible in trash.");
            return false;
        } catch (StaleElementReferenceException e) {
            System.out.println("Deleted file is not present in trash.");
            return false;
        }
        return false;
    }

    void deleteSelectedFileInTrash(String fileName) {
        DELETE_PERMANENTLY_BUTTON = By.xpath("//button[contains(@data-params,'" + fileName + "')][2]");
        System.out.println("Waiting for delete permanently button.");
        new WebDriverWait(driver, 15000).until(ExpectedConditions.elementToBeClickable(DELETE_PERMANENTLY_BUTTON));
        deletePermanentlyButton = driver.findElement(DELETE_PERMANENTLY_BUTTON);
        if (deletePermanentlyButton.isDisplayed()) {
            System.out.println("File is already selected.");
            System.out.println("Deleting " + fileName + " permanently.");
            deletePermanentlyButton.click();
            System.out.println("Delete permanently button clicked.");
        } else {
            System.out.println("Delete permanently button is not displayed.");
        }
    }

    void restoreFile(String fileName) {
        System.out.println("Restoring file " + fileName);
        String currentUrl = driver.getCurrentUrl();
        if (!currentUrl.equals(trashUrl)) {
            driver.get(trashUrl);
            System.out.println("Trash page is download.");
        }
        String xPath = "//div[@data-id='/trash/" + fileName + "']";
        By DELETED_FILE = By.xpath(xPath);
        System.out.println("Waiting for deleted file.");
        new WebDriverWait(driver, 15000).until(ExpectedConditions.elementToBeClickable(DELETED_FILE));
        WebElement deletedFile = driver.findElement(DELETED_FILE);
        deletedFile.click();
        System.out.println("Deleted file is find and selected.");
        System.out.println("Waiting for restore button.");
        new WebDriverWait(driver, 15000).until(ExpectedConditions.elementToBeClickable(RESTORE_BUTTON));
        restoreButton = driver.findElement(RESTORE_BUTTON);
        restoreButton.click();
        System.out.println("Restore button clicked.");
    }

    boolean checkRestoredFile(String deletedFrom, String fileName) {
        driver.get(deletedFrom);
        System.out.println("Primary folder " + deletedFrom + " is open.");
        try {
            String restoredFileName = "//div[@data-id='/disk/" + fileName + "']";
            By RESTORED_FILE = By.xpath(restoredFileName);
            System.out.println("Waiting for restored file " + fileName + " is clickable.");
            new WebDriverWait(driver, 15000).until(ExpectedConditions.elementToBeClickable(RESTORED_FILE));
            WebElement restoredFile = driver.findElement(RESTORED_FILE);
            restoredFile.click();
            System.out.println("Restored file found and selected.");
            return true;
        } catch (Exception e) {
            System.out.println("Some problem with restored file: " + e);
        }
        return false;
    }

    void removeSeveralElements(File[] elements) {
        String currentUrl = driver.getCurrentUrl();
        if (!currentUrl.equals(diskUrl)) {
            System.out.println("Uploading disk page.");
            driver.get(diskUrl);
        } else {
            System.out.println("It's already disk page.");
        }
        By[] diskElementsPaths = new By[elements.length];
        WebElement[] diskElements = new WebElement[elements.length];
        for (int i = 0; i < elements.length; i++) {
            diskElementsPaths[i] = By.xpath("//div[@data-id='/disk/" + elements[i].getName() + "']");
            diskElements[i] = driver.findElement(diskElementsPaths[i]);
        }
        System.out.println("Files are found in disk.");
        Actions ctrlPress = new Actions(driver).keyDown(Keys.CONTROL);
        ctrlPress.build().perform();
        System.out.println("Ctrl is pressed.");
        for (int i = 0; i < elements.length; i++) {
            diskElements[i].click();
            System.out.println("File " + elements[i].getName() + " selected.");
        }
        ctrlPress = new Actions(driver).keyUp(Keys.CONTROL);
        ctrlPress.build().perform();
        System.out.println("Ctrl is unpressed.");
        System.out.println("Files are selected.");
        trash = driver.findElement(TRASH);
        Action dragToTrashAction = new Actions(driver).dragAndDrop(diskElements[0], trash).build();
        dragToTrashAction.perform();
        System.out.println("Selected files removed to trash by drag&drop.");
    }

    boolean checkFilesInTrash(String[] filesNames) {
        System.out.println("Checking removed files in trash.");
        String URL = driver.getCurrentUrl();
        if (URL.equals("https://disk.yandex.ru/client/trash")) {
            System.out.println("Current page is already trash page.");
        } else {
            trash = driver.findElement(TRASH);
            Action doubleClickTrashAction = new Actions(driver).doubleClick(trash).build();
            doubleClickTrashAction.perform();
            System.out.println("Going to trash by double click.");
            System.out.println("Waiting for trash content.");
            try {
                Thread.sleep(9000);
            } catch (Exception e) {
            }
        }
        try {
            String[] xPaths = new String[filesNames.length];
            By[] DELETED_FILES = new By[filesNames.length];
            WebElement[] deletedFiles = new WebElement[filesNames.length];
            Actions ctrlPress = new Actions(driver).keyDown(Keys.CONTROL);
            ctrlPress.build().perform();
            System.out.println("Ctrl is pressed.");
            for (int i = 0; i < filesNames.length; i++) {
                xPaths[i] = "//div[@data-id='/trash/" + filesNames[i] + "']";
                DELETED_FILES[i] = By.xpath(xPaths[i]);
                deletedFiles[i] = driver.findElement(DELETED_FILES[i]);
                deletedFiles[i].click();
                System.out.println("Deleted file " + filesNames[i] + " exists in trash and selected.");
            }
            ctrlPress = new Actions(driver).keyUp(Keys.CONTROL);
            ctrlPress.build().perform();
            System.out.println("Ctrl is unpressed.");
            System.out.println("Deleted files are selected in trash.");
            return true;
        } catch (Exception e) {
            System.out.println("Some selection exception: " + e);
        }
        return false;
    }
}