package com.home.tat.home9.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.home.tat.home9.utils.WebDriverHelper;

public class TrashPage extends DiskPage {
	public static final String BASE_URL = "https://disk.yandex.ru/client/trash";
	public static final String FILE_ELEMENT_LOCATOR_PATTERN = "//*[@class='nb-resource__name']//div[contains(., '%s')]";
	public static final By DELETE_BUTTON_LOCATOR = By
			.xpath("//button[@data-click-action='resource.delete']");
	public static final By RESTORE_BUTTON_LOCATOR = By
			.xpath("//button[@data-click-action='resource.restore']");

	public TrashPage(WebDriver driver) {
		super(driver);
	}

	public void open() {
		driver.get(BASE_URL);
	}

	public TrashPage deleteFilePermanently(String fileName) {
		driver.navigate().refresh();
		findAndClickFileElement(fileName);
		WebDriverHelper.waitForElementIsClickable(DELETE_BUTTON_LOCATOR);
		WebElement deleteButton = driver.findElement(DELETE_BUTTON_LOCATOR);
		deleteButton.click();
		WebDriverHelper.waitForDisappear(By.xpath(String.format(
				FILE_ELEMENT_LOCATOR_PATTERN, fileName)));
		return this;
	}

	public TrashPage restoreFile(String fileName) {
		driver.navigate().refresh();
		findAndClickFileElement(fileName);
		WebDriverHelper.waitForElementIsClickable(RESTORE_BUTTON_LOCATOR);
		WebElement deleteButton = driver.findElement(RESTORE_BUTTON_LOCATOR);
		deleteButton.click();
		WebDriverHelper.waitForDisappear(By.xpath(String.format(
				FILE_ELEMENT_LOCATOR_PATTERN, fileName)));
		return this;
	}

	public boolean isFilesPresent(String... filesNames) {
		boolean fileExisting = false;
		for (String fileName : filesNames) {
			try {
				WebDriverHelper.waitForAppearence(By.xpath(String.format(
						FILE_ELEMENT_LOCATOR_PATTERN, fileName)));
			} catch (Exception e) {
				fileExisting = false;
			}
			fileExisting = true;
		}
		return fileExisting;
	}
}
