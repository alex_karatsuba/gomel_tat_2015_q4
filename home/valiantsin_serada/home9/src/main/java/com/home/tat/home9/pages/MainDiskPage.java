package com.home.tat.home9.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

import com.home.tat.home9.utils.WebDriverHelper;

public class MainDiskPage extends DiskPage {
	public static final String BASE_URL = "https://www.disk.yandex.ru";
	public static final String FILE_NAME = "testFile.txt";
	public static final By UPLOAD_BUTTON_LOCATOR = By
			.xpath("//input[@class='button__attach']");
	public static final By CLOSE_LINK_LOCATOR = By
			.xpath("//a[@data-click-action='dialog.close']");
	public static final By DOWNLOAD_BUTTON_LOCATOR = By
			.xpath("//div[contains(@class,'deselect ns-view-visible')]//ancestor::button[@data-click-action='resource.download']");
	public static final By TRASH_LINK_LOCATOR = By
			.xpath("//div[contains(@class,' slices ns-view-visible')]//div[@data-id='trash']//a[@href='/client/trash']");
	public static final String FILE_ELEMENT_LOCATOR_PATTERN = "//*[@class='nb-resource__name']//div[contains(., '%s')]";

	public MainDiskPage(WebDriver driver) {
		super(driver);
	}

	public void open() {
		driver.get(BASE_URL);
	}

	public MainDiskPage uploadFile(final String filePath) {

		final String SCRIPT_FOR_CLICK = "arguments[0].click();";
		WebElement uploadButton = driver.findElement(UPLOAD_BUTTON_LOCATOR);
		uploadButton.sendKeys(filePath);

		if (WebDriverHelper.getBrowserName().equals(
				WebDriverHelper.CHROME_BROWSER)) {
			((JavascriptExecutor) driver).executeScript(SCRIPT_FOR_CLICK,
					uploadButton);
		} else {
			uploadButton.click();
		}

		skipAlertMessage();
		driver.navigate().refresh();
		return this;
	}

	public MainDiskPage downloadFile(String fileName) {
		findAndClickFileElement(fileName);
		WebDriverHelper.waitForElementIsClickable(DOWNLOAD_BUTTON_LOCATOR);
		WebElement downloadButton = driver.findElement(DOWNLOAD_BUTTON_LOCATOR);
		downloadButton.click();
		WebDriverHelper.waitForFileDownloading(FILE_NAME);
		return this;
	}

	public MainDiskPage sendFilesToTrash(String... fileNames) {
		Actions action = new Actions(driver);
		WebDriverHelper.waitForElementIsClickable(TRASH_LINK_LOCATOR);
		WebElement trashElement = driver.findElement(TRASH_LINK_LOCATOR);
		WebElement lastFileElement = null;
		action.keyDown(Keys.CONTROL);
		for (String fileName : fileNames) {
			WebDriverHelper.waitForAppearence(By.xpath(String.format(
					FILE_ELEMENT_LOCATOR_PATTERN, fileName)));
			WebElement fileElement = driver.findElement(By.xpath(String.format(
					FILE_ELEMENT_LOCATOR_PATTERN, fileName)));
			action.click(fileElement);
			lastFileElement = fileElement;
		}

		dropElementToTarget(action, lastFileElement, trashElement);
		action.keyUp(Keys.CONTROL).build().perform();
		return this;
	}

	public MainDiskPage openFolder(String folderName) {
		WebDriverHelper.waitForAppearence(By.xpath(String.format(
				FILE_ELEMENT_LOCATOR_PATTERN, folderName)));
		WebElement folderElement = driver.findElement(By.xpath(String.format(
				FILE_ELEMENT_LOCATOR_PATTERN, folderName)));
		Action action = new Actions(driver).doubleClick(folderElement).build();
		action.perform();
		return this;
	}

	public void skipAlertMessage() {
		WebDriverHelper.waitForElementIsClickable(CLOSE_LINK_LOCATOR);
		WebElement confirmButton = driver.findElement(CLOSE_LINK_LOCATOR);
		confirmButton.click();
	}

	public void dropElementToTarget(Actions action, WebElement sourceElement,
			WebElement targetElement) {

		action.dragAndDropBy(sourceElement, targetElement.getLocation().getX()
				- sourceElement.getLocation().getX(), targetElement
				.getLocation().getY() - sourceElement.getLocation().getY());

	}
}
