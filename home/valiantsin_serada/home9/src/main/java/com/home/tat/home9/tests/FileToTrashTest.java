package com.home.tat.home9.tests;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.home.tat.home9.pages.MainDiskPage;
import com.home.tat.home9.pages.TrashPage;

public class FileToTrashTest extends BaseTest {
	public static final String UPLOAD_FILE_PATH = "D:\\uploads\\testFile.txt";
	public static final String FILE_NAME = "testFile.txt";
	public static final String FOLDER_NAME = "TextFiles";
	MainDiskPage diskPage;
	TrashPage trashPage;

	@Test
	public void uploadTest() {
		diskPage = login();
		diskPage.openFolder(FOLDER_NAME);
		diskPage.uploadFile(UPLOAD_FILE_PATH);
		Assert.assertTrue(diskPage.isFilePresent(FILE_NAME));
	}

	@Test(dependsOnMethods = { "uploadTest" })
	public void sendToTrashTest() {
		diskPage.sendFilesToTrash(FILE_NAME);
		trashPage = new TrashPage(driver);
		trashPage.open();
		Assert.assertTrue(trashPage.isFilePresent(FILE_NAME));
	}

	@Test(dependsOnMethods = { "sendToTrashTest" })
	public void deleteFileTest() {
		trashPage.deleteFilePermanently(FILE_NAME);
		Assert.assertFalse(trashPage.isFilePresent(FILE_NAME));
	}
}
