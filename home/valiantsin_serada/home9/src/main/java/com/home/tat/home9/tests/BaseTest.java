package com.home.tat.home9.tests;

import java.net.MalformedURLException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

import com.home.tat.home9.pages.LoginPage;
import com.home.tat.home9.pages.MainDiskPage;
import com.home.tat.home9.utils.WebDriverHelper;

public class BaseTest {
	public final String userLogin = "tat-test-user@yandex.ru";
	public final String userPassword = "tat-123qwe";
	protected WebDriver driver;

	@Parameters("browser")
	@BeforeClass
	public void prepareBrowser(String browser) throws MalformedURLException {
		driver = WebDriverHelper.getWebDriver(browser);
	}

	public MainDiskPage login() {
		LoginPage loginPage = new LoginPage(driver);
		loginPage.open();
		return loginPage.loginAs(userLogin, userPassword);
	}

	@AfterClass
	public void closeBrowser() {
		WebDriverHelper.shutdownWebDriver();
	}

}
