package com.home.tat.home7;

import java.net.MalformedURLException;

import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class SendUnfilledAddressFieldTest extends BaseMailTest {

	@Parameters("browser")
	@Test
	public void sendUnfilledAddressFieldTest(String browser)
			throws MalformedURLException {
		final String emptyAddress = "";
		loginMail(browser, userLogin, userPassword);
		sendMail(emptyAddress, mailSubject, mailContent);
		WebElement sendMailButton = driver
				.findElement(SEND_MAIL_BUTTON_LOCATOR);
		Assert.assertTrue(sendMailButton.isDisplayed());
	}

}
