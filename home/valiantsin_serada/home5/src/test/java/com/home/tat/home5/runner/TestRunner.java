package com.home.tat.home5.runner;

import java.util.ArrayList;
import java.util.List;

import org.testng.TestListenerAdapter;
import org.testng.TestNG;
import org.testng.xml.XmlSuite;

import com.home.tat.home5.listeners.TestMethodFailListener;

public class TestRunner {

	public static void main(String[] args) {
		final String NAME_XML_SUITE = "TmpSuite";
		final String PATH_FILES_FOUND_SUITE = "./src/test/resources/FilesFoundSuite.xml";
		final String PATH_VERIFICATION_SUITE = "./src/test/resources/VerificationSuite.xml";
		TestListenerAdapter tla = new TestListenerAdapter();
		TestNG tng = new TestNG();
		tng.addListener(tla);
		tng.addListener(new TestMethodFailListener());
		XmlSuite suite = new XmlSuite();
		suite.setName(NAME_XML_SUITE);
		List<String> files = new ArrayList<String>();
		files.addAll(new ArrayList<String>() {
			{
				add(PATH_FILES_FOUND_SUITE);
				add(PATH_VERIFICATION_SUITE);
			}
		});
		suite.setSuiteFiles(files);
		suite.setParallel(XmlSuite.ParallelMode.METHODS);
		suite.setThreadCount(4);
		List<XmlSuite> suites = new ArrayList<XmlSuite>();
		suites.add(suite);
		tng.setXmlSuites(suites);
		tng.run();
	}
}
