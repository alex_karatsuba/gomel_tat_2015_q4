package com.home.tat.home5;

import java.io.File;
import java.util.List;
import java.util.Map;

import junit.framework.Assert;

import org.testng.annotations.Test;

public class CorrectNumberValuesTest extends WordListTest {
	@Test(groups = "numberCount")
	public void checkNumberFiles() {
		final String FAIL_MESSAGE = "Number files wrong";
		List<File> files = wordList.getFiles(SOURCE_PATH,
				wordList.FILE_NAME_PATTERN);
		int numberFiles = files.size();
		Assert.assertEquals("Number files wrong", numberFiles, 1);
	}

	@Test(groups = "numberCount")
	public void checkNumberStrings() {
		final String FAIL_MESSAGE = "Number strings wrong";
		List<String> listStrings = getListStrings();
		int numberOfStrings = listStrings.size();
		int checkNumberOfStrings = testStringList.size();
		Assert.assertEquals("Number strings wrong", numberOfStrings,
				checkNumberOfStrings);
	}

	@Test(groups = "numberCount")
	public void checkNumberPositions() {
		final String FAIL_MESSAGE = "Number Positions wrong";
		List<String> listStrings = getListStrings();
		Map<String, Integer> statMap = wordList.fillMapStatistics(listStrings);
		int numberPositions = statMap.size();
		int checkNumberPositions = testMap.size();
		Assert.assertEquals(FAIL_MESSAGE, numberPositions, checkNumberPositions);
	}

	private List<String> getListStrings() {
		List<File> files = wordList.getFiles(SOURCE_PATH,
				wordList.FILE_NAME_PATTERN);
		List<String> listStrings = wordList.parseFilesToListStrings(files);
		return listStrings;
	}

}
