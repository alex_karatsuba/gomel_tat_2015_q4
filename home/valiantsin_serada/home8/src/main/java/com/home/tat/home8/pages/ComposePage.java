package com.home.tat.home8.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.home.tat.home8.entities.Letter;
import com.home.tat.home8.utils.WebDriverHelper;

public class ComposePage extends Page {
	public static final By COMPOSE_BUTTON_LOCATOR = By
			.xpath("//a[@href='#compose']");
	public static final By TO_INPUT_LOCATOR = By
			.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
	public static final By SUBJECT_INPUT_LOCATOR = By.name("subj");
	public static final By MAIL_TEXT_LOCATOR = By.id("compose-send");
	public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
	public static final By UNFILLED_ADDRESS_FIELD_ALERT_LOCATOR = By
			.xpath("//img[@class='b-mail-icon b-mail-icon_error']");
	public static final By BUTTON_SAVE_LOCATOR = By
			.xpath("//button[@data-action='dialog.save']");
	public static final By NEW_MESSAGE_LOCATOR = By
			.xpath("//a[@class='b-statusline__link']");
	public static final By POPUP_TABLE_LOCATOR = By
			.xpath("//a[@data-action='delete']");
	public static final By DRAFTBOX_LINK_LOCATOR = By
			.xpath("//a[@href='#draft']");
	public static final By DELETE_BUTTON_LOCATOR = By
			.xpath("//a[@data-action='compose.delete']");
	public static final int TIME_FOR_FIND_ELEMENT = 5000;
	public static final int ID_LENGTH = 19;
	public static final String HREF_ATRIBUTE = "href";

	public ComposePage(WebDriver driver) {
		super(driver);

	}

	public void open() {
		WebDriverHelper.waitForElementIsClickable(COMPOSE_BUTTON_LOCATOR);
		WebElement composeButton = driver.findElement(COMPOSE_BUTTON_LOCATOR);
		composeButton.click();
	}

	public ComposePage typeAddress(String address) {
		WebElement toInput = driver.findElement(TO_INPUT_LOCATOR);
		toInput.sendKeys(address);
		return this;
	}

	public ComposePage typeSubject(String subject) {
		WebElement toInput = driver.findElement(SUBJECT_INPUT_LOCATOR);
		toInput.sendKeys(subject);
		return this;
	}

	public ComposePage typeContentText(String contentText) {
		WebElement toInput = driver.findElement(MAIL_TEXT_LOCATOR);
		toInput.sendKeys(contentText);
		return this;
	}

	public InboxPage sendMail(Letter letter) {
		WebElement sendMailButton = driver
				.findElement(SEND_MAIL_BUTTON_LOCATOR);
		sendMailButton.click();
		WebDriverHelper.waitForDisappear(SEND_MAIL_BUTTON_LOCATOR);
		setIdLetter(letter);
		return new InboxPage(driver);
	}

	public InboxPage sendLetter(Letter letter) {
		typeAddress(letter.getAddress());
		typeSubject(letter.getSubject());
		typeContentText(letter.getBody());
		return sendMail(letter);
	}

	public DraftboxPage createDraft(Letter letter) {
		typeAddress(letter.getAddress());
		typeSubject(letter.getSubject());
		typeContentText(letter.getBody());
		WebDriverHelper.waitForElementIsClickable(DRAFTBOX_LINK_LOCATOR);
		WebElement draftboxLink = driver.findElement(DRAFTBOX_LINK_LOCATOR);
		draftboxLink.click();
		skipAlertMessage();
		return new DraftboxPage(driver);
	}

	public ComposePage sendUnAddressedLetter(Letter letter) {
		typeSubject(letter.getSubject());
		typeContentText(letter.getBody());
		WebElement sendMailButton = driver
				.findElement(SEND_MAIL_BUTTON_LOCATOR);
		sendMailButton.click();
		return this;
	}

	public boolean isUnfilledAddressFieldAlertExist() {
		try {
			WebDriverHelper
					.waitForDisappear(UNFILLED_ADDRESS_FIELD_ALERT_LOCATOR);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public void skipAlertMessage() {
		WebDriverHelper.waitForElementIsClickable(BUTTON_SAVE_LOCATOR);
		if (WebDriverHelper.isElementExist(POPUP_TABLE_LOCATOR)) {
			WebElement savechangesButton = driver
					.findElement(BUTTON_SAVE_LOCATOR);
			savechangesButton.click();
		}
	}

	public DraftboxPage deleteDraft() {
		WebDriverHelper.waitForElementIsClickable(DELETE_BUTTON_LOCATOR);
		WebElement deleteButton = driver.findElement(DELETE_BUTTON_LOCATOR);
		deleteButton.click();

		return new DraftboxPage(driver);
	}

	public void setIdLetter(Letter letter) {
		WebDriverHelper.waitForElementIsClickable(NEW_MESSAGE_LOCATOR);
		WebElement newMessageAlert = driver.findElement(NEW_MESSAGE_LOCATOR);
		String hrefNewMailLink = newMessageAlert.getAttribute(HREF_ATRIBUTE);
		String idNewMessage = hrefNewMailLink.substring(hrefNewMailLink
				.length() - ID_LENGTH);
		letter.setId(idNewMessage);

	}

}
