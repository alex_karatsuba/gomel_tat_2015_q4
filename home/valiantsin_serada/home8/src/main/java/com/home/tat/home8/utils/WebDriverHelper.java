package com.home.tat.home8.utils;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WebDriverHelper {
	public static final String REMOTE_SERVER_ADDRESS = "http://localhost:4444/wd/hub";
	public static final String BROWSER_QUIT_ERROR_MESSAGE = "Problem with shutting down driver:";
	public static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 20;
	public static final String CHROME_BROWSER = "CHROME";
	public static final String FIREFOX_BROWSER = "FIREFOX";
	public static final int DRIVE_IMPL_WAIT_TIMEOUT_SECONDS = 5;
	public static final int TIME_FOR_FIND_ELEMENT = 5;

	private static WebDriver driver;

	public static WebDriver getWebDriver(String browser)
			throws MalformedURLException {
		if (driver == null) {
			if (browser.equals(CHROME_BROWSER)) {
				driver = new RemoteWebDriver(new URL(REMOTE_SERVER_ADDRESS),
						DesiredCapabilities.chrome());
			} else
				driver = new RemoteWebDriver(new URL(REMOTE_SERVER_ADDRESS),
						DesiredCapabilities.firefox());
			driver.manage().window().maximize();
			driver.manage()
					.timeouts()
					.pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS,
							TimeUnit.SECONDS);
			driver.manage()
					.timeouts()
					.implicitlyWait(DRIVE_IMPL_WAIT_TIMEOUT_SECONDS,
							TimeUnit.SECONDS);
		}
		return driver;
	}

	public static void shutdownWebDriver() {
		try {
			driver.quit();
			driver = null;
		} catch (Exception e) {
			System.err.println(BROWSER_QUIT_ERROR_MESSAGE + e.getMessage());
		}
	}

	public static WebElement waitForElementIsClickable(By locator) {
		new WebDriverWait(driver, TIME_FOR_FIND_ELEMENT)
				.until(ExpectedConditions.elementToBeClickable(locator));
		return driver.findElement(locator);
	}

	public static WebElement waitForDisappear(By locator) {
		new WebDriverWait(driver, TIME_FOR_FIND_ELEMENT)
				.until(ExpectedConditions.invisibilityOfElementLocated(locator));
		return driver.findElement(locator);
	}

	public static WebElement waitForAppearence(By locator) {
		new WebDriverWait(driver, TIME_FOR_FIND_ELEMENT)
				.until(ExpectedConditions.visibilityOfElementLocated(locator));
		return driver.findElement(locator);
	}

	public static boolean isElementExist(By locator) {
		try {
			new WebDriverWait(driver, TIME_FOR_FIND_ELEMENT)
					.until(ExpectedConditions
							.invisibilityOfElementLocated(locator));
		} catch (Exception e) {
			return false;
		}
		return true;
	}

}
