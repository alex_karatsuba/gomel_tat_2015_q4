package com.home.tat.home8.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage extends Page {
	public static final String BASE_URL = "http://www.ya.ru";
	public static final By ENTER_BUTTON_LOCATOR = By
			.xpath("//a[contains(@href, 'mail.yandex')]");
	public static final By LOGIN_INPUT_LOCATOR = By.name("login");
	public static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");

	public LoginPage(WebDriver driver) {
		super(driver);
	}

	public void open() {
		driver.get(BASE_URL);
		WebElement enterButton = driver.findElement(ENTER_BUTTON_LOCATOR);
		enterButton.click();
	}

	public LoginPage typeUsername(String username) {
		driver.findElement(LOGIN_INPUT_LOCATOR).sendKeys(username);
		return this;
	}

	public LoginPage typePassword(String password) {
		driver.findElement(PASSWORD_INPUT_LOCATOR).sendKeys(password);
		return this;
	}

	public InboxPage submitLogin() {
		driver.findElement(LOGIN_INPUT_LOCATOR).submit();
		return new InboxPage(driver);
	}

	public PassportPage submitLoginExpectingFailure(String username,
			String password) {
		typeUsername(username);
		typePassword(password);
		driver.findElement(LOGIN_INPUT_LOCATOR).submit();
		return new PassportPage(driver);
	}

	public InboxPage loginAs(String username, String password) {
		typeUsername(username);
		typePassword(password);
		return submitLogin();
	}

}
