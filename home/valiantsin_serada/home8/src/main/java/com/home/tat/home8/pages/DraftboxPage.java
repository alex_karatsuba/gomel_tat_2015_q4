package com.home.tat.home8.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.home.tat.home8.entities.Letter;
import com.home.tat.home8.utils.WebDriverHelper;

public class DraftboxPage extends Page {
	public static final By DRAFTBOX_LINK_LOCATOR = By
			.xpath("//a[@href='#draft']");
	public static final By STATUSLINE_MESSAGE_LOCATOR = By
			.xpath("//div[@class='b-statusline']");
	public static final By TRASHBOX_LINK_LOCATOR = By
			.xpath("//a[@href='#trash']");
	public static final String MAIL_LINK_LOCATOR_PATTERN = "//*[@class='block-messages']//a[contains(., '%s')]";
	final String HREF_ATRIBUTE = "href";

	public DraftboxPage(WebDriver driver) {
		super(driver);
	}

	public void open() {
		WebDriverHelper.waitForElementIsClickable(DRAFTBOX_LINK_LOCATOR)
				.click();
	}

	public ComposePage findAndShowCreatedDraft(Letter letter) {
		WebElement draftboxMailLink = driver.findElement(By.xpath(String
				.format(MAIL_LINK_LOCATOR_PATTERN, letter.getBody())));
		driver.get(draftboxMailLink.getAttribute(HREF_ATRIBUTE));
		return new ComposePage(driver);
	}

	public TrashboxPage switchToTrashboxAfterDeleting(Letter letter) {
		WebDriverHelper.waitForDisappear(By.xpath(String.format(
				MAIL_LINK_LOCATOR_PATTERN, letter.getBody())));
		WebDriverHelper.waitForAppearence(STATUSLINE_MESSAGE_LOCATOR);
		WebDriverHelper.waitForElementIsClickable(TRASHBOX_LINK_LOCATOR);
		WebElement trashboxLink = driver.findElement(TRASHBOX_LINK_LOCATOR);
		trashboxLink.click();
		return new TrashboxPage(driver);
	}

	public boolean isLetterPresent(Letter letter) {
		try {
			WebDriverHelper.waitForAppearence(By.xpath(String.format(
					MAIL_LINK_LOCATOR_PATTERN, letter.getSubject())));
		} catch (Exception e) {
			return false;
		}
		return true;
	}

}
