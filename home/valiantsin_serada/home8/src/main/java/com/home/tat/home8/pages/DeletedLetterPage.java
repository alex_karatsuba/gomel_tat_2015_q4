package com.home.tat.home8.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.home.tat.home8.utils.WebDriverHelper;

public class DeletedLetterPage extends Page {
	public static final By PERMANENTDELETE_BUTTON_LOCATOR = By
			.xpath("//a[@data-action='delete']");

	public DeletedLetterPage(WebDriver driver) {
		super(driver);
	}

	public TrashboxPage deleteLetter() {
		WebDriverHelper
				.waitForElementIsClickable(PERMANENTDELETE_BUTTON_LOCATOR);
		WebElement permanentDeleteButton = driver
				.findElement(PERMANENTDELETE_BUTTON_LOCATOR);
		permanentDeleteButton.click();
		return new TrashboxPage(driver);
	}

}
