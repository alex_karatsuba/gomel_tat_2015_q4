package com.home.tat.home8.runner;

import java.util.ArrayList;
import java.util.List;

import org.testng.TestNG;
import org.testng.xml.XmlSuite;

public class TestRunner {
	public static void main(String[] args) {
		final String NAME_XML_SUITE = "TmpSuite";
		final String CHROME_SUITE = "./src/main/resources/ChromeSuite.xml";
		final String FIREFOX_SUITE = "./src/main/resources/FireFoxSuite.xml";
		TestNG tng = new TestNG();
		XmlSuite suite = new XmlSuite();
		suite.setName(NAME_XML_SUITE);
		List<String> files = new ArrayList<String>();
		files.addAll(new ArrayList<String>() {
			{
				add(CHROME_SUITE);
				add(FIREFOX_SUITE);
			}
		});
		suite.setSuiteFiles(files);
		List<XmlSuite> suites = new ArrayList<XmlSuite>();
		suites.add(suite);
		tng.setXmlSuites(suites);
		tng.run();
	}

}
