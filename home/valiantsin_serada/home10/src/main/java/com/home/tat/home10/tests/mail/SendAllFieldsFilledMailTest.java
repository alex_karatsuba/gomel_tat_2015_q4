package com.home.tat.home10.tests.mail;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.home.tat.home10.lib.mail.builders.LetterBuilder;
import com.home.tat.home10.lib.mail.models.Letter;
import com.home.tat.home10.lib.mail.screens.ComposePage;
import com.home.tat.home10.lib.mail.screens.InboxPage;

public class SendAllFieldsFilledMailTest extends BaseTest {
	private Letter letter;

	@BeforeClass
	public void prepareData() {
		letter = LetterBuilder.getRandomLetter();
	}

	@Test
	public void sendMail() {
		InboxPage inboxPage = login();
		ComposePage composePage = new ComposePage(driver);
		composePage.open();
		inboxPage = composePage.sendLetter(letter);
		inboxPage.open();
		Assert.assertTrue(inboxPage.isLetterPresent(letter));
	}
}
