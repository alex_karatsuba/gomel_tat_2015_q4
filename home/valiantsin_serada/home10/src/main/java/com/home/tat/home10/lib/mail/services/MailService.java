package com.home.tat.home10.lib.mail.services;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.home.tat.home10.lib.common.utils.WebDriverHelper;
import com.home.tat.home10.lib.mail.models.Letter;
import com.home.tat.home10.lib.mail.screens.bloks.ComposeForm;

public class MailService {

	public static void sendMail(ComposeForm composeForm, Letter letter) {
		composeForm.getAddressInput().sendKeys(letter.getAddress());
		composeForm.getSubjectInput().sendKeys(letter.getSubject());
		composeForm.getContentMailInput().sendKeys(letter.getBody());
		composeForm.getSubmitButton().click();
	}

	public static void skipAlert(WebDriver driver, By locatorPopupPanel,
			By locatorSaveButton) {
		WebDriverHelper.waitForElementIsClickable(locatorSaveButton);
		if (WebDriverHelper.isElementExist(locatorPopupPanel)) {

			WebElement savechangesButton = driver
					.findElement(locatorSaveButton);
			savechangesButton.click();
		}
	}

	public static void setIdLetter(WebDriver driver, By messageLocator,
			Letter letter) {
		final int ID_LENGTH = 19;
		final String HREF_ATRIBUTE = "href";
		WebDriverHelper.waitForElementIsClickable(messageLocator);
		WebElement newMessageAlert = driver.findElement(messageLocator);
		String hrefNewMailLink = newMessageAlert.getAttribute(HREF_ATRIBUTE);
		String idNewMessage = hrefNewMailLink.substring(hrefNewMailLink
				.length() - ID_LENGTH);
		letter.setId(idNewMessage);
	}

	public static boolean isLetterPresent(String mailLinkLocatorPattern,
			Letter letter) {
		try {
			WebDriverHelper.waitForAppearence(By.xpath(String.format(
					mailLinkLocatorPattern, letter.getId())));
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public static boolean checkErronOnEmptyAddress(
			String errorMessageLocatorPattern, String errorMessage) {
		try {
			WebDriverHelper.waitForAppearence(By.xpath(String.format(
					errorMessageLocatorPattern, errorMessage)));
		} catch (Exception e) {
			return false;
		}
		return true;
	}
}
