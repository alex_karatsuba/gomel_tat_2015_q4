package com.home.tat.home10.lib.common.constants;

public interface CommonConstants {
	String MAILBOX_URL = "http://www.ya.ru";
	String BASE_YANDEX_URL = "http://www.ya.ru";
	String PASSPORTPAGE_BASE_URL = "https://passport.yandex.ru";
	String DEFAULT_MAIL_USER_LOGIN = "tat-test-user@yandex.ru";
	String DEFAULT_MAIL_USER_PASSWORD = "tat-123qwe";
	String DEFAULT_MAIL_TO_SEND = "tat-test-user@yandex.ru";

	String PASSPORT_PAGE_ERROR_MESSAGE = "Нет аккаунта с таким логином.";
	String EMPTY_ADDRESS_ERROR_MESSAGE = "Поле не заполнено. Необходимо ввести адрес.";
}
