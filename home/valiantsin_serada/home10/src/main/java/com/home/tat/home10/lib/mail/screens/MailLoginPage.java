package com.home.tat.home10.lib.mail.screens;

import static com.home.tat.home10.lib.common.constants.CommonConstants.BASE_YANDEX_URL;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import ru.yandex.qatools.htmlelements.loader.HtmlElementLoader;

import com.home.tat.home10.lib.common.models.Account;
import com.home.tat.home10.lib.common.screens.Page;
import com.home.tat.home10.lib.mail.screens.bloks.LoginForm;
import com.home.tat.home10.lib.mail.services.LoginService;

public class MailLoginPage extends Page {
	public static final By ENTER_BUTTON_LOCATOR = By
			.xpath("//a[contains(@href, 'mail.yandex')]");
	public static final By LOGIN_INPUT_LOCATOR = By.name("login");
	public static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");

	private LoginForm loginForm;

	public MailLoginPage(WebDriver driver) {
		super(driver);
		HtmlElementLoader.populatePageObject(this, driver);
	}

	public void open() {
		driver.get(BASE_YANDEX_URL);
		WebElement enterButton = driver.findElement(ENTER_BUTTON_LOCATOR);
		enterButton.click();
	}

	public InboxPage loginAs(Account account) {
		LoginService.loginMailbox(loginForm, account);
		return PageFactory.initElements(driver, InboxPage.class);
	}

	public PassportPage submitLoginExpectingFailure(Account account) {
		LoginService.loginMailbox(loginForm, account);
		return PageFactory.initElements(driver, PassportPage.class);
	}

}
