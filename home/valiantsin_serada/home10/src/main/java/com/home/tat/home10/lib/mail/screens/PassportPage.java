package com.home.tat.home10.lib.mail.screens;

import static com.home.tat.home10.lib.common.constants.CommonConstants.PASSPORTPAGE_BASE_URL;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.home.tat.home10.lib.common.screens.Page;
import com.home.tat.home10.lib.mail.services.LoginService;

public class PassportPage extends Page {
	public static final By ERROR_MESSAGE_ELEMENT_LOCATOR = By
			.xpath("//div[@class='error-msg']");

	public PassportPage(WebDriver driver) {
		super(driver);
	}

	public void open() {
		driver.get(PASSPORTPAGE_BASE_URL);
	}

	public boolean checkErrorMessageDisplayed(String message) {

		return LoginService.checkErrorOnFailedLogin(driver,
				ERROR_MESSAGE_ELEMENT_LOCATOR, message);

	}

}
