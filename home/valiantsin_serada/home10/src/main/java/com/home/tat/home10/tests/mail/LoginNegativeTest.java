package com.home.tat.home10.tests.mail;

import static com.home.tat.home10.lib.common.constants.CommonConstants.PASSPORT_PAGE_ERROR_MESSAGE;

import org.junit.Assert;
import org.testng.annotations.Test;

import com.home.tat.home10.lib.common.builders.AccountBuilder;
import com.home.tat.home10.lib.common.models.Account;
import com.home.tat.home10.lib.mail.screens.MailLoginPage;
import com.home.tat.home10.lib.mail.screens.PassportPage;

public class LoginNegativeTest extends BaseTest {

	@Test
	public void loginNegativeTest() {
		MailLoginPage loginPage = new MailLoginPage(driver);
		loginPage.open();
		Account account = AccountBuilder.getAccountWithWrongLogin();
		PassportPage passportPage = loginPage
				.submitLoginExpectingFailure(account);
		Assert.assertTrue(passportPage
				.checkErrorMessageDisplayed(PASSPORT_PAGE_ERROR_MESSAGE));
	}

}
