package tests;

import framework.YandexMail;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class DraftMailTest extends LoginTest {

    public static final String SUBJ = "a1s2d3";

    @Test(description = "Create and delete draft mail", dependsOnMethods = "correctLogin")
    public void createAndDeleteDraftMail() {
        waitForElementPresent(YandexMail.DRAFT_LOCATOR);
        driver.findElement(YandexMail.DRAFT_LOCATOR).click();
        driver.findElement(YandexMail.WRITE_MESSAGE_BUTTON_LOCATOR).click();
        driver.findElement(YandexMail.SUBJECT_LOCATOR).sendKeys(SUBJ);
        driver.manage().timeouts().implicitlyWait(35, TimeUnit.SECONDS);
        driver.findElement(YandexMail.BODY_MAIL_LOCATOR).sendKeys("Blah blah blah");

        driver.findElement(YandexMail.DRAFT_LOCATOR).click();
        if (driver.findElement(YandexMail.POPUP_LOCATOR).isDisplayed()) {
            driver.findElement(YandexMail.POPUP_BUTTON_LOCATOR).click();
        }
        driver.findElement(YandexMail.DRAFT_LOCATOR).click();

        String mailPattern = "//*[@title='" + SUBJ + "']/../../../../../../label/input[@type='checkbox']";
        driver.findElement(By.xpath(mailPattern)).click();
        driver.findElement(YandexMail.DELETE_MESSAGE_BUTTON_LOCATOR).click();
        driver.findElement(YandexMail.TRASH_LOCATOR).click();
        Assert.assertTrue(driver.findElement(By.xpath(mailPattern)).isEnabled());
    }

    @Test
    public void deleteFromTrash() {
        driver.findElement(YandexMail.TRASH_LOCATOR).click();
        String mailPattern = "//*[@title='" + SUBJ + "']/../../../../../../label/input[@type='checkbox']";
        driver.findElement(By.xpath(mailPattern)).click();
        driver.findElement(YandexMail.DELETE_MESSAGE_BUTTON_LOCATOR).click();
        Assert.assertFalse(driver.findElement(By.xpath(mailPattern)).isEnabled());
    }
}
