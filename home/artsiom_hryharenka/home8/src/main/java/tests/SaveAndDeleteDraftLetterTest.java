package tests;

import org.testng.Assert;
import org.testng.annotations.Test;


public class SaveAndDeleteDraftLetterTest extends LoginTest {

    @Test(description = "Create and delete draft mail")
    public void saveAndDeleteDraftLetter() {
        Assert.assertTrue(mainPage.saveAndDeleteDraftLetter());
    }

}
