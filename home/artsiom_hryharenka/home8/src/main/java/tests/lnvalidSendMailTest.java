package tests;

import pages.YandexMail;
import org.testng.Assert;
import org.testng.annotations.Test;


public class lnvalidSendMailTest extends LoginTest {

    @Test
    public void sendWrongMessage(){
        waitForElementPresent(YandexMail.WRITE_MESSAGE_BUTTON_LOCATOR);
        driver.findElement(YandexMail.WRITE_MESSAGE_BUTTON_LOCATOR).click();
        driver.findElement(YandexMail.SEND_MAIL_BUTTON_LOCATOR).click();
        Assert.assertEquals(driver.findElement(YandexMail.ERROR_MESSAGE_LOCATOR).getText(),
                                                "Поле не заполнено. Необходимо ввести адрес.");
    }
}
