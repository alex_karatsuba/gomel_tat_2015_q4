package tests;

import org.testng.Assert;
import org.testng.annotations.Test;

public class InvalidLoginTest extends BaseTest{

    @Test
    public void wrongLogin() {
        Assert.assertTrue(loginPage.isErrorDisplayed());
    }
}
