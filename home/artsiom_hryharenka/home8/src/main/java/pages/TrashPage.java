package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class TrashPage extends MainPage {

    public static final By DELETE_MESSAGE_BUTTON_LOCATOR = By.xpath("//div[@class='block-toolbar']//a[@data-action='delete']");

    public TrashPage(WebDriver driver) {
        super(driver);
    }

    public void clickDeleteButton() {
        driver.findElement(DELETE_MESSAGE_BUTTON_LOCATOR).click();
    }

    public void markLetter(String subj) {
        String mailPattern = "//*[@title='" + subj + "']/../../../../../../label/input[@type='checkbox']";
        waitForElementIsClickable(By.xpath(mailPattern));
        driver.findElement(By.xpath(mailPattern)).click();
    }

    public boolean isPresentLetter(String subj) {
        String mailPattern = "//*[@title='" + subj + "']/../../../../../../label/input[@type='checkbox']";
        //waitForElementIsClickable(By.xpath(mailPattern));
        return driver.findElement(By.xpath(mailPattern)).isDisplayed();
    }
}
