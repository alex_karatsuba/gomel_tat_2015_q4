package runner;

import listeners.CustomListener;
import org.testng.TestListenerAdapter;
import org.testng.TestNG;
import org.testng.xml.XmlSuite;

import java.util.ArrayList;
import java.util.List;

public class TestRunner {
    public static void main(String[] args) {
        TestListenerAdapter listenerAdapter = new TestListenerAdapter();
        TestNG testNG = new TestNG();
        testNG.addListener(listenerAdapter);
        testNG.addListener(new CustomListener());
        XmlSuite suite = new XmlSuite();
        suite.setName("TmpSuite");
        List<String> files = new ArrayList<String>(
        );
        files.addAll(new ArrayList<String>() {{
            add("./src/test/resources/suites/allTests.xml");
            //add("./src/test/resources/suites/parallel.xml");
        }});
        suite.setSuiteFiles(files);
        suite.setParallel(XmlSuite.ParallelMode.METHODS);
        suite.setThreadCount(4);
        List<XmlSuite> suites = new ArrayList<XmlSuite>();
        suites.add(suite);
        testNG.setXmlSuites(suites);
        testNG.run();
    }
}
