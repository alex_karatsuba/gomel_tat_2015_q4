package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import utils.GenerateFileHelper;


public class TrashPage extends TopMenuPage {

    public static final String FIRST_FILE_LOCATOR = "//div[contains(@data-id, 'trash/" + GenerateFileHelper.getFirstFileName() + "')]";
    public static final String SECOND_FILE_LOCATOR = "//div[contains(@data-id, 'trash/" + GenerateFileHelper.getSecondFileName() + "')]";

    public TrashPage(WebDriver driver) {
        super(driver);
    }

    public DetailsPage markFile() {
        waitForElementIsClickable(By.xpath(FIRST_FILE_LOCATOR));
        driver.findElement(By.xpath(FIRST_FILE_LOCATOR)).click();
        return new DetailsPage(driver);
    }

    public boolean isFilePresent() {
        waitForElementIsClickable(By.xpath(FIRST_FILE_LOCATOR));
        WebElement element = driver.findElement(By.xpath(FIRST_FILE_LOCATOR));
        return element.getText().equals(GenerateFileHelper.getFirstFileName());
    }

    public boolean isFilesPresent() {
        waitForElementIsClickable(By.xpath(FIRST_FILE_LOCATOR));
        WebElement element1 = driver.findElement(By.xpath(FIRST_FILE_LOCATOR));
        waitForElementIsClickable(By.xpath(SECOND_FILE_LOCATOR));
        WebElement element2 = driver.findElement(By.xpath(SECOND_FILE_LOCATOR));
        return element2.getText().equals(GenerateFileHelper.getSecondFileName()) && element1.getText().equals(GenerateFileHelper.getFirstFileName());
    }
}
