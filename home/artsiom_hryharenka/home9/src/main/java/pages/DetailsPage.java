package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utils.GenerateFileHelper;

import java.io.File;

public class DetailsPage extends FilesPage{

    public static final By DOWNLOAD_BUTTON_LOCATOR = By.xpath("//*[@data-click-action='resource.download']");
    public static final By DELETE_BUTTON_LOCATOR = By.xpath("//div[contains(@class, 'b-aside js-prevent-deselect ns-view-visible')]//*[@data-click-action='resource.delete']");
    public static final By RESTORE_BUTTON_LOCATOR = By.xpath("//div[contains(@class, 'b-aside js-prevent-deselect ns-view-visible')]//*[@data-click-action='resource.restore']");

    public DetailsPage(WebDriver driver) {
        super(driver);
    }

    public void download() {
        waitForElementIsClickable(DOWNLOAD_BUTTON_LOCATOR);
        driver.findElement(DOWNLOAD_BUTTON_LOCATOR).click();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException ex) {
            throw new RuntimeException(ex.getMessage());
        }
    }

    public void remove() {
        waitForElementIsClickable(DELETE_BUTTON_LOCATOR);
        driver.findElement(DELETE_BUTTON_LOCATOR).click();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException ex) {
            throw new RuntimeException(ex.getMessage());
        }
    }

    public void restore() {
        waitForElementIsClickable(RESTORE_BUTTON_LOCATOR);
        driver.findElement(RESTORE_BUTTON_LOCATOR).click();
        try {
            Thread.sleep(10000);
        } catch (InterruptedException ex) {
            throw new RuntimeException(ex.getMessage());
        }
    }

    public boolean isSuccessfulDownload() {
        File file = new File(GenerateFileHelper.getDownloadDirectoryPath(), GenerateFileHelper.getFirstFileName());
        try {
            while(!file.canWrite()){
                Thread.sleep(1000);
            }
            return true;
        } catch (InterruptedException ex) {
            throw new RuntimeException(ex.getMessage());
        }

    }
}
