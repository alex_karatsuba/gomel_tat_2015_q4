package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage extends Page {
    public static final String YANDEX_START_PAGE = "https://disk.yandex.ru/";
    public static final String USER_NAME = "artsiom.hryharenka";
    public static final String PASSWORD = "3985209";

    public static final By INPUT_LOGIN_LOCATOR = By.xpath("//input[@name='login']");
    public static final By INPUT_PASSWORD_LOCATOR = By.xpath("//input[@name='password']");
    public static final By BUTTON_SUBMIT_LOCATOR = By.xpath("//button[@type='submit']");

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public void open(){
        driver.get(YANDEX_START_PAGE);
    }

    public TopMenuPage login(){
        waitForElementIsClickable(INPUT_LOGIN_LOCATOR);
        driver.findElement(INPUT_LOGIN_LOCATOR).sendKeys(USER_NAME);
        driver.findElement(INPUT_PASSWORD_LOCATOR).sendKeys(PASSWORD);
        waitForElementIsClickable(BUTTON_SUBMIT_LOCATOR);
        driver.findElement(BUTTON_SUBMIT_LOCATOR).click();
        return new TopMenuPage(driver);
    }

}
