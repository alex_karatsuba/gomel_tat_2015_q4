package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import utils.GenerateFileHelper;

import java.util.List;

public class FilesPage extends TopMenuPage {

    public static final By ALL_FILES_LOCATOR = By.xpath("//div[@class='nb-resource__text-name']");
    public static final String FIRST_FILE_LOCATOR = "//div[contains(@data-id, 'disk/" + GenerateFileHelper.getFirstFileName() + "')]";
    public static final String SECOND_FILE_LOCATOR = "//div[contains(@data-id, 'disk/" + GenerateFileHelper.getSecondFileName() + "')]";
    public static final By TRASH_PICTURE_LOCATOR = By.xpath("//div[@data-id='/trash']");
    //public static final By FILES_BUTTON_LOCATOR = By.cssSelector("div[data-id='trash'] a[href$='/trash']");

    public FilesPage(WebDriver driver) {
        super(driver);
    }

    public DetailsPage markFile() {
        waitForElementIsClickable(By.xpath(FIRST_FILE_LOCATOR));
        driver.findElement(By.xpath(FIRST_FILE_LOCATOR)).click();
        return new DetailsPage(driver);
    }

    public void removeFile() {
        waitForElementIsClickable(By.xpath(FIRST_FILE_LOCATOR));
        WebElement element = driver.findElement(By.xpath(FIRST_FILE_LOCATOR));
        waitForElementIsClickable(TRASH_PICTURE_LOCATOR);
        WebElement target = driver.findElement(TRASH_PICTURE_LOCATOR);
        (new Actions(driver)).dragAndDrop(element, target).perform();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException ex) {
            throw new RuntimeException(ex.getMessage());
        }
    }

    public void removeFiles() {
        waitForElementIsClickable(By.xpath(FIRST_FILE_LOCATOR));
        WebElement element1 = driver.findElement(By.xpath(FIRST_FILE_LOCATOR));
        waitForElementIsClickable(By.xpath(SECOND_FILE_LOCATOR));
        WebElement element2 = driver.findElement(By.xpath(SECOND_FILE_LOCATOR));

        Actions action = new Actions(driver);
        action.keyDown(Keys.CONTROL);

        action.moveToElement(element1).click();
        action.moveToElement(element2).click();

        action.keyUp(Keys.CONTROL).perform();
        waitForElementIsClickable(TRASH_PICTURE_LOCATOR);
        WebElement target = driver.findElement(TRASH_PICTURE_LOCATOR);
        (new Actions(driver)).dragAndDrop(element1, target).perform();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException ex) {
            throw new RuntimeException(ex.getMessage());
        }
    }

    public boolean isTrueFile() {
        waitForElementIsClickable(ALL_FILES_LOCATOR);
        List<WebElement> elementList = driver.findElements(ALL_FILES_LOCATOR);
        for (WebElement element : elementList) {
            if(element.getText().equals(GenerateFileHelper.getFirstFileName()))
                return true;
        }

        return false;
    }

}
