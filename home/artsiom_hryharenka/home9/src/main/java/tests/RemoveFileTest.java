package tests;

import org.testng.annotations.Test;
import pages.DetailsPage;
import pages.FilesPage;
import pages.TrashPage;

import static org.testng.Assert.assertTrue;

public class RemoveFileTest extends UploadFileTest {

    @Test(priority = 1)
    public void removeFile() {
        FilesPage filesPage = topMenuPage.openFilesPage();
        filesPage.removeFile();
        TrashPage trashPage = topMenuPage.openTrashPage();
        boolean isFilePresent = trashPage.isFilePresent();
        DetailsPage detailsPage = trashPage.markFile();
        detailsPage.remove();
        assertTrue(isFilePresent);
    }

}
