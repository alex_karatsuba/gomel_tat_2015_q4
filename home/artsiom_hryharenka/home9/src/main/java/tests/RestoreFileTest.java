package tests;

import org.testng.annotations.Test;
import pages.DetailsPage;
import pages.FilesPage;
import pages.TrashPage;

import static org.testng.Assert.assertTrue;

public class RestoreFileTest extends UploadFileTest {

    @Test(priority = 1)
    public void restoreFile() {
        FilesPage filesPage = new FilesPage(driver);
        filesPage.removeFile();
        TrashPage trashPage = topMenuPage.openTrashPage();
        DetailsPage detailsPage = trashPage.markFile();
        detailsPage.restore();
        filesPage = topMenuPage.openFilesPage();
        assertTrue(filesPage.isTrueFile());
    }
}
