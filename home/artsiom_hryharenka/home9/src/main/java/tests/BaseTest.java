package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterGroups;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeGroups;
import pages.LoginPage;
import pages.TopMenuPage;
import utils.GenerateFileHelper;

import java.net.MalformedURLException;
import java.net.URL;

public class BaseTest {
    protected static WebDriver driver;
    public static LoginPage loginPage;
    public static TopMenuPage topMenuPage;

    @BeforeClass(description = "Firefox remote launch")
    public void setUp() throws MalformedURLException{
        if (driver == null) {
            FirefoxProfile profile = new FirefoxProfile();
            profile.setPreference("browser.downloadFileTest.dir", GenerateFileHelper.getDownloadDirectoryPath());
            profile.setPreference("browser.downloadFileTest.manager.showWhenStarting", false);
            profile.setPreference("browser.downloadFileTest.folderList", 2);
            profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "text/plain");

            DesiredCapabilities capabilities = DesiredCapabilities.firefox();
            capabilities.setCapability(FirefoxDriver.PROFILE, profile);

            driver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), capabilities);
            driver.manage().window().maximize();

            loginPage = new LoginPage(driver);
            loginPage.open();
        }

    }

    @BeforeClass(description = "login", dependsOnMethods = "setUp")
    public void login() {
        topMenuPage = loginPage.login();
    }

    @BeforeGroups(groups = "data")
    public void createFile() {
        GenerateFileHelper.createFile();
    }

    @AfterGroups(groups = "data")
    public void removeFile() {
        GenerateFileHelper.removeDirectories();
    }

    @AfterClass
    public void tearDown() {
        try {
            driver.quit();
        } catch (Exception e) {
            System.err.println("Problem with shutting down driver: " + e.getMessage());
        } finally {
            driver = null;
        }
    }
}
