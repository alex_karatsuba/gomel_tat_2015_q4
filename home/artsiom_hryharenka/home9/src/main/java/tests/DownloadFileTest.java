package tests;

import junit.framework.Assert;
import org.testng.annotations.Test;
import pages.DetailsPage;
import pages.FilesPage;
import utils.GenerateFileHelper;

public class DownloadFileTest extends UploadFileTest{

    @Test(priority = 1, groups = "data")
    public void downloadFileTest() {
        FilesPage filesPage = new FilesPage(driver);
        DetailsPage detailsPage = filesPage.markFile();
        detailsPage.download();
        Assert.assertTrue(detailsPage.isSuccessfulDownload());
        GenerateFileHelper.removeDirectories();
    }
}
