package utils;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class GenerateFileHelper {

    private static final String FIRST_TEST_FILE_NAME = RandomStringUtils.randomAlphabetic(5) + ".txt";
    private static final String SECOND_TEST_FILE_NAME = RandomStringUtils.randomAlphabetic(5) + ".txt";
    private static final String UPLOAD_DIRECTORY_PATH = "E:\\UPLOAD\\";
    private static final String DOWNLOAD_DIRECTORY_PATH = "E:\\DOWNLOAD\\";

    public static String getFirstFileName() {
        return FIRST_TEST_FILE_NAME;
    }

    public static String getSecondFileName() {
        return SECOND_TEST_FILE_NAME;
    }

    public static String getUploadDirectoryPath() {
        return UPLOAD_DIRECTORY_PATH;
    }

    public static String getDownloadDirectoryPath() {
        return DOWNLOAD_DIRECTORY_PATH;
    }

    public static void createFile() {
            try {
                File folderD = new File(DOWNLOAD_DIRECTORY_PATH);
                folderD.mkdirs();

                File folder = new File(UPLOAD_DIRECTORY_PATH);
                folder.mkdirs();

                File fileTXT = new File(folder, FIRST_TEST_FILE_NAME);

                FileWriter writer = new FileWriter(fileTXT, false);
                writer.write(DOWNLOAD_DIRECTORY_PATH + FIRST_TEST_FILE_NAME);
                writer.flush();
                writer.close();

                fileTXT = new File(folder, SECOND_TEST_FILE_NAME);
                writer = new FileWriter(fileTXT, false);
                writer.write(DOWNLOAD_DIRECTORY_PATH + SECOND_TEST_FILE_NAME);
                writer.flush();
                writer.close();
            } catch (IOException ex) {
                throw new RuntimeException(ex.getMessage());
            }
    }

    public static void removeDirectories() {
        try {
            FileUtils.deleteDirectory(new File(DOWNLOAD_DIRECTORY_PATH));
            FileUtils.deleteDirectory(new File(UPLOAD_DIRECTORY_PATH));
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
