package com.gomel.tat.home5;

import com.gomel.tat.home4.WordList;
import org.testng.annotations.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by Vitali on 19.12.2015.
 */
public class BaseWordListTest {
    public WordList wordList;
    protected String startDir = "D:\\TempTest";
    int dirIndex = 0;
    public File testReadFile;

    public void makeTempDirsWithFiles() throws IOException {
        try {

            File TempDir = new File(startDir + "\\home3");
            TempDir.mkdirs();
            File TempDirWithSpace = new File(startDir + "\\home 3");
            TempDirWithSpace.mkdir();
            File TempDirNoMatch = new File(startDir + "\\home");
            TempDirNoMatch.mkdir();

            File TempDirHelloTXT = new File(TempDir.getPath() + "\\hello.txt");
            File TempDirWithSpaceHelloTXT = new File(TempDirWithSpace.getPath() + "\\hello.txt");
            File TempDirNoMatchHelloTXT = new File(TempDirNoMatch.getPath() + "\\hello.txt");

            File TempDirReadmeTXT = new File(TempDir.getPath() + "\\readme.txt");
            File TempDirWithSpaceReadmeTXT = new File(TempDirWithSpace.getPath() + "\\readme.txt");
            File TempDirNoMatchReadmeTXT = new File(TempDirNoMatch.getPath() + "\\readme.txt");

            String testText = "hello, world, I have suffered enough to keep my life within my mind. ___''==1!! !123 122";

            TempDirHelloTXT.createNewFile();
            TempDirWithSpaceHelloTXT.createNewFile();
            TempDirNoMatchHelloTXT.createNewFile();

            FileWriter hello3Writer = new FileWriter(TempDirHelloTXT.getPath());
            hello3Writer.write(testText);
            hello3Writer.close();

            FileWriter hello_3Writer = new FileWriter(TempDirWithSpaceHelloTXT.getPath());
            hello_3Writer.write(testText);
            hello_3Writer.close();

            FileWriter helloWriter = new FileWriter(TempDirNoMatchHelloTXT.getPath());
            helloWriter.write(testText);
            helloWriter.close();

            TempDirReadmeTXT.createNewFile();
            TempDirWithSpaceReadmeTXT.createNewFile();
            TempDirNoMatchReadmeTXT.createNewFile();

            FileWriter readme3Writer = new FileWriter(TempDirReadmeTXT.getPath());
            readme3Writer.write(testText);
            readme3Writer.close();

            FileWriter readme_3Writer = new FileWriter(TempDirWithSpaceReadmeTXT.getPath());
            readme_3Writer.write(testText);
            readme_3Writer.close();

            FileWriter readmeWriter = new FileWriter(TempDirNoMatchReadmeTXT.getPath());
            readmeWriter.write(testText);
            readmeWriter.close();

            File testReadFileDir = new File(startDir+"\\readFileTest");
            testReadFileDir.mkdirs();
            testReadFile = new File(startDir + "\\readFileTest\\readfiletest.txt");
            testReadFile.createNewFile();

            FileWriter fileWriter = new FileWriter(testReadFile);
            fileWriter.write("HELLO%$-hello");
            fileWriter.close();

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Somewhy cant create files and folders.");
        }
    }


    @BeforeSuite
    public void makeTempDirsWithFilesFinally() throws IOException {
        System.out.println("Making TempDirs before suite");
        while (new File(startDir).exists()) {
            dirIndex++;
            startDir = "C:\\TempTest" + dirIndex;
        }
        makeTempDirsWithFiles();
    }


    private void deleteTempDirWithFiles(File tempDir) {
        if (tempDir.isDirectory()) {
            if (tempDir.listFiles() != null) {
                for (File nexFile : tempDir.listFiles())
                    deleteTempDirWithFiles(nexFile);
                tempDir.delete();
            } else {
                tempDir.delete();
            }
        } else {
            tempDir.delete();
        }
    }

    @BeforeMethod
    public void setUp() {
        wordList = new WordList();
        System.out.println("Method Started");
    }

    @AfterSuite
    public void deleteTempDirWithFilesFinally() {
        System.out.println("Delete TempDirs after suite");
        deleteTempDirWithFiles(new File(startDir));
    }
}
