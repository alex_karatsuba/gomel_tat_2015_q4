package com.gomel.tat.home9.page.yandexmail;

import com.gomel.tat.home9.page.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static com.gomel.tat.home9.BrowserPreparation.StartBrowserPreparation.waitForElementIsClickable;

/**
 * Created by user on 27.12.2015.
 */
public class InboxPage extends Page {

    public static final By INBOX_LINK_LOCATOR = By.xpath("//a[@href='#inbox']");

    public InboxPage(WebDriver driver) {
        super(driver);
    }

    public void open() {
        waitForElementIsClickable(INBOX_LINK_LOCATOR).click();
    }


}
