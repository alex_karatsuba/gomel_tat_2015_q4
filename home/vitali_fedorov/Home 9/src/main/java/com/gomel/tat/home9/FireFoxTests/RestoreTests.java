package com.gomel.tat.home9.FireFoxTests;

import com.gomel.tat.home9.page.yandexmail.DiskPage;
import com.gomel.tat.home9.page.yandexmail.TrashPage;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by user on 09.01.2016.
 */
public class RestoreTests extends LoginMailPositiveTest {

    @Test(priority = 0)
    public void UploadTest() {
        DiskPage diskPage = new DiskPage(driver);

        diskPage.openFromMail();
        diskPage.uploadFile();

        Assert.assertTrue(diskPage.assert_Upload(), "Uploading failed.");
    }

    @Test(priority = 1)
    public void removeTest() {
        DiskPage diskPage = new DiskPage(driver);
        TrashPage trashPage = new TrashPage(driver);

        diskPage.removeFile();
        trashPage.open();

        Assert.assertTrue(trashPage.assertRemove(), "File was not removed to trash");
    }

    @Test(priority = 2)
    public void restoreTest(){
        DiskPage diskPage = new DiskPage(driver);
        TrashPage trashPage = new TrashPage(driver);

        trashPage.restore();
        diskPage.openFromDisk();

        Assert.assertTrue(diskPage.assertRestoration(),"File was not restored");
    }
}
