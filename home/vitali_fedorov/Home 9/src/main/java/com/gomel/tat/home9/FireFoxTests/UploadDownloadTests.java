package com.gomel.tat.home9.FireFoxTests;

import com.gomel.tat.home9.page.yandexmail.DiskPage;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

/**
 * Created by user on 08.01.2016.
 */
public class UploadDownloadTests extends LoginMailPositiveTest {

    @Test(priority = 0)
    public void UploadTest() {
        DiskPage diskPage = new DiskPage(driver);

        diskPage.openFromMail();
        diskPage.uploadFile();

        Assert.assertTrue(diskPage.assert_Upload(), "Uploading failed.");
    }


    @Test(priority = 1)
    public void DownloadTest() throws IOException {
        DiskPage diskPage = new DiskPage(driver);

        diskPage.downloadFile();

        Assert.assertTrue(diskPage.checkContent(diskPage.checkDownloadFile()), "Looks like, you have problem with downloading.");
    }
}
