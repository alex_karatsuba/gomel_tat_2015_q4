package com.gomel.tat.home8.FirefoxTests;

import com.gomel.tat.home8.LetterWorking.Letter;
import com.gomel.tat.home8.LetterWorking.LetterFactory;
import com.gomel.tat.home8.ui.page.yandexmail.ComposePage;
import com.gomel.tat.home8.ui.page.yandexmail.DraftPage;
import com.gomel.tat.home8.ui.page.yandexmail.TrashPage;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Created by user on 24.12.2015.
 */
public class YandexDraftMailTest extends LoginMailPositiveTest {
    private Letter letter;

    @BeforeClass
    public void prepareRandomLetterWithoutSubBody() {
        letter = LetterFactory.getLetterWithoutSubBody();
    }

    @Test(priority = 0)
    public void createAndDeleteDraftMailTest() {
        ComposePage composePage = new ComposePage(driver);
        DraftPage draftPage = new DraftPage(driver);

        draftPage.open();
        composePage.creatingLetterForDraft(letter);
        draftPage.open();
        draftPage.saveAndQuit();
        draftPage.clickCheckBox(letter);
        draftPage.deleteChecked();

        Assert.assertTrue(draftPage.deleteMessageAssert(), "Looks like, you didnt delete this message");
    }

    @Test(priority = 1)
    public void deleteFromTrash() {
        TrashPage trashPage = new TrashPage(driver);

        trashPage.open();
        trashPage.clickCheckBox(letter);
        trashPage.deleteChecked();

        Assert.assertTrue(trashPage.deleteMessageAssert(), "Looks like, you didnt delete this message ");
    }
}


