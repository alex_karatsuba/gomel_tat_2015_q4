package com.gomel.tat.home8;

import org.openqa.selenium.By;

/**
 * Created by user on 23.12.2015.
 */
public class MainBrowserInformation {
    public static final String BASE_URL = "http://www.ya.ru";


    // UI data
    public static final By ENTER_BUTTON_LOCATOR = By.xpath("//a[contains(@href, 'mail.yandex')]");
    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");
    public static final By POSITIVE_LOGIN_ASSERT_LOCATOR = By.xpath(".//*[@id='js-page']/div/div[3]/div[1]/a[2]/span[1]");
    public static final By ERROR_LOGIN_ASSERT_LOCATOR = By.xpath(".//*[@id='nb-1']/body/div/div[1]/div[2]/div/div/div[1]");

    public static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    public static final By TO_INPUT_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    public static final By SUBJECT_INPUT_LOCATOR = By.name("subj");
    public static final By MAIL_TEXT_LOCATOR = By.id("compose-send");
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.xpath("//.[@id='compose-send']");
    public static final By INBOX_LINK_LOCATOR = By.xpath("//a[@href='#inbox']");
    public static final By DRAFT_LINK_LOCATOR = By.xpath("//a[@href='#draft']");
    public static final By ATTACH_LOCATOR = By.xpath("//input[@name='att']");
    public static final By ERROR_NOTIFICATION = By.xpath(".//*[@id='js-page']/div/div[5]/div/div[3]/div/div[3]/div/div/div/div[2]/div/div/form/table/tbody/tr[3]/td[2]/span[3]/span");
    public static final By SEND_TEST_LETTER = By.xpath(" .//*[@id='js-page']/div/div[5]/div/div[3]/div/div[3]/div/div/div/div[1]/div[3]/div[2]/div/div[2]/div[2]/div/span[2]/span/a/span[2]/span/span");
    public static final By INBOX_TEST_LETTER = By.xpath(".//*[@id='js-page']/div/div[5]/div/div[3]/div/div[3]/div/div/div/div[1]/div[3]/div[1]/div/div[2]/div[2]/div[1]/span[2]/span/a/span[2]/span/span");
    public static final By DRAFT_TEST_LETTER = By.xpath(" .//*[@id='js-page']/div/div[5]/div/div[3]/div/div[3]/div/div/div/div[1]/div[3]/div[3]/div/div[2]/div[2]/div[1]/span[2]/span/a/span[2]/span/span");
    public static final String MAIL_LINK_LOCATOR_PATTERN = "//*[@class='block-messages']//a[contains(., '%s')]";
    public static final By DRAFT_DELETE_LOCATOR = By.xpath("//.[@data-action='delete']");
    public static final By CHECK_BOX_LOCATOR_DRAFT = By.xpath("//input[@type='checkbox' and @class= 'b-messages__message__checkbox__input']");


    public static final int TIME_OUT_MAIL_ARRIVED_SECONDS = 10;
    public static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 20;
    public static final int DRIVE_IMPL_WAIT_TIMEOUT_SECONDS = 5;


    public String PositiveUserLogin = "samagonvvv";
    public String PositiveUserPassword = "newzar1234";
    public String NegativeUserLogin = "Admin";
    public String NegativeUserPassword = "I_randomly_hacked_account";
    public String mailTo = "samagonvvv@yandex.ru";
    public String wrongMailTo = "asdasdasdasdas";
    public String mailSubject = "test subject" + Math.random() * 100;
    public String mailContent = "mail content" + Math.random() * 100;


}



