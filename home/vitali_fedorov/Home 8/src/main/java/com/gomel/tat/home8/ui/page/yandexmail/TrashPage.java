package com.gomel.tat.home8.ui.page.yandexmail;

import com.gomel.tat.home8.LetterWorking.Letter;
import com.gomel.tat.home8.ui.page.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.gomel.tat.home8.BrowserPreparation.StartBrowserPreparation.waitForElementIsClickable;
import static com.gomel.tat.home8.BrowserPreparation.StartBrowserPreparation.waitForLocated;

/**
 * Created by user on 31.12.2015.
 */
public class TrashPage extends Page {
    public static final By DRAFT_LINK_LOCATOR = By.xpath("//a[@href='#trash']");
    public static final String MAIL_LINK_LOCATOR_PATTERN = "//*[@class='block-messages']//a[contains(., '%s')]";
    public static final String DRAFT_LETTER_WITH_NEW_ID = "//*[@type='checkbox' and @value='%s']";
    public static final By DELETE_LINK_LOCATOR = By.xpath("//*[@data-action='delete']");
    public static final By DELETE_MESSAGE_ASSERT_LOCATOR = By.xpath("//div[@class='b-statusline']");
    public static final int TIME_OUT_MAIL_DELETED = 10;


    public TrashPage(WebDriver driver) {
        super(driver);
    }

    public void open() {
        waitForElementIsClickable(DRAFT_LINK_LOCATOR).click();
    }

    public void clickCheckBox(Letter letter) {
        waitForLocated(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, letter.getSubject())));
        Pattern pattern = Pattern.compile("[0-9]{15,}");
        Matcher matcher = pattern.matcher(driver.findElement(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, letter.getSubject()))).getAttribute("href"));
        matcher.find();
        String id = matcher.group();
        driver.findElement(By.xpath(String.format(DRAFT_LETTER_WITH_NEW_ID, id))).click();
    }
    public void deleteChecked() {
        waitForElementIsClickable(DELETE_LINK_LOCATOR).click();
    }
    public boolean deleteMessageAssert() {

        try {
            new WebDriverWait(driver, TIME_OUT_MAIL_DELETED)
                    .until(ExpectedConditions.visibilityOfElementLocated(
                            DELETE_MESSAGE_ASSERT_LOCATOR));

        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
