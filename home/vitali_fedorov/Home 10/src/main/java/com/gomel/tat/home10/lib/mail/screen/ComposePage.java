package com.gomel.tat.home10.lib.mail.screen;

import com.gomel.tat.home10.LetterWorking.Letter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.gomel.tat.home10.BrowserPreparation.StartBrowserPreparation.waitForDisappear;
import static com.gomel.tat.home10.BrowserPreparation.StartBrowserPreparation.waitForLocated;
import static com.gomel.tat.home10.lib.common.Service.WebDriverHelp.waitForElementIsBeClickable;


/**
 * Created by user on 27.12.2015.
 */
public class ComposePage {

    // public static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    @FindBy(xpath = "//*[@data-params='field=to']//ancestor::tr//input[@type='text']")
    WebElement addressInput;

    @FindBy(name = "subj")
    WebElement subjectInput;

    @FindBy(id = "compose-send")
    WebElement bodyInput;

    @FindBy(xpath = "//a[@href='#compose']")
    WebElement composeButton;

    @FindBy(id = "compose-submit")
    WebElement sendButton;



    @FindBy(xpath = "//*[@class='b-notification b-notification_error b-notification_error_invalid']")
    WebElement addressErrorNotification;

    public void enterAddress(String email, WebDriver driver) {
        waitForElementIsBeClickable(addressInput, driver);
        addressInput.sendKeys(email);
    }

    public void enterSubject(String )

    public void sendLetter(Letter letter, boolean positiveTest) {
        WebElement toInput = driver.findElement(TO_INPUT_LOCATOR);
        toInput.sendKeys(letter.getTo());
        WebElement subjectInput = driver.findElement(SUBJECT_INPUT_LOCATOR);
        subjectInput.sendKeys(letter.getSubject());
        WebElement mailContentText = driver.findElement(MAIL_TEXT_LOCATOR);
        mailContentText.sendKeys(letter.getBody());

        WebElement sendMailButton = driver.findElement(SEND_MAIL_BUTTON_LOCATOR);
        sendMailButton.click();

        if (positiveTest) {
            waitForDisappear(SEND_MAIL_BUTTON_LOCATOR);
        }
    }


    // public static final By TO_INPUT_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    //  public static final By SUBJECT_INPUT_LOCATOR = By.name("subj");
    //  public static final By MAIL_TEXT_LOCATOR = By.id("compose-send");
    //  public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
    // private static final By NEW_LETTER_NOTIFICATION_LOCATOR = By.xpath("//*[@class='b-statusline']//a");

    //  public static final By ADDRESS_ERROR_NOTIFICATION = By.xpath("//*[@class='b-notification b-notification_error b-notification_error_invalid']");

    public static final int TIME_OUT_MAIL_ARRIVED_SECONDS = 5;
    public static final int TIME_OUT_MAIL_ERROR_APPEARS = 5;


    public void open() {
        WebElement composeButton = driver.findElement(COMPOSE_BUTTON_LOCATOR);
        composeButton.click();

    }


    public void sendLetterWithoutBodySub(Letter letter) {
        WebElement toInput = driver.findElement(TO_INPUT_LOCATOR);
        toInput.sendKeys(letter.getTo());

        WebElement sendMailButton = driver.findElement(SEND_MAIL_BUTTON_LOCATOR);
        sendMailButton.click();
        waitForDisappear(SEND_MAIL_BUTTON_LOCATOR);
    }

    public void creatingLetterForDraft(Letter letter) {
        WebElement toInput = driver.findElement(TO_INPUT_LOCATOR);
        toInput.sendKeys(letter.getTo());
        WebElement subjectInput = driver.findElement(SUBJECT_INPUT_LOCATOR);
        subjectInput.sendKeys(letter.getSubject());
        WebElement mailContentText = driver.findElement(MAIL_TEXT_LOCATOR);
        mailContentText.sendKeys(letter.getBody());

    }




    public boolean addressError() {

        try {
            new WebDriverWait(driver, TIME_OUT_MAIL_ERROR_APPEARS)
                    .until(ExpectedConditions.visibilityOfElementLocated(
                            ADDRESS_ERROR_NOTIFICATION));

        } catch (Exception e) {
            return false;
        }
        return true;
    }

}

