package com.gomel.tat.home10.ui.page.yandexmail;

import com.gomel.tat.home10.LetterWorking.Letter;
import com.gomel.tat.home10.ui.page.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.gomel.tat.home10.BrowserPreparation.StartBrowserPreparation.waitForElementIsClickable;

/**
 * Created by user on 28.12.2015.
 */
public class SentPage extends Page {
    public static final By SENT_MAIL_BUTTON_LOCATOR = By.xpath("//a[@href='#sent']");
    public static final int TIME_OUT_MAIL_ARRIVED_SECONDS = 10;

    public static final String MAIL_LINK_LOCATOR_PATTERN = "//*[@class='block-messages']//a[contains(., '%s')]";
    public static final String MAIL_LINK_ALL_WITHOUT_SUBJECT = "//*[@title='samagonvvv@yandex.ru' and contains(text(),'samagonvvv@yandex.ru')]";

    public SentPage(WebDriver driver) {
        super(driver);
    }

    public void open() {
        waitForElementIsClickable(SENT_MAIL_BUTTON_LOCATOR).click();

    }

    public boolean isLetterPresent(Letter letter) {

        try {
            new WebDriverWait(driver, TIME_OUT_MAIL_ARRIVED_SECONDS)
                    .until(ExpectedConditions.visibilityOfElementLocated(
                            By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, letter.getSubject()))));
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public boolean isLetterWithoutBodySubPresent() {
        try {
            new WebDriverWait(driver, TIME_OUT_MAIL_ARRIVED_SECONDS)
                    .until(ExpectedConditions.presenceOfElementLocated(
                            By.xpath(MAIL_LINK_ALL_WITHOUT_SUBJECT)));
        } catch (Exception e) {
            return false;
        }
        return true;
    }

}


