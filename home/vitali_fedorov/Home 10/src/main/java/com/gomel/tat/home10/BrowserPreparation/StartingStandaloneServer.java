package com.gomel.tat.home10.BrowserPreparation;

import org.openqa.grid.selenium.GridLauncher;
import org.testng.annotations.BeforeClass;

/**
 * Created by user on 29.12.2015.
 */
public class StartingStandaloneServer {

    @BeforeClass
    public void Run() throws Exception {

        System.setProperty("webdriver.chrome.driver", "C:\\GIT\\GOMEL_TAT_2015_Q42\\home\\vitali_fedorov\\Home 8\\src\\main\\resources\\chromedriver.exe");
        GridLauncher.main(new String[]{"-port", "4444", "-multywidow", "-rustAllSSLCertificates"});

    }
}
