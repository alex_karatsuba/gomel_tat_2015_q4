package com.gomel.tat.home10.FirefoxTests;

import com.gomel.tat.home10.LetterWorking.Letter;
import com.gomel.tat.home10.LetterWorking.LetterFactory;
import com.gomel.tat.home10.ui.page.yandexmail.ComposePage;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Created by user on 29.12.2015.
 */
public class SendMailNegativeTest extends LoginMailPositiveTest {
    private Letter letter;

    @BeforeClass
    public void prepareRandomLetterNegativeTest() {
        letter = LetterFactory.getNegativeRandomLetter();
    }

    @Test
    public void CheckErrorWhileCompose() {
        ComposePage composePage = new ComposePage(driver);

        composePage.open();
        composePage.sendLetter(letter, false);

        Assert.assertTrue(composePage.addressError(), "Looks like, this address actually works!");
    }

}
