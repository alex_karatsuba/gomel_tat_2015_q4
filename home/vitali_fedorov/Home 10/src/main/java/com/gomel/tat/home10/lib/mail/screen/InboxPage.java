package com.gomel.tat.home10.lib.mail.screen;

import com.gomel.tat.home10.ui.page.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.gomel.tat.home10.BrowserPreparation.StartBrowserPreparation.waitForElementIsClickable;
import static com.gomel.tat.home10.BrowserPreparation.StartBrowserPreparation.waitForLocated;

/**
 * Created by user on 27.12.2015.
 */
public class InboxPage extends Page {

    @FindBy(xpath = "//*[@class='b-statusline']//a")
    WebElement newLetterNotification;

    public static final String LETTER_JUST_SENT = "//*[@class='block-messages']//a[@href='#message%s']";
    public static final By INBOX_LINK_LOCATOR = By.xpath("//a[@href='#inbox']");

    public InboxPage(WebDriver driver) {
        super(driver);
    }

    public void open() {
        waitForElementIsClickable(INBOX_LINK_LOCATOR).click();
    }

    public String getIDfromHref() {
        waitForLocated(NEW_LETTER_NOTIFICATION_LOCATOR);

        Pattern pattern = Pattern.compile("/[0-9]+");
        Matcher matcher = pattern.matcher(driver.findElement(NEW_LETTER_NOTIFICATION_LOCATOR).getAttribute("href"));
        matcher.find();
        String id = matcher.group();
        return id;
    }

    public boolean isLetterHere() {

        try {
            new WebDriverWait(driver, TIME_OUT_MAIL_ARRIVED_SECONDS)
                    .until(ExpectedConditions.visibilityOfElementLocated(
                            By.xpath(String.format(LETTER_JUST_SENT, getIDfromHref()))));

        } catch (Exception e) {
            return false;
        }
        return true;
    }

}
