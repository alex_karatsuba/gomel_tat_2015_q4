package com.gomel.tat.home10.ui.page;

import org.openqa.selenium.WebDriver;

/**
 * Created by user on 27.12.2015.
 */
public abstract class Page {

    protected WebDriver driver;

    public Page(WebDriver driver) {

        this.driver = driver;
    }
}
