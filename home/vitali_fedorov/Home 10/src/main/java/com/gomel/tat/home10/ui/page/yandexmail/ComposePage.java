package com.gomel.tat.home10.ui.page.yandexmail;

import com.gomel.tat.home10.LetterWorking.Letter;
import com.gomel.tat.home10.ui.page.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.gomel.tat.home10.BrowserPreparation.StartBrowserPreparation.waitForDisappear;
import static com.gomel.tat.home10.BrowserPreparation.StartBrowserPreparation.waitForLocated;


/**
 * Created by user on 27.12.2015.
 */
public class ComposePage extends Page {

    public static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    public static final By TO_INPUT_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    public static final By SUBJECT_INPUT_LOCATOR = By.name("subj");
    public static final By MAIL_TEXT_LOCATOR = By.id("compose-send");
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
    private static final By NEW_LETTER_NOTIFICATION_LOCATOR = By.xpath("//*[@class='b-statusline']//a");
    public static final String LETTER_JUST_SENT = "//*[@class='block-messages']//a[@href='#message%s']";
    public static final By ADDRESS_ERROR_NOTIFICATION = By.xpath("//*[@class='b-notification b-notification_error b-notification_error_invalid']");

    public static final int TIME_OUT_MAIL_ARRIVED_SECONDS = 5;
    public static final int TIME_OUT_MAIL_ERROR_APPEARS = 5;


    public ComposePage(WebDriver driver) {
        super(driver);
    }

    public void open() {
        WebElement composeButton = driver.findElement(COMPOSE_BUTTON_LOCATOR);
        composeButton.click();

    }

    public void sendLetter(Letter letter, boolean positiveTest) {
        WebElement toInput = driver.findElement(TO_INPUT_LOCATOR);
        toInput.sendKeys(letter.getTo());
        WebElement subjectInput = driver.findElement(SUBJECT_INPUT_LOCATOR);
        subjectInput.sendKeys(letter.getSubject());
        WebElement mailContentText = driver.findElement(MAIL_TEXT_LOCATOR);
        mailContentText.sendKeys(letter.getBody());

        WebElement sendMailButton = driver.findElement(SEND_MAIL_BUTTON_LOCATOR);
        sendMailButton.click();

        if (positiveTest) {
            waitForDisappear(SEND_MAIL_BUTTON_LOCATOR);
        }
    }

    public void sendLetterWithoutBodySub(Letter letter) {
        WebElement toInput = driver.findElement(TO_INPUT_LOCATOR);
        toInput.sendKeys(letter.getTo());

        WebElement sendMailButton = driver.findElement(SEND_MAIL_BUTTON_LOCATOR);
        sendMailButton.click();
        waitForDisappear(SEND_MAIL_BUTTON_LOCATOR);
    }

    public void creatingLetterForDraft(Letter letter) {
        WebElement toInput = driver.findElement(TO_INPUT_LOCATOR);
        toInput.sendKeys(letter.getTo());
        WebElement subjectInput = driver.findElement(SUBJECT_INPUT_LOCATOR);
        subjectInput.sendKeys(letter.getSubject());
        WebElement mailContentText = driver.findElement(MAIL_TEXT_LOCATOR);
        mailContentText.sendKeys(letter.getBody());

    }


    public String getIDfromHref() {
        waitForLocated(NEW_LETTER_NOTIFICATION_LOCATOR);

        Pattern pattern = Pattern.compile("/[0-9]+");
        Matcher matcher = pattern.matcher(driver.findElement(NEW_LETTER_NOTIFICATION_LOCATOR).getAttribute("href"));
        matcher.find();
        String id = matcher.group();
        return id;
    }

    public boolean isLetterHere() {

        try {
            new WebDriverWait(driver, TIME_OUT_MAIL_ARRIVED_SECONDS)
                    .until(ExpectedConditions.visibilityOfElementLocated(
                            By.xpath(String.format(LETTER_JUST_SENT, getIDfromHref()))));

        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public boolean addressError() {

        try {
            new WebDriverWait(driver, TIME_OUT_MAIL_ERROR_APPEARS)
                    .until(ExpectedConditions.visibilityOfElementLocated(
                            ADDRESS_ERROR_NOTIFICATION));

        } catch (Exception e) {
            return false;
        }
        return true;
    }

}

