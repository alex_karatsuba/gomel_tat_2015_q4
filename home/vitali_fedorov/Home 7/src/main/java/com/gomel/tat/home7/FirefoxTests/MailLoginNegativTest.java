package com.gomel.tat.home7.FirefoxTests;


import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * Created by user on 23.12.2015.
 */
public class MailLoginNegativTest extends BrowserPreparation {

    @Test
    public void InvalidAccountNegativeTest()  {

            WebElement enterButton = driver.findElement(ENTER_BUTTON_LOCATOR);
            enterButton.click();
            WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
            loginInput.sendKeys(NegativeUserLogin);
            WebElement passInput = driver.findElement(PASSWORD_INPUT_LOCATOR);
            passInput.sendKeys(NegativeUserPassword);
            passInput.submit();
            Assert.assertEquals(driver.findElement(ERROR_LOGIN_ASSERT_LOCATOR).isDisplayed(), true, "It is the real login!");



    }

    @Test
    public void InvalidAccountPasswordNegativeTest() {

        WebElement enterButton = driver.findElement(ENTER_BUTTON_LOCATOR);
        enterButton.click();
        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(PositiveUserLogin);
        WebElement passInput = driver.findElement(PASSWORD_INPUT_LOCATOR);
        passInput.sendKeys(NegativeUserPassword);
        passInput.submit();
        Assert.assertEquals(driver.findElement(ERROR_LOGIN_ASSERT_LOCATOR).isDisplayed(), true, "It is the real login and password");
        }
}
