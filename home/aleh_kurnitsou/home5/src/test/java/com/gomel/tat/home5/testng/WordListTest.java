package com.gomel.tat.home5.testng;
import com.gomel.tat.home5.WordList;
import org.testng.Assert;
import org.testng.annotations.Test;


import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;


public class WordListTest {

   //protected WordList wordList;

    public static final String PATH = "D:\\TAT\\home\\";
    public static final String FILEMASK = ".*.txt";
    private static PrintStream stream;

    @Test
    public void printStatisticTest() {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(baos);

        WordList wordList = new WordList();
        wordList.values = new HashMap<>();
        wordList.values.put("test.txt", 3);
        wordList.setStream(printStream);
        wordList.printStatistics();

        Assert.assertEquals(baos.toString(), "test.txt: 3\r\nОбщее кол-во слов: 1\r\n", "Oleg was good boy");
    }

     @Test
        public void WordsAmountTest(){
        WordList wordList = new WordList();
        WordList.setStream(System.out);
        wordList.getWords(PATH,  FILEMASK);
        Assert.assertNotEquals(0,wordList.values.size());
     }

   @Test
    public void WordsAmountTest2(){
        WordList wordList = new WordList();
        WordList.setStream(System.out);
        wordList.getWords(PATH,  FILEMASK);
        Assert.assertEquals(508, wordList.values.size());

    }

    @Test

    public void WordAmountTest1(){
        WordList wordList = new WordList();
        WordList.setStream(System.out);
        wordList.getWords(PATH,  FILEMASK);
        for (Map.Entry<String, Integer> value : wordList.values.entrySet()) {
            if (value.getKey().equals("Victor")) {
                Assert.assertEquals("2", value.getValue()+"");
            }
        }
    }
}




