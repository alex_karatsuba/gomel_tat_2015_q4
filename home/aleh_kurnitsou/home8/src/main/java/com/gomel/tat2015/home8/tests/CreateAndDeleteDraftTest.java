package com.gomel.tat2015.home8.tests;

import com.gomel.tat2015.home8.ui.page.yandexmail.ComposePage;
import com.gomel.tat2015.home8.ui.page.yandexmail.DraftPage;
import com.gomel.tat2015.home8.ui.page.yandexmail.TrashPage;
import com.gomel.tat2015.home8.utils.Letter;
import com.gomel.tat2015.home8.utils.LetterFactory;
import com.gomel.tat2015.home8.utils.WebDriverHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.net.MalformedURLException;

public class CreateAndDeleteDraftTest extends BaseLoginTest {

    static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    public static final By TO_INPUT_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    public static final By DRAFT_DIALOG_BUTTON_LOCATOR = By.xpath("//*[@data-action='dialog.save']");
    public static final By DRAFT_FOLDER_LOCATOR = By.xpath("//*[@href='#draft']");
    public static final String USER_LOGIN = "tat-test-tat@yandex.ru";

    @BeforeClass
    public void prepareBrowser() throws InterruptedException, MalformedURLException {
        driver = WebDriverHelper.getWebDriver();
        login();
    }

    @Test
    public void draftMail() {

        ComposePage composePage = new ComposePage(driver);
        composePage.open();
        Letter letter = LetterFactory.getRandomLetter();
        composePage.saveDraft(letter);
        DraftPage draftPage = new DraftPage(driver);
        draftPage.removeDraftMail(letter);
        TrashPage trashPage = new TrashPage(driver);
        trashPage.deleteLetter(letter);
    }

}
