package com.gomel.tat2015.home8.utils;

import java.util.UUID;

public class LetterFactory {

    public static Letter getRandomLetter() {
        String mailTo = "tat-test-tat@yandex.ru";
        String mailSubject = UUID.randomUUID().toString();
        String mailContent = "mail content" + Math.random() * 100000000;
        return new Letter(mailTo, mailSubject, mailContent);
    }
}
