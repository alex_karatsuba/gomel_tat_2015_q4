package com.gomel.tat2015.home8.tests;

import com.gomel.tat2015.home8.ui.page.yandexmail.LoginPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class NegativeLoginTest extends BaseLoginTest {

    public static final String USER_LOGIN = "tat-test-tat@yandex.ru"; // ACCOUNT
    public static final String WRONG_USER_PASSWORD = "Tat12345678"; // ACCOUNT
    public static final String PASSPORT_URL = "https://passport.yandex.ru";

    @Test
    public void loginNegativeTest() {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.open();
        loginPage.login(USER_LOGIN, WRONG_USER_PASSWORD);
        String currentUrl = driver.getCurrentUrl();
        Assert.assertTrue(currentUrl.contains(PASSPORT_URL));
    }

}
