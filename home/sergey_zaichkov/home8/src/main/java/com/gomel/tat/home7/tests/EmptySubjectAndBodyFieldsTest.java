package com.gomel.tat.home7.tests;

import com.gomel.tat.home7.essence.Letter;
import com.gomel.tat.home7.pages.ComposePage;
import com.gomel.tat.home7.pages.InboxPage;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.net.MalformedURLException;

public class EmptySubjectAndBodyFieldsTest extends LoginTest {

    String mailTo = "test-USER-trololo@yandex.ru";
    String mailSubject = "";
    String mailContent = "";

    @Test
    public void emptySubjectAndBodyFields() throws MalformedURLException {
        ComposePage composePage = new ComposePage(driver);
        composePage.open();
        Letter letter = new Letter(mailTo, mailSubject, mailContent);
        composePage.sendLetter(letter);
        InboxPage inboxPage = new InboxPage(driver);
        inboxPage.open();
        Assert.assertTrue(inboxPage.isLetterPresent(letter), "Letter should be present in inbox folder");
    }
}
