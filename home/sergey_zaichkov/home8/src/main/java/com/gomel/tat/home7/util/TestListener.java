package com.gomel.tat.home7.util;


import org.apache.commons.io.FileUtils;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

public class TestListener extends TestListenerAdapter {
    private File fileToLog = new File( "D:\\report.txt");


    @Override
    public void onTestFailure(ITestResult tr) {
        StringBuffer failStringData = new StringBuffer();
        failStringData.append("Test name:  ").append(tr.getTestClass());
        failStringData.append(" Stack trace: ");
        StringWriter errors = new StringWriter();
        tr.getThrowable().printStackTrace(new PrintWriter(errors));
        failStringData.append(errors);

        try {
            FileUtils.writeStringToFile(fileToLog, failStringData.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
