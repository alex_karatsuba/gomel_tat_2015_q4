package com.gomel.tat.home7;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class PrepareBrowser {

    // AUT data
    public static final String BASE_URL = "https://mail.yandex.by";

    // UI data
    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");
    public static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    public static final By TO_INPUT_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    public static final By SUBJECT_INPUT_LOCATOR = By.name("subj");
    public static final By MAIL_TEXT_LOCATOR = By.id("compose-send_ifr");
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.xpath("//button[@id='compose-submit']");
    public static final By INBOX_LINK_LOCATOR = By.xpath("//a[@href='#inbox']");
    public static final By ATTACH_LOCATOR = By.xpath("//input[@name='att']");
    public static final String MAIL_LINK_LOCATOR_PATTERN = "//*[@class='block-messages']//a[contains(., '%s')]";

    // Tools data
    public static final int TIME_OUT_MAIL_ARRIVED_SECONDS = 10;
    public static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 20;
    public static final int DRIVE_IMPL_WAIT_TIMEOUT_SECONDS = 5;
    WebDriver driver;

    // Test data
    String userLogin = "test-USER-trololo";
    String userPassword = "hopHEYlalaley2016";
    String mailTo = "test-USER-trololo@yandex.ru"; // ENUM
    String mailSubject = "test subject" + Math.random() * 100000000; // RANDOM
    String mailContent = "mail content" + Math.random() * 100000000;// RANDOM


    @BeforeTest(description = "Prepare browser")
    public void prepareBrowser() throws MalformedURLException {
        driver = new RemoteWebDriver(new URL("http://localhost:4445/wd/hub"), DesiredCapabilities.firefox());
        driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(DRIVE_IMPL_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
    }

    @AfterClass(description = "Close browser")
    public void clearBrowser() {
        driver.quit();
    }
}




