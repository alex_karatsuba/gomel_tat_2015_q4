package com.gomel.tat.home7;


import org.testng.TestNG;
import org.testng.collections.Lists;

import java.util.List;

public class TestRunner {
    public static void main(String[] args) {
        TestNG testng = new TestNG();
        testng.addListener(new TestListener());
        List<String> suites = Lists.newArrayList();
        suites.add("src\\main\\resources\\suite.xml");
        testng.setTestSuites(suites);
        testng.run();
    }
}
