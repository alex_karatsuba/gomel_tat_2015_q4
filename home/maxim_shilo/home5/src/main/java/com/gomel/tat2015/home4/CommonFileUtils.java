package com.gomel.tat2015.home4;

import org.mozilla.universalchardet.UniversalDetector;

import java.io.FileInputStream;
import java.io.IOException;

public class CommonFileUtils {
    public static String getEncoding(String filePath) throws IOException{
        byte[] buf = new byte[4];
        UniversalDetector detector = new UniversalDetector(null);
        int bufNum;
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = new FileInputStream(filePath);
            while ((bufNum = fileInputStream.read(buf)) > 0 && !detector.isDone()) {
                detector.handleData(buf, 0, bufNum);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            fileInputStream.close();
        }
        detector.dataEnd();
        String encoding = detector.getDetectedCharset();
        detector.reset();
        return encoding;
    }
}
