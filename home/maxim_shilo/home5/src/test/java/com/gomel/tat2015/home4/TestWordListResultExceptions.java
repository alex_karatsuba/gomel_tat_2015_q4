package com.gomel.tat2015.home4;

import org.testng.annotations.Test;

import java.io.IOException;

public class TestWordListResultExceptions extends BaseWordListTest {

    @Test(expectedExceptions = IOException.class)
    public void PrintStatsThrowsExIfGetWordsMethodNotCalled() throws IOException {
        wordList.printStatistics();
    }

    @Test(expectedExceptions = IOException.class)
    public void PrintStatsThrowsExIfFilesNotFound() throws IOException {
        String fileNamePattern = "*hi*";
        wordList.getWords(tempDirPath, fileNamePattern);
        wordList.printStatistics();
    }
}