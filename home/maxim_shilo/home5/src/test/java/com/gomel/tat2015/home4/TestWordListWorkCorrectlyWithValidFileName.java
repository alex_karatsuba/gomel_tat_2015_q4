package com.gomel.tat2015.home4;

import org.testng.Assert;
import org.testng.annotations.*;

import java.io.IOException;

public class TestWordListWorkCorrectlyWithValidFileName extends BaseWordListTest {

    @Test(groups = "testGroups", dataProvider = "fileNameValues")
    public void TestGetWordsFileName(String fileNamePattern) throws IOException {
        wordList.getWords(tempDirPath, fileNamePattern);
        Assert.assertEquals(wordList.values, expectedResult, "Invalid result of operation");
    }

    @Test
    public void EmptyTestForGroupsWork(){
        System.out.println("If this test runs - correct XML!");
    }

    @DataProvider(name = "fileNameValues")
    public static Object[][] fileNameValuesForCheck() {
        return new Object[][]{
                {"*/hello.txt"},
                {"*hello*"},
                {"*home*.txt"},
                {"*.txt"},
                {"*home*/hello*"},
                {"*home*hello*.txt"},
                {"*h*hel*.txt"},
        };
    }
}
