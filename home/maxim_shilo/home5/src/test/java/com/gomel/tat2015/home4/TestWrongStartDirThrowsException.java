package com.gomel.tat2015.home4;

import org.testng.annotations.*;

import java.io.IOException;

public class TestWrongStartDirThrowsException extends BaseWordListTest {

    private String startDir;
    private static final String fileNamePattern = "*hello.txt";

    @Factory(dataProvider = "startDirValues")
    public TestWrongStartDirThrowsException(String startDir) {
        this.startDir = startDir;
    }

    @Test (expectedExceptions = IOException.class)
    public void checkWrongDirectoryNameThrowsException() throws Exception{
        wordList.getWords(startDir, fileNamePattern);
    }

    @DataProvider(name = "startDirValues")
    public static Object[][] startDirValuesForCheck() {
        return new Object[][]{
                {"D:**"},
                {"*.txt"},
                {"D://wrong_folder_name/"}
        };
    }
}
