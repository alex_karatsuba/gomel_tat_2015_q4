package com.gomel.tat.maxim_shilo.home8.test;

import com.gomel.tat.maxim_shilo.home8.ui.page.yandex_mail.*;
import com.gomel.tat.maxim_shilo.home8.utils.Letter;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.*;

import static com.gomel.tat.maxim_shilo.home8.ui.page.Page.getLetterIDFromStatusLine;
import static com.gomel.tat.maxim_shilo.home8.utils.WebDriverHelper.waitForAppear;

public class SendMailWithoutBodyTest extends BaseTestLogin {

    private Letter letter;

    @BeforeClass
    public void createLetter() {
        letter = new Letter("Maxim.Shilo.tat2015@yandex.ru", "", "");
    }

    @Test()
    public void SendMailWithoutBody() {
        ComposePage composePage = new ComposePage(driver);
        composePage.open();
        composePage.sendLetter(letter);
        letter.ID = getLetterIDFromStatusLine();
        InboxPage inboxPage = new InboxPage(driver);
        inboxPage.open();
        Assert.assertTrue(inboxPage.isLetterPresent(letter.ID));
        OutboxPage outboxPage = new OutboxPage(driver);
        outboxPage.open();
        Assert.assertTrue(outboxPage.isLetterPresent(letter.ID));
    }
}
