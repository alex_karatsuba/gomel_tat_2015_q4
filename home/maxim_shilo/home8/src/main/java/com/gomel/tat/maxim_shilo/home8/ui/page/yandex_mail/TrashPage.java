package com.gomel.tat.maxim_shilo.home8.ui.page.yandex_mail;

import com.gomel.tat.maxim_shilo.home8.ui.page.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static com.gomel.tat.maxim_shilo.home8.utils.WebDriverHelper.waitForElementIsClickable;

public class TrashPage extends Page {

    private static final By TRASH_FOLDER_LOCATOR = By.xpath("//a[@href='#trash']");

    public TrashPage(WebDriver driver){
        super(driver);
    }

    public void open(){
        waitForElementIsClickable(TRASH_FOLDER_LOCATOR).click();
    }
}