package com.gomel.tat.maxim_shilo.home8.test;

import static com.gomel.tat.maxim_shilo.home8.ui.page.Page.*;
import com.gomel.tat.maxim_shilo.home8.ui.page.yandex_mail.*;
import com.gomel.tat.maxim_shilo.home8.utils.Letter;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.*;

import static com.gomel.tat.maxim_shilo.home8.utils.WebDriverHelper.*;

public class SendMailTest extends BaseTestLogin {

    private Letter letter;

    @BeforeClass
    public void createLetter(){
        letter = Letter.getRandomLetter();
    }

    @Test()
    public void SendMail() {
        ComposePage composePage = new ComposePage(driver);
        composePage.open();
        composePage.sendLetter(letter);
        letter.ID = getLetterIDFromStatusLine();
        InboxPage inboxPage = new InboxPage(driver);
        inboxPage.open();
        Assert.assertTrue(inboxPage.isLetterPresent(letter.ID));
        OutboxPage outboxPage = new OutboxPage(driver);
        outboxPage.open();
        Assert.assertTrue(outboxPage.isLetterPresent(letter.ID));
    }
}
