package com.gomel.tat.maxim_shilo.home9.utils;

import org.openqa.selenium.By;

public class Letter {

    private String to;
    private String subject;
    private String body;
    private By LETTER_LOCATOR;
    public String ID;

    private static final String MAIL_LINK_LOCATOR_PATTERN =
            "//*[@class='block-messages' and (not(@style) or @style='')]//a[contains(., '%s')]";

    public Letter(String to, String subject, String body) {
        this.to = to;
        this.subject = subject;
        this.body = body;
        if (!subject.equals("")) {
            LETTER_LOCATOR = By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, subject));
        } else {
            LETTER_LOCATOR = By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, "(Без темы)"));
        }
    }

    public String getTo() {
        return to;
    }

    public String getSubject() {
        return subject;
    }

    public String getBody() {
        return body;
    }

    public By getLocator() {
        return LETTER_LOCATOR;
    }

    public static Letter getRandomLetter() {
        String mailTo = "Maxim.Shilo.tat2015@yandex.ru";
        String mailSubject = "test_letter" + Math.random() * 100000000;
        String mailContent = "mail content" + Math.random() * 100000000;
        return new Letter(mailTo, mailSubject, mailContent);
    }
}
