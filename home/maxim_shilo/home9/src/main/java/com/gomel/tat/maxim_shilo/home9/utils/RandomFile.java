package com.gomel.tat.maxim_shilo.home9.utils;

import java.io.*;
import java.util.ArrayList;

public class RandomFile {

    private static final String DEFAULT_FILE_DIRETORY = "D:/yandex_test/";
    private String filePath;
    private String fileContent;
    private String fileName;

    public RandomFile() {
        fileContent = "test" + Math.random() * 1000000000;
        fileName = "test" + Math.random() * 10000000 + ".txt";
        filePath = DEFAULT_FILE_DIRETORY + fileName;
    }

    public String getContent() {
        return fileContent;
    }

    public String getFileName() {
        return fileName;
    }

    public String getPath() {
        return filePath;
    }

    public static ArrayList<RandomFile> getSeveralFiles() throws IOException{
        ArrayList<RandomFile> randomFiles = new ArrayList<RandomFile>();
        for (int i = 0; i < 3; i++) {
            RandomFile randomFile = new RandomFile();
            randomFile.createTempFile();
            randomFiles.add(randomFile);
        }
        return randomFiles;
    }

    public void createTempFile() throws IOException {
        File tempTxt = new File(filePath);
        if (!tempTxt.exists()) {
            tempTxt.createNewFile();
        }
        BufferedWriter writer = new BufferedWriter(
                new FileWriter(tempTxt));
        writer.write(fileContent);
        writer.close();
    }

    public void deleteTempFile() {
        File tempFile = new File(filePath);
        if (tempFile.exists()) {
            tempFile.delete();
        }
    }
}
