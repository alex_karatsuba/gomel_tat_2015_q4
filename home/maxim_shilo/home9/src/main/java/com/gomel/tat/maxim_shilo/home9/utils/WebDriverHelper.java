package com.gomel.tat.maxim_shilo.home9.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class WebDriverHelper {

    private static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 20;
    private static final int TIME_OUT_ELEMENT_AVAIBLE_SECONDS = 10;
    private static final int TIME_OUT_ELEMENT_IS_PRESENT_SECONDS = 10;

    public static final String DOWNLOAD_DIRECTORY = "D:/yandex_test/download/";

    private static WebDriver driver;

    public static WebDriver getChromeWebDriver() throws MalformedURLException {
        ChromeOptions options = new ChromeOptions();
        Map<String, Object> prefs = new HashMap<String, Object>();
        prefs.put("profile.default_content_settings.popups", 0);
        prefs.put("download.directory_upgrade", true);
        prefs.put("prompt_for_download", false);
        prefs.put("download.prompt_for_download", false);
        prefs.put("download.extensions_to_open", "");
        prefs.put("default_directory", DOWNLOAD_DIRECTORY);
        prefs.put("download.default_directory", DOWNLOAD_DIRECTORY);
        prefs.put("savefile.default_directory", DOWNLOAD_DIRECTORY);
        options.setExperimentalOption("prefs", prefs);
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        if (driver == null) {
            driver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), capabilities);
            driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        }
        return driver;
    }

    public static WebDriver getFirefoxWebDriver() throws MalformedURLException {
        FirefoxProfile profile = new FirefoxProfile();
        profile.setPreference("browser.download.dir", DOWNLOAD_DIRECTORY);
        DesiredCapabilities capabilities = DesiredCapabilities.firefox();
        capabilities.setCapability(FirefoxDriver.PROFILE, profile);
        if (driver == null) {
            driver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), DesiredCapabilities.firefox());
            driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        }
        return driver;
    }

    public static void shutdownWebDriver() {
        try {
            driver.quit();
        } catch (Exception e) {
            System.err.println("Problem with shutting down driver: " + e.getMessage());
        } finally {
            driver = null;
        }
    }

    public static WebElement waitForElementIsClickable(By locator) {
        new WebDriverWait(driver, TIME_OUT_ELEMENT_AVAIBLE_SECONDS).until(ExpectedConditions.elementToBeClickable(locator));
        return driver.findElement(locator);
    }

    public static WebElement waitForAppear(By locator) {
        new WebDriverWait(driver, TIME_OUT_ELEMENT_IS_PRESENT_SECONDS)
                .until(ExpectedConditions.presenceOfElementLocated(locator));
        return driver.findElement(locator);
    }

    public static WebElement waitForDisappear(By locator) {
        new WebDriverWait(driver, TIME_OUT_ELEMENT_AVAIBLE_SECONDS)
                .until(ExpectedConditions.invisibilityOfElementLocated(locator));
        return driver.findElement(locator);
    }

    public static boolean isElementPresent(By locator) {
        try {
            new WebDriverWait(driver, TIME_OUT_ELEMENT_AVAIBLE_SECONDS)
                    .until(ExpectedConditions.visibilityOfElementLocated(locator));
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}