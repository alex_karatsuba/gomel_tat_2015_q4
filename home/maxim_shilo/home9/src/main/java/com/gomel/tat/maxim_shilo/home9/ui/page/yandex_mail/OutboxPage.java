package com.gomel.tat.maxim_shilo.home9.ui.page.yandex_mail;

import com.gomel.tat.maxim_shilo.home9.ui.page.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static com.gomel.tat.maxim_shilo.home9.utils.WebDriverHelper.waitForElementIsClickable;

public class OutboxPage extends Page {

    public static final By OUTBOX_FOLDER_LOCATOR = By.xpath("//a[@href='#sent' and @data-action='move']");

    public OutboxPage(WebDriver driver) {
        super(driver);
    }

    public void open() {
        waitForElementIsClickable(OUTBOX_FOLDER_LOCATOR).click();
    }
}


