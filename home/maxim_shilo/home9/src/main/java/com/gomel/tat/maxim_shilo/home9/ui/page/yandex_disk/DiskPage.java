package com.gomel.tat.maxim_shilo.home9.ui.page.yandex_disk;

import com.gomel.tat.maxim_shilo.home9.ui.page.Page;
import com.gomel.tat.maxim_shilo.home9.utils.RandomFile;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static com.gomel.tat.maxim_shilo.home9.utils.WebDriverHelper.*;

public class DiskPage extends Page {

    private static final String YANDEX_DISK_PAGE = "https://disk.yandex.ru/client/disk";
    private static final String TRASHBOX_PAGE =  "https://disk.yandex.ru/client/trash";
    private static final By UPLOAD_FILE_LOCATOR = By.xpath("//input[@class='button__attach']");
    private static final By CLOSE_UPLOAD_WINDOW_BUTTON_LOCATOR = By.xpath("//button[contains(@class,'button-close')]");
    private static final String DOWNLOAD_BUTTON_LOCATOR_PATTERN =
            "//button[@data-click-action='resource.download' and contains(@data-params,'%s')]";
    private static final String DELETE_BUTTON_LOCATOR_PATTERN =
            "//button[@data-click-action='resource.delete' and contains(@data-params,'%s')]";
    private static final String RESTORE_BUTTON_LOCATOR_PATTERN =
            "//button[@data-click-action='resource.restore' and contains(@data-params,'%s')]";
    private static final By FILE_DELETED_STATUSLINE_LOCATOR = By.xpath("//div[@class='notifications__text js-message']");
    private static final String UPLOADED_FILE_PATTERN = "//div[contains(@data-id,'%s')]";
    private static final By TRASH_LOCATOR = By.xpath("//div[@data-id='/trash']");

    public DiskPage(WebDriver driver) {
        super(driver);
    }

    public void openTrashBox(){
        driver.get(TRASHBOX_PAGE);
    }

    public void open(){
        driver.get(YANDEX_DISK_PAGE);
    }

    public boolean isFilePresent(RandomFile randomFile) {
        try {
            waitForAppear(By.xpath(String.format(UPLOADED_FILE_PATTERN, randomFile.getFileName())));
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public boolean isSeveralFilesPresents(List<RandomFile> randomFiles){
        for (RandomFile file : randomFiles) {
            try {
                waitForAppear(By.xpath(String.format(UPLOADED_FILE_PATTERN, file.getFileName())));
            } catch (Exception e) {
                return false;
            }
        }
        return true;
    }

    public boolean isFilePresentOnPC(RandomFile randomFile) throws IOException {
        File fileOnPC = new File(DOWNLOAD_DIRECTORY + randomFile.getFileName());
        if (fileOnPC.exists()) {
            String content = FileUtils.readFileToString(fileOnPC);
            if (content.equals(randomFile.getContent())) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public void uploadFile(RandomFile randomFile) {
        WebElement uploadButton = waitForAppear(UPLOAD_FILE_LOCATOR);
        uploadButton.sendKeys(randomFile.getPath());
        waitForElementIsClickable(CLOSE_UPLOAD_WINDOW_BUTTON_LOCATOR).click();
    }

    public void downloadFile(RandomFile randomFile) throws InterruptedException {
        waitForAppear(By.xpath(String.format(UPLOADED_FILE_PATTERN, randomFile.getFileName()))).click();
        waitForElementIsClickable(By.xpath
                (String.format(DOWNLOAD_BUTTON_LOCATOR_PATTERN, randomFile.getFileName()))).click();
        Thread.sleep(7000);
    }

    public void removeFile(RandomFile randomFile) throws InterruptedException{
        WebElement fileUploaded = waitForAppear(By.xpath(String.format(UPLOADED_FILE_PATTERN, randomFile.getFileName())));
        WebElement trash = driver.findElement(TRASH_LOCATOR);
        Action action = new Actions(driver).dragAndDrop(fileUploaded, trash).build();
        action.perform();
        waitForAppear(FILE_DELETED_STATUSLINE_LOCATOR);
        Thread.sleep(5000);
    }

    public void restoreFile(RandomFile randomFile){
        waitForAppear(By.xpath(String.format(UPLOADED_FILE_PATTERN, randomFile.getFileName()))).click();
        waitForElementIsClickable(By.xpath
                (String.format(RESTORE_BUTTON_LOCATOR_PATTERN, randomFile.getFileName()))).click();
    }

    public void deleteFilePermanently(RandomFile randomFile) throws InterruptedException{
        waitForAppear(By.xpath(String.format(UPLOADED_FILE_PATTERN, randomFile.getFileName()))).click();
        waitForElementIsClickable(By.xpath
                (String.format(DELETE_BUTTON_LOCATOR_PATTERN, randomFile.getFileName()))).click();
        waitForAppear(FILE_DELETED_STATUSLINE_LOCATOR);
    }

    public void deleteFiles(List<RandomFile> randomFiles)throws InterruptedException{
        Actions builder = new Actions(driver);
        builder.keyDown(Keys.CONTROL);
        for (RandomFile file : randomFiles) {
            builder.click(waitForAppear(By.xpath(String.format(UPLOADED_FILE_PATTERN, file.getFileName()))));
        }
        builder.keyUp(Keys.CONTROL);
        WebElement draggable = waitForAppear(By.xpath(
                String.format(UPLOADED_FILE_PATTERN, randomFiles.get(randomFiles.size()-1).getFileName())));
        WebElement trash = driver.findElement(TRASH_LOCATOR);
        builder.dragAndDrop(draggable, trash);
        Action deleteSeveralElements = builder.build();
        deleteSeveralElements.perform();
    }

    public void uploadSeveralFiles(List<RandomFile> randomFiles) {
        WebElement uploadButton = waitForAppear(UPLOAD_FILE_LOCATOR);
        for (RandomFile file : randomFiles) {
            uploadButton.sendKeys(file.getPath());
        }
        waitForElementIsClickable(CLOSE_UPLOAD_WINDOW_BUTTON_LOCATOR).click();
    }
}
