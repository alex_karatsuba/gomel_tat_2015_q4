package com.gomel.tat.maxim_shilo.home9.test_yandex_mail;

import static com.gomel.tat.maxim_shilo.home9.ui.page.Page.*;

import com.gomel.tat.maxim_shilo.home9.BaseTestLogin;
import com.gomel.tat.maxim_shilo.home9.ui.page.yandex_mail.*;
import com.gomel.tat.maxim_shilo.home9.utils.Letter;
import org.testng.Assert;
import org.testng.annotations.*;

public class SendMailTest extends BaseTestLogin {

    private Letter letter;

    @BeforeClass
    public void createLetter(){
        letter = Letter.getRandomLetter();
    }

    @Test()
    public void SendMail() {
        ComposePage composePage = new ComposePage(driver);
        composePage.open();
        composePage.sendLetter(letter);
        letter.ID = getLetterIDFromStatusLine();
        InboxPage inboxPage = new InboxPage(driver);
        inboxPage.open();
        Assert.assertTrue(inboxPage.isLetterPresent(letter.ID));
        OutboxPage outboxPage = new OutboxPage(driver);
        outboxPage.open();
        Assert.assertTrue(outboxPage.isLetterPresent(letter.ID));
    }
}
