package com.gomel.tat.maxim_shilo.home9.test_yandex_disk;

import org.testng.Assert;
import org.testng.annotations.Test;

public class DeleteFilePermanentlyTest extends DeleteFileTest {

    @Test(priority = 3)
    public void DeleteFilePermanently()throws InterruptedException{
        diskPage.deleteFilePermanently(randomFile);
        Assert.assertFalse(diskPage.isFilePresent(randomFile));
    }
}
