package yandex_tests;

import org.openqa.selenium.*;
import org.testng.annotations.Test;

public class TestDraftMail extends BaseYandexTestClass{

    private static final String mailName = "test_draft";
    private static final By MAIL_SENT_LOCATOR = By.xpath(
            String.format("//*[@class='block-messages' and not(@style)]//a[contains(., '%s')]", mailName));
    private static final By SAVE_BUTTON_LOCATOR = By.xpath("//button[@data-action='dialog.save']");

    @Test()
    public void TestCreateDraftMail() {
        LoginToYandex();
        WebElement composeButton = driver.findElement(COMPOSE_BUTTON_LOCATOR);
        composeButton.click();
        WebElement sendToInput = driver.findElement(SEND_TO_INPUT_LOCATOR);
        sendToInput.sendKeys(login + "@yandex.ru");
        WebElement subjectInput = driver.findElement(SUBJECT_LOCATOR);
        subjectInput.sendKeys(mailName);
        WebElement mailTextArea = driver.findElement(MAIL_TEXT_LOCATOR);
        mailTextArea.sendKeys("Test this mail if you can!");
        WebElement draftButton = driver.findElement(DRAFT_FOLDER_LOCATOR);
        draftButton.click();
        waitForElementIsClickable(SAVE_BUTTON_LOCATOR).click();
        waitForDisappear(SEND_TO_INPUT_LOCATOR);
        waitForElementIsClickable(MAIL_SENT_LOCATOR).click();
        clearMailFolder(DRAFT_FOLDER_LOCATOR);
        WebElement trashButton = driver.findElement(TRASH_FOLDER_LOCATOR);
        trashButton.click();
        WebElement trashMailSent = driver.findElement(MAIL_SENT_LOCATOR);
        trashMailSent.click();
        clearTrashbox();
    }
}
