package com.gomel.tat.maxim_shilo.home10.lib.mail.screen;

import com.gargoylesoftware.htmlunit.ElementNotFoundException;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;

import static com.gomel.tat.maxim_shilo.home10.lib.ui.WebDriverWaits.*;
import static com.gomel.tat.maxim_shilo.home10.lib.ui.WebDriverActions.*;

public class ComposePage extends BaseMailPage {



    private static final By SEND_TO_INPUT_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    private static final By SUBJECT_LOCATOR = By.name("subj");
    private static final By MAIL_TEXT_LOCATOR = By.id("compose-send_ifr");

    private static final By SEND_BUTTON_LOCATOR = By.id("compose-submit");
    private static final By SAVE_BUTTON_LOCATOR = By.xpath("//button[@data-action='dialog.save']");

    private static final By ERROR_ADDRESS_REQUIRED_LOCATOR =
            By.xpath("//span[contains(@class,'error_required') and not(contains(@class,'hidden'))]");

    @Override
    public ComposePage open() {
        waitForElementIsClickable(COMPOSE_BUTTON_LOCATOR).click();
        return this;
    }

    public ComposePage typeRecipient(String recipient) {
        waitForElementIsPresent(SEND_TO_INPUT_LOCATOR).sendKeys(recipient);
        return this;
    }

    public ComposePage typeSubject(String subject) {
        waitForElementIsPresent(SUBJECT_LOCATOR).sendKeys(subject);
        return this;
    }

    public ComposePage typeBody(String body) {
        waitForElementIsPresent(MAIL_TEXT_LOCATOR).sendKeys(body);
        return this;
    }

    public ComposePage send() {
        findElement(SEND_BUTTON_LOCATOR).click();
        return this;
    }

    public void waitUntilLetterSent(){
        waitForElementIsVisible(STATUSLINE_LOCATOR);
    }

    public boolean isErrorPresent() {
        try {
            waitForElementIsVisible(ERROR_ADDRESS_REQUIRED_LOCATOR);
        } catch (ElementNotVisibleException e) {
            return false;
        } catch (ElementNotFoundException e) {
            return false;
        }
        return true;
    }


//    public void saveInDraft(Letter letter){
//        waitForElementIsClickable(SEND_TO_INPUT_LOCATOR).sendKeys(letter.getTo());
//        driver.findElement(SUBJECT_LOCATOR).sendKeys(letter.getSubject());
//        waitForAppear(MAIL_TEXT_LOCATOR).sendKeys(letter.getBody());
//        DraftPage draftPage = new DraftPage(driver);
//        draftPage.open();
//        if (isElementPresent(SAVE_BUTTON_LOCATOR)){
//            driver.findElement(SAVE_BUTTON_LOCATOR).click();
//        }
//    }
}
