package com.gomel.tat.maxim_shilo.home10.tests;

import com.gomel.tat.maxim_shilo.home10.lib.common.Account;
import com.gomel.tat.maxim_shilo.home10.lib.common.AccountBuilder;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.*;

import static com.gomel.tat.maxim_shilo.home10.lib.mail.service.LoginService.*;

import java.net.MalformedURLException;

import static com.gomel.tat.maxim_shilo.home10.lib.ui.WebDriverHelper.*;

public class BaseTestLogin {

    public WebDriver driver;
    public Account account;

    @BeforeClass()
    public void setUp() throws MalformedURLException {
        driver = getDriver("chrome");
        account = AccountBuilder.getAccount();
    }

    @Test()
    public void LoginIntoMail() throws InterruptedException {
        loginToMailBox(account);
        Assert.assertTrue(isLoginSuccess(), "Login failed!");
    }

    @AfterClass()
    public void clearBrowser() {
        shutdownWebDriver();
    }
}
