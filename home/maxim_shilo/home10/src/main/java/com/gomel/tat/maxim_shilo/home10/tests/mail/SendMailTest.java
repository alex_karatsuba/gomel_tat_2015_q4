package com.gomel.tat.maxim_shilo.home10.tests.mail;

import static com.gomel.tat.maxim_shilo.home10.lib.mail.service.MailService.*;

import com.gomel.tat.maxim_shilo.home10.lib.mail.*;
import com.gomel.tat.maxim_shilo.home10.lib.mail.screen.*;
import com.gomel.tat.maxim_shilo.home10.tests.BaseTestLogin;
import org.testng.Assert;
import org.testng.annotations.*;

public class SendMailTest extends BaseTestLogin {

    private Letter letter;

    @BeforeClass
    public void createLetter(){
        letter = LetterBuilder.getRandomLetter();
    }

    @Test()
    public void SendMail() {
        sendMail(letter);
        waitUntilLetterSent();
        Assert.assertTrue(isLetterPresent(letter, new InboxPage()), "Letter is not present in Inbox Folder!");
        Assert.assertTrue(isLetterPresent(letter, new OutboxPage()), "Letter is not present in Outbox Folder!");
    }
}
