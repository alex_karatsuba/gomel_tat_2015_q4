package com.gomel.tat.maxim_shilo.home10.tests.mail;

import static com.gomel.tat.maxim_shilo.home10.lib.mail.service.MailService.*;

import com.gomel.tat.maxim_shilo.home10.lib.mail.LetterBuilder;
import com.gomel.tat.maxim_shilo.home10.tests.BaseTestLogin;
import com.gomel.tat.maxim_shilo.home10.lib.mail.Letter;
import org.testng.Assert;
import org.testng.annotations.*;

public class SendMailWithEmptyToFieldTest extends BaseTestLogin {

    private Letter letter;

    @BeforeClass
    public void createLetter(){
        letter = LetterBuilder.getLetterWithEmptyToField();
    }

    @Test()
    public void SendMailWithEmptyToField() {
        sendMail(letter);
        Assert.assertTrue(isErrorMsgPresent(), "Error message is not presented!");
    }
}
