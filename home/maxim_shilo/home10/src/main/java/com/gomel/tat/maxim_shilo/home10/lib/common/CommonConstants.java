package com.gomel.tat.maxim_shilo.home10.lib.common;

public interface CommonConstants {

    String DEFAULT_MAIL_USER_LOGIN = "Maxim.Shilo.tat2015";
    String DEFAULT_MAIL_USER_PASSWORD = "passwordfortat2015";
    String DEFAULT_MAIL_TO_SEND = "Maxim.Shilo.tat2015@yandex.ru";

    String DEFAULT_DOWNLOAD_DIRECTORY = "D:/yandex_test/download/";
    String TEMP_FILES_DIRETORY = "D:/yandex_test/";

    int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 20;
    int TIME_OUT_ELEMENT_IS_CLICKABLE_SECONDS = 10;
    int TIME_OUT_ELEMENT_IS_PRESENT_SECONDS = 15;
    int TIME_OUT_ELEMENT_IS_DISAPPEAR_SECONDS = 10;
}
