package com.gomel.tat.maxim_shilo.home10.lib.ui;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

import static com.gomel.tat.maxim_shilo.home10.lib.ui.WebDriverWaits.*;

public class WebDriverActions extends WebDriverHelper {

    public static void openPage(String URL) {
        driver.get(URL);
    }

    public static void refreshPage(){
        driver.navigate().refresh();
    }

    public static WebElement findElement(By locator) {
        return driver.findElement(locator);
    }

    public static boolean isElementPresent(By locator) {
        try {
            waitForElementIsPresent(locator);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public static void dragAndDrop(WebElement from, WebElement to) {
        Action action = new Actions(driver).dragAndDrop(from, to).build();
        action.perform();
    }

    public static Actions createActionsBuilder(){
        return new Actions(driver);
    }
}
