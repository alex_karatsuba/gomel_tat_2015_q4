package com.gomel.tat.maxim_shilo.home10.lib.disk.screen;

import com.gomel.tat.maxim_shilo.home10.lib.disk.RandomFile;
import org.openqa.selenium.By;

import static com.gomel.tat.maxim_shilo.home10.lib.ui.WebDriverWaits.*;

public class FilePanel extends BaseDiskPage {

    private static final String DOWNLOAD_BUTTON_LOCATOR_PATTERN =
            "//button[@data-click-action='resource.download' and contains(@data-params,'%s')]";
    private static final String RESTORE_BUTTON_LOCATOR_PATTERN =
            "//button[@data-click-action='resource.restore' and contains(@data-params,'%s')]";
    private static final String DELETE_BUTTON_LOCATOR_PATTERN =
            "//button[@data-click-action='resource.delete' and contains(@data-params,'%s')]";

    private RandomFile randomFile;

    public FilePanel(RandomFile randomFile) {
        this.randomFile = randomFile;
    }

    @Override
    public FilePanel open() {
        waitForElementIsPresent(By.xpath(String.format(UPLOADED_FILE_PATTERN, randomFile.getFileName()))).click();
        return this;
    }

    public FilePanel clickDownloadFileButton() {
        waitForElementIsClickable(By.xpath
                (String.format(DOWNLOAD_BUTTON_LOCATOR_PATTERN, randomFile.getFileName()))).click();
        return this;
    }

    public FilePanel clickRestoreFileButton() {
        waitForElementIsClickable(By.xpath
                (String.format(RESTORE_BUTTON_LOCATOR_PATTERN, randomFile.getFileName()))).click();
        return this;
    }

    public FilePanel clickDeleteFileButton() {
        waitForElementIsClickable(By.xpath
                (String.format(DELETE_BUTTON_LOCATOR_PATTERN, randomFile.getFileName()))).click();
        return this;
    }

    public FilePanel waitUntilFileDownloaded() throws InterruptedException {
        Thread.sleep(7000);
        return this;
    }
}
