package com.gomel.tat.maxim_shilo.home10.lib.disk.screen;

import com.gomel.tat.maxim_shilo.home10.lib.disk.RandomFile;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

import java.util.ArrayList;

import static com.gomel.tat.maxim_shilo.home10.lib.ui.WebDriverActions.*;
import static com.gomel.tat.maxim_shilo.home10.lib.ui.WebDriverWaits.*;

public class DiskPage extends BaseDiskPage{

    private static final String YANDEX_DISK_PAGE = "https://disk.yandex.ru/client/disk";
    private static final By UPLOAD_FILE_LOCATOR = By.xpath("//input[@class='button__attach']");
    private static final By CLOSE_UPLOAD_WINDOW_BUTTON_LOCATOR = By.xpath("//button[contains(@class,'button-close')]");
    private static final By TRASH_LOCATOR = By.xpath("//div[@data-id='/trash']");
    private static final By FILE_DELETED_STATUSLINE_LOCATOR = By.xpath("//div[@class='notifications__text js-message']");

    @Override
    public DiskPage open() {
        openPage(YANDEX_DISK_PAGE);
        return this;
    }

    public DiskPage uploadFile(RandomFile randomFile) {
        waitForElementIsPresent(UPLOAD_FILE_LOCATOR).sendKeys(randomFile.getPath());
        return this;
    }

    public DiskPage waitUntilFileUploaded(){
        waitForElementIsClickable(CLOSE_UPLOAD_WINDOW_BUTTON_LOCATOR);
        return this;
    }

    public DiskPage closeUploadWindow(){
        findElement(CLOSE_UPLOAD_WINDOW_BUTTON_LOCATOR).click();
        return this;
    }

    public DiskPage moveToTrash(WebElement file){
        WebElement trash = findElement(TRASH_LOCATOR);
        dragAndDrop(file, trash);
        return this;
    }

    public DiskPage deleteSeveralFiles(ArrayList<RandomFile> randomFiles) {
        Actions builder = createActionsBuilder();
        builder.keyDown(Keys.CONTROL);
        for (RandomFile file : randomFiles) {
            builder.click(findFile(file));
        }
        builder.keyUp(Keys.CONTROL);
        WebElement draggable = findFile(randomFiles.get(randomFiles.size()-1));
        WebElement trash = findElement(TRASH_LOCATOR);
        builder.dragAndDrop(draggable, trash);
        Action deleteSeveralElements = builder.build();
        deleteSeveralElements.perform();
        return this;
    }
}
