package com.gomel.tat.maxim_shilo.home10.tests.disk;

import com.gomel.tat.maxim_shilo.home10.lib.disk.screen.TrashPage;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

import static com.gomel.tat.maxim_shilo.home10.lib.disk.service.DiskService.*;

public class DeleteFileTest extends UploadFileTest{

    @Test(priority = 2)
    public void RemoveFile() throws InterruptedException, IOException {
        removeFileToTrash(randomFile);
        Assert.assertTrue(isFilePresent(randomFile, new TrashPage()), "File not deleted!");
    }
}
