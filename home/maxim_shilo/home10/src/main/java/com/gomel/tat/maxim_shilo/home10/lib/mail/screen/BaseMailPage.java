package com.gomel.tat.maxim_shilo.home10.lib.mail.screen;

import com.gargoylesoftware.htmlunit.ElementNotFoundException;
import com.gomel.tat.maxim_shilo.home10.lib.mail.Letter;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;

import static com.gomel.tat.maxim_shilo.home10.lib.ui.WebDriverWaits.*;

public class BaseMailPage {

    protected static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    protected static final By STATUSLINE_LOCATOR = By.xpath("//div[@class='b-statusline']");

    private static final By DELETE_BUTTON_LOCATOR = By.xpath("//a[@data-action='delete']");
    private static final By DELETE_DRAFT_BUTTON_LOCATOR = By.xpath("//a[@data-action='compose.delete']");

    private static final String MAIL_LINK_LOCATOR_PATTERN =
            "//*[@class='block-messages' and (not(@style) or @style='')]//a[contains(*, '%s')]";
    private static final By EMPTY_MAIL_LINK_LOCATOR =
            By.xpath("//*[@class='block-messages' and (not(@style) or @style='')]//a[contains(*, 'Без темы')]");

    public BaseMailPage open() {
        return this;
    }

    public boolean isLetterPresent(Letter letter) {
        try {
            waitForElementIsPresent(By.xpath(String
                    .format(MAIL_LINK_LOCATOR_PATTERN, letter.getSubject())));
        } catch (ElementNotFoundException e) {
            return false;
        } catch (StaleElementReferenceException e) {
            return false;
        } catch (TimeoutException e) {
            return false;
        }
        return true;
    }

    public boolean isEmptyLetterPresent() {
        try {
            waitForElementIsPresent(EMPTY_MAIL_LINK_LOCATOR);
        } catch (ElementNotFoundException e) {
            return false;
        } catch (StaleElementReferenceException e) {
            return false;
        } catch (TimeoutException e) {
            return false;
        }
        return true;
    }

    public BaseMailPage openLetter(Letter letter) {
        waitForElementIsPresent(By.xpath(String
                .format(MAIL_LINK_LOCATOR_PATTERN, letter.getSubject())))
                .click();
        return this;
    }

    public BaseMailPage openEmptyLetter() {
        waitForElementIsPresent(EMPTY_MAIL_LINK_LOCATOR).click();
        return this;
    }

    public void deleteLetter() {
        waitForElementIsClickable(DELETE_BUTTON_LOCATOR).click();
        waitForElementIsVisible(STATUSLINE_LOCATOR);
        waitForDisappear(STATUSLINE_LOCATOR);
    }

    public void deleteDraftLetter() {
        waitForElementIsPresent(DELETE_DRAFT_BUTTON_LOCATOR).click();
        waitForElementIsVisible(STATUSLINE_LOCATOR);
        waitForDisappear(STATUSLINE_LOCATOR);
    }

}
