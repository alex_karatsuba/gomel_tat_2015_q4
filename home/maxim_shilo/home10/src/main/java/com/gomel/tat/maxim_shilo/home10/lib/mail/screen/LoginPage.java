package com.gomel.tat.maxim_shilo.home10.lib.mail.screen;

import org.openqa.selenium.By;

import static com.gomel.tat.maxim_shilo.home10.lib.ui.WebDriverWaits.*;
import static com.gomel.tat.maxim_shilo.home10.lib.ui.WebDriverActions.*;

public class LoginPage extends BaseMailPage {

    private static final String MAIL_LOGIN_PAGE_URL = "https://mail.yandex.by";

    private static final By LOGIN_INPUT_LOCATOR = By.name("login");
    private static final By PASSOWRD_INPUT_LOCATOR = By.name("passwd");

    private static final By ERROR_MSG_LOCATOR = By.xpath("//div[@class='error-msg']");

    @Override
    public LoginPage open(){
        openPage(MAIL_LOGIN_PAGE_URL);
        return this;
    }

    public LoginPage typeLogin(String login){
        waitForElementIsPresent(LOGIN_INPUT_LOCATOR).sendKeys(login);
        return this;
    }

    public LoginPage typePassword(String password){
        waitForElementIsPresent(PASSOWRD_INPUT_LOCATOR).sendKeys(password);
        return this;
    }

    public void submitLoginForm(){
        findElement(PASSOWRD_INPUT_LOCATOR).submit();
    }

    public boolean loginSuccess(){
        return isElementPresent(COMPOSE_BUTTON_LOCATOR);
    }

    public boolean errorMsgIsPresent(){
        return isElementPresent(ERROR_MSG_LOCATOR);
    }
}
