package com.gomel.tat.maxim_shilo.home10.tests.disk;

import com.gomel.tat.maxim_shilo.home10.lib.disk.RandomFileBuilder;
import com.gomel.tat.maxim_shilo.home10.lib.disk.screen.BaseDiskPage;
import com.gomel.tat.maxim_shilo.home10.lib.disk.screen.DiskPage;
import com.gomel.tat.maxim_shilo.home10.tests.BaseTestLogin;
import com.gomel.tat.maxim_shilo.home10.lib.disk.RandomFile;
import org.testng.Assert;
import org.testng.annotations.*;

import static com.gomel.tat.maxim_shilo.home10.lib.disk.service.DiskService.*;

import java.io.IOException;

public class UploadFileTest extends BaseTestLogin {

    public RandomFile randomFile;
    public BaseDiskPage diskPage;

    @BeforeClass()
    public void prepareFile() throws IOException {
        randomFile = RandomFileBuilder.getRandomFile();
    }

    @Test(priority = 1)
    public void UploadFile() throws InterruptedException {
        uploadFile(randomFile);
        Assert.assertTrue(isFilePresent(randomFile, new DiskPage()), "Upload failed!");
    }

    @AfterClass()
    public void deleteFile() {
        RandomFileBuilder.deleteTempFile(randomFile);
    }
}
