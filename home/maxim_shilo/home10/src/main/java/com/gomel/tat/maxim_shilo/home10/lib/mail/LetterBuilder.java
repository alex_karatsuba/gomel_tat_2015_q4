package com.gomel.tat.maxim_shilo.home10.lib.mail;

import static com.gomel.tat.maxim_shilo.home10.lib.common.CommonConstants.*;

import org.apache.commons.lang3.RandomStringUtils;

public class LetterBuilder {

    public static Letter getRandomLetter() {
        Letter letter = new Letter();
        letter.setRecipient(DEFAULT_MAIL_TO_SEND)
                .setSubject(RandomStringUtils.randomAlphabetic(10))
                .setBody(RandomStringUtils.randomAlphabetic(30));
        return letter;
    }

    public static Letter getLetterWithEmptyToField(){
        Letter letter = new Letter();
        letter.setRecipient("")
                .setSubject(RandomStringUtils.randomAlphabetic(10))
                .setBody(RandomStringUtils.randomAlphabetic(30));
        return letter;
    }

    public static Letter getLetterWithoutBodyAndSubject(){
        Letter letter = new Letter();
        letter.setRecipient(DEFAULT_MAIL_TO_SEND)
                .setSubject("")
                .setBody("");
        return letter;
    }
}
