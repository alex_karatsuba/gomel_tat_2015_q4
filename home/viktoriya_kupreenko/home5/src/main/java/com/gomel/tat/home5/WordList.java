package com.gomel.tat.home5;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.RegexFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;

import java.util.*;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WordList {
    private List<String> values = new ArrayList<String>();
    private HashMap<String, Integer> lib = new HashMap<String, Integer>();
    public List<String> getWords(String baseDirectory, String fileNamePattern) {
        Collection<File> files = getFileList(baseDirectory, fileNamePattern);
        String files_content = filesToString(files);
        String[] file_content_arr = wordsInFileContent(files_content);
        values = removeEmptyStrings(file_content_arr);
        return values;
    }
    public void printStatistics() {
       lib = wordsCounter(values);
        for (String word : lib.keySet()) {
            System.out.println(word + ": " + lib.get(word));
        }
    }
    public Collection<File> getFileList(String baseDirectory, String fileNamePattern) {
        Collection<File> files = FileUtils.listFiles(new File(baseDirectory), new RegexFileFilter(".*"), TrueFileFilter.INSTANCE);
        String pattern = prepareRegex(fileNamePattern);
        Pattern r = Pattern.compile(pattern);
        Iterator<File> i = files.iterator();
        while (i.hasNext()) {
            Matcher m = r.matcher(i.next().getPath());
            if (!m.find()) {
                i.remove();
            }
        }
        return files;
    }
    public String filesToString(Collection<File> files) {
        String files_content = "";
        for (File file : files) {
            try {
                files_content += FileUtils.readFileToString(file, "windows-1251") + " ";
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return files_content;
    }
    public String[] wordsInFileContent(String input) {
        String[] output = input.split("[^а-яА-Яa-zA-Z]");
        return output;
    }
    public List<String> removeEmptyStrings(String[] file_content_arr) {
        List<String> words = new ArrayList<String>();
        for (String word : file_content_arr) {
            if (!word.isEmpty()) {
                words.add(word);
            }
        }
        return words;
    }
    public String prepareRegex(String input) {
        String output = input.replace("*", ".*").replace("\\", "\\\\");
        return output;
    }
    public HashMap<String, Integer> wordsCounter(List<String> words) {
        HashMap<String, Integer> library = new HashMap<String, Integer>();
        for (String word : words) {
            int count = library.containsKey(word) ? library.get(word) : 0;
            library.put(word, ++count);
        }
        return library;
    }
}
