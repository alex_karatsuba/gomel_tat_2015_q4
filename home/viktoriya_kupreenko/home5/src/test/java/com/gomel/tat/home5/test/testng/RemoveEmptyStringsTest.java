package com.gomel.tat.home5.test.testng;

import org.testng.Assert;
import org.testng.annotations.*;

import java.util.Arrays;
import java.util.List;

public class RemoveEmptyStringsTest extends BaseWordListTest {
    @Test(dataProvider = "dataProviderForRemoveEmptyStringsTest", groups = "get words")
    public void removeEmptyStrings(String[] file_content_arr, List<String> expectedArray) {
        List<String> values = wordList.removeEmptyStrings(file_content_arr);
        Assert.assertEquals(values, expectedArray, "Invalid result array");
    }

    @DataProvider(name = "dataProviderForRemoveEmptyStringsTest")
    public Object[][] dataProvider() {
        String[] file_content_arr1 = {"список", "слов", ""};
        String[] file_content_arr2 = {"Hello", "", "world"};
        String[] file_content_arr3 = {"", "cat", "dog"};
        String[] file_content_arr4 = {"My", "", "name", "", "is", ""};
        List<String> expectedArray1 = Arrays.asList("список", "слов");
        List<String> expectedArray2 = Arrays.asList("Hello", "world");
        List<String> expectedArray3 = Arrays.asList("cat", "dog");
        List<String> expectedArray4 = Arrays.asList("My", "name", "is");
        return new Object[][]{
                {file_content_arr1, expectedArray1},
                {file_content_arr2, expectedArray2},
                {file_content_arr3, expectedArray3},
                {file_content_arr4, expectedArray4}
        };
    }
}