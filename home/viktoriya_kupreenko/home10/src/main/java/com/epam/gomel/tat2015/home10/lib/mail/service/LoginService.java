package com.epam.gomel.tat2015.home10.lib.mail.service;

import com.epam.gomel.tat2015.home10.lib.common.Account;
import com.epam.gomel.tat2015.home10.lib.mail.screen.MailLoginPage;
import org.openqa.selenium.support.PageFactory;

import static com.epam.gomel.tat2015.home10.lib.ui.Browser.*;

import static com.epam.gomel.tat2015.home10.lib.common.CommonConstants.*;

public class LoginService {

    public void loginToMailbox(Account account) {
        MailLoginPage mailLoginPage = PageFactory.initElements(driver, MailLoginPage.class);
        mailLoginPage.open();
        mailLoginPage.login(account.getLogin(), account.getPassword());
    }

    public boolean checkSuccessLogin() {
        return checkIfElementIsPresent(INBOX_LINK_LOCATOR);
    }

    public boolean checkErrorOnFailedLogin() {
        return checkIfElementIsPresent(ERROR_MASSAGE_LOCATOR);
    }

}
