package com.epam.gomel.tat2015.home10.lib.ui;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import static com.epam.gomel.tat2015.home10.lib.common.CommonConstants.*;

public class Browser {

    public static WebDriver driver;

    public static WebDriver getWebDriver() {
        try {
//            driver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), DesiredCapabilities.firefox());
            driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), DesiredCapabilities.chrome());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
//        driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(COMMON_TIME_OUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(COMMON_TIME_OUT_SECONDS, TimeUnit.SECONDS);
        return driver;
    }

    public static void shutdownWebDriver() {
        try {
            driver.quit();
        } catch (Exception e) {
            System.err.println("Problem with shutting down driver: " + e.getMessage());
        }
    }

    public static WebElement waitForElementIsClickable(By locator) {
        new WebDriverWait(driver, COMMON_TIME_OUT_SECONDS).until(ExpectedConditions.elementToBeClickable(locator));
        return driver.findElement(locator);
    }

    public static WebElement waitForElementIsVisible(By locator) {
        new WebDriverWait(driver, COMMON_TIME_OUT_SECONDS).until(ExpectedConditions.visibilityOfElementLocated(locator));
        return driver.findElement(locator);
    }

    public static WebElement waitForDisappear(By locator) {
        new WebDriverWait(driver, COMMON_TIME_OUT_SECONDS).until(ExpectedConditions.invisibilityOfElementLocated(locator));
        return driver.findElement(locator);
    }

    public static boolean checkIfElementIsPresent(By locator) {
        try {
            waitForElementIsVisible(locator);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
