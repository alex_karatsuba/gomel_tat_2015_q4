package com.epam.gomel.tat2015.home10.lib.mail.screen;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ComposePage extends Page {

    @FindBy(xpath = "//*[@data-params='field=to']//ancestor::tr//input[@type='text']")
    WebElement fieldToInput;

    @FindBy(id = "compose-subj")
    WebElement letterSubjectInput;

    @FindBy(id = "compose-send")
    WebElement letterBodyInput;

    @FindBy(xpath = "//a[@href='#compose']")
    WebElement COMPOSE_BUTTON_LOCATOR;

    public ComposePage(WebDriver driver) {
        super(driver);
    }

    public void open() {
        COMPOSE_BUTTON_LOCATOR.click();
    }

    public void sendMail(String to, String subject, String body) {
        fieldToInput.sendKeys(to);
        letterSubjectInput.sendKeys(subject);
        letterBodyInput.sendKeys(body);
        fieldToInput.submit();
    }
}
