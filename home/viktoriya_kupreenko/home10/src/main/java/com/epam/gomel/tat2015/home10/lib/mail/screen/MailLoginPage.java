package com.epam.gomel.tat2015.home10.lib.mail.screen;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static com.epam.gomel.tat2015.home10.lib.common.CommonConstants.*;

public class MailLoginPage extends Page {

    @FindBy(name = "login")
    WebElement loginInput;

    @FindBy(name = "passwd")
    WebElement passInput;

    public MailLoginPage(WebDriver driver) {
        super(driver);
    }

    public void open() {
        driver.get(MAILBOX_URL);
    }

    public void login(String login, String pass) {
        loginInput.sendKeys(login);
        passInput.sendKeys(pass);
        loginInput.submit();
    }


}


