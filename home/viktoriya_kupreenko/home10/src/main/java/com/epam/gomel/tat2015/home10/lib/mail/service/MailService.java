package com.epam.gomel.tat2015.home10.lib.mail.service;

import com.epam.gomel.tat2015.home10.lib.common.Account;
import com.epam.gomel.tat2015.home10.lib.mail.Letter;
import com.epam.gomel.tat2015.home10.lib.mail.screen.ComposePage;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import static com.epam.gomel.tat2015.home10.lib.common.CommonConstants.*;
import static com.epam.gomel.tat2015.home10.lib.ui.Browser.*;


public class MailService {

    ComposePage composePage;

    public void sendLetter(Account account, Letter letter) {
        composePage = PageFactory.initElements(driver, ComposePage.class);
        composePage.open();
        composePage.sendMail(letter.getRecipient(), letter.getSubject(), letter.getBody());

    }

    public boolean isLetterExistInInbox(Account account, Letter letter) {
        return checkIfElementIsPresent(By.xpath(String.format(LATTER_LOCATOR_PATTERN, letter.getSubject())));
    }

}
