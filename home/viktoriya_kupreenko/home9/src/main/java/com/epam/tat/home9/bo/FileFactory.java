package com.epam.tat.home9.bo;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

public class FileFactory {

    public static final String FILE_PATH_UPLOAD = "D:\\";
    public static final String FILE_PATH_DOWNLOAD = "X:\\";
    public static String FILE_NAME;
    public static String FILE_NAME1;
    public static String FILE_NAME2;
    public static String FILE_NAME3;
    public static final String FILE_CONTENT = "This is a test " + Math.random() * 10000000;

    public static void createFile() throws IOException {
        FILE_NAME = "test" + Math.random() * 10000000 + ".txt";
        File file = new File("D:/" + FILE_NAME);
        FileUtils.writeStringToFile(file, FILE_CONTENT);
    }

    public static void createFiles() throws IOException {
        FILE_NAME1 = "test" + Math.random() * 10000000 + ".txt";
        FILE_NAME2 = "test" + Math.random() * 10000000 + ".txt";
        FILE_NAME3 = "test" + Math.random() * 10000000 + ".txt";
        File file1 = new File(FILE_PATH_UPLOAD + FILE_NAME1);
        File file2 = new File(FILE_PATH_UPLOAD + FILE_NAME2);
        File file3 = new File(FILE_PATH_UPLOAD + FILE_NAME3);
        FileUtils.writeStringToFile(file1, FILE_CONTENT);
        FileUtils.writeStringToFile(file2, FILE_CONTENT);
        FileUtils.writeStringToFile(file3, FILE_CONTENT);
    }

}
