package com.epam.tat.home9.test;

import com.epam.tat.home9.ui.page.yandexdisk.DiskPage;
import com.epam.tat.home9.ui.page.yandexdisk.TrashPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class RestoreTest extends RemoveTest {

    @Test(priority = 3)
    public void testRestore() {
        TrashPage trashPage = new TrashPage(driver);
        trashPage.selectFile();
        trashPage.restore();
        DiskPage diskPage = new DiskPage(driver);
        diskPage.open();
        Assert.assertTrue(diskPage.isFilePresent());
    }
}
