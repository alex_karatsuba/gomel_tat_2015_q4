package com.epam.tat.home9.test;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import static com.epam.tat.home9.util.WebDriverHelper.getWebDriver;
import static com.epam.tat.home9.util.WebDriverHelper.shutdownWebDriver;

public class BaseTest {
    protected WebDriver driver;

    @BeforeClass
    public void prepareBrowser() {
        driver = getWebDriver();
    }

    @AfterClass
    public void shutdown() {
        shutdownWebDriver();
    }

}
