package com.epam.tat.home9.ui.page;

import org.openqa.selenium.WebDriver;

public abstract class Page {

    protected WebDriver driver;
    public static final int TIME_OUT_FILE_SECONDS = 10;

    public Page(WebDriver driver) {
        this.driver = driver;
    }
}
