package com.epam.tat.home9.test;

import com.epam.tat.home9.ui.page.yandexdisk.TrashPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TrashTest extends RemoveTest {

    @Test(priority = 3)
    public void testTrash() {
        TrashPage trashPage = new TrashPage(driver);
        trashPage.selectFile();
        trashPage.removePermanently();
        Assert.assertFalse(trashPage.isFilePresent());
    }
}
