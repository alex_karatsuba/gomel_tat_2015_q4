package com.epam.tat.home7;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import org.testng.annotations.Test;

import java.util.List;

public class DeleteDraftMailTest extends LoginTest {


    // UI data

    public static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    public static final By TO_INPUT_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    public static final By SUBJECT_INPUT_LOCATOR = By.name("subj");
    public static final By MAIL_TEXT_LOCATOR = By.id("compose-send");
    public static final By DELETE_BUTTON_LOCATOR = By.xpath(" //a[@data-action='compose.delete']");
    public static final By DELETE_BUTTON_LOCATOR1 = By.xpath(" //a[contains(@title,'Delete')]");
    public static final By TRASH_LINK_LOCATOR = By.xpath(" //a[@ class='b-folders__folder__link' and @href='#trash']");
    public static final String MAIL_LINK_LOCATOR_PATTERN = "//*[@class='block-messages']//a[contains(., '%s')]";
    public static final By SAVED_IN_DRAFT = By.xpath("(//span[@class='b-compose-message__actions__helper b-compose-message__actions__helper_saved'])[2]");
    public static final By DRAFT_LINK_LOCATOR = By.xpath("//a[@href='#draft']");
    public static final String CHECKBOX_LOCATOR_PATTERN = "//*[@class='block-messages']//a[contains(., '%s')]/ancestor::span/preceding-sibling::label/input";
    // Test data

    private String mailTo = "tat-test-user@yandex.ru"; // ENUM
    private String mailSubject = "test subject" + Math.random() * 100000000; // RANDOM
    private String mailContent = "mail content" + Math.random() * 100000000;// RANDOM
    // Tools data
    public static final int TIME_OUT_MAIL_ARRIVED_SECONDS = 20;

    @Test(description = "Create draft email", dependsOnMethods = "login")
    public void createDraftMail() {
        waitForElementIsClickable(COMPOSE_BUTTON_LOCATOR);
        WebElement composeButton = driver.findElement(COMPOSE_BUTTON_LOCATOR);
        composeButton.click();
        waitForElementIsClickable(TO_INPUT_LOCATOR);
        WebElement toInput = driver.findElement(TO_INPUT_LOCATOR);
        toInput.sendKeys(mailTo);
        waitForElementIsClickable(SUBJECT_INPUT_LOCATOR);
        WebElement subjectInput = driver.findElement(SUBJECT_INPUT_LOCATOR);
        subjectInput.sendKeys(mailSubject);
        waitForElementIsClickable(MAIL_TEXT_LOCATOR);
        WebElement mailContentText = driver.findElement(MAIL_TEXT_LOCATOR);
        mailContentText.sendKeys(mailContent);
        waitForElementIsClickable(SAVED_IN_DRAFT);
        List<WebElement> actualRes = driver.findElements(SAVED_IN_DRAFT);
        Assert.assertEquals(!actualRes.isEmpty(), true, "Mail was not saved in draft");
    }

    @Test(description = "Delete draft email", dependsOnMethods = "createDraftMail", expectedExceptions = NoSuchElementException.class)
    public void deleteDraftMail() {
        driver.get("https://mail.yandex.by/?#draft");
//        waitForElementIsClickable(DRAFT_LINK_LOCATOR);
//        WebElement draftLink = driver.findElement(DRAFT_LINK_LOCATOR);
//        draftLink.click();

//        waitForElementIsClickable(By.xpath(String.format(CHECKBOX_LOCATOR_PATTERN, mailSubject)));
        WebElement checkboxInput = driver.findElement(By.xpath(String.format(CHECKBOX_LOCATOR_PATTERN, mailSubject)));
        checkboxInput.click();

        waitForElementIsClickable(DELETE_BUTTON_LOCATOR1);
        WebElement deleteButton = driver.findElement(DELETE_BUTTON_LOCATOR1);
        deleteButton.click();

        driver.get("https://mail.yandex.by/?#trash");
//        waitForElementIsClickable(TRASH_LINK_LOCATOR);
//        WebElement trashLink = driver.findElement(TRASH_LINK_LOCATOR);
//        trashLink.click();

//        waitForElementIsClickable(By.xpath(String.format(CHECKBOX_LOCATOR_PATTERN, mailSubject)));
        WebElement checkboxInputDelete = driver.findElement(By.xpath(String.format(CHECKBOX_LOCATOR_PATTERN, mailSubject)));
        checkboxInputDelete.click();

        waitForElementIsClickable(DELETE_BUTTON_LOCATOR1);
        WebElement deleteButton1 = driver.findElement(DELETE_BUTTON_LOCATOR1);
        deleteButton1.click();

        waitForDisappear(By.xpath(String.format(CHECKBOX_LOCATOR_PATTERN, mailSubject)));

        boolean actualRes = false;
        try {
            driver.findElement(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, mailSubject)));
        } catch (NoSuchElementException e) {
            actualRes = true;
        }

        Assert.assertEquals(actualRes, true, "Mail was not saved in draft");
    }
}