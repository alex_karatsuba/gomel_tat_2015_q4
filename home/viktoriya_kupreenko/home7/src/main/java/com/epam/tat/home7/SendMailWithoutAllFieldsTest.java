package com.epam.tat.home7;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class SendMailWithoutAllFieldsTest extends LoginTest {

    // AUT data

    public static final String MAIL_URL = "http://mail.yandex.by";

    // UI data

    public static final By TO_INPUT_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
    public static final By INBOX_LINK_LOCATOR = By.xpath("//a[@ class='b-folders__folder__link' and @href='#inbox']");
    public static final By SENT_LINK_LOCATOR = By.xpath("//a[@href='#sent']");
    public static final By ATTACH_LOCATOR = By.xpath("//input[@name='att']");
    public static final String MAIL_ID_LOCATOR_PATTERN = "//a[@class='b-messages__message__link daria-action' and @href='#%s']";
    public static final By SEND_MAIL_MASSAGE_LOCATOR = By.xpath("//div[@class='b-statusline']//a[@class='b-statusline__link']");
    private String[] splited_href;
    private String xpath;
    // Tools data
    public static final int TIME_OUT_MAIL_ARRIVED_SECONDS = 10;

    // Test data

    private String mailTo = "tat-test-user@yandex.ru"; // ENUM

    @Test(description = "Check send email without subject and body", dependsOnMethods = "login")
    public void sendMailWithoutSubjectBody() {
        waitForElementIsClickable(COMPOSE_BUTTON_LOCATOR);
        WebElement composeButton = driver.findElement(COMPOSE_BUTTON_LOCATOR);
        composeButton.click();
        waitForElementIsClickable(TO_INPUT_LOCATOR);
        WebElement toInput = driver.findElement(TO_INPUT_LOCATOR);
        toInput.sendKeys(mailTo);
        WebElement attachFileButton = driver.findElement(ATTACH_LOCATOR);
        attachFileButton.sendKeys("D:\\LU28ACmNKIU.jpg");
        waitForElementIsClickable(SEND_MAIL_BUTTON_LOCATOR);
        WebElement sendMailButton = driver.findElement(SEND_MAIL_BUTTON_LOCATOR);
        sendMailButton.click();
        waitForDisappear(SEND_MAIL_BUTTON_LOCATOR);
        waitForElementIsClickable(SEND_MAIL_MASSAGE_LOCATOR);
        List<WebElement> actualRes1 = driver.findElements(SEND_MAIL_MASSAGE_LOCATOR);
        Assert.assertEquals(!actualRes1.isEmpty(), true, "Mail was not sent");
        String href = driver.findElement(By.xpath("//div[@class='b-statusline']//a[@class='b-statusline__link']")).getAttribute("href");
        splited_href = href.split("#");

    }

    @Test(description = "Check send email IN inbox and Send mail", dependsOnMethods = "sendMailWithoutSubjectBody")
    public void checkInboxFolder() {
//        driver.get("https://mail.yandex.by/?#inbox");
        waitForElementIsClickable(INBOX_LINK_LOCATOR);
        WebElement inboxLink = driver.findElement(INBOX_LINK_LOCATOR);
        inboxLink.click();

        xpath = String.format(MAIL_ID_LOCATOR_PATTERN, splited_href[splited_href.length - 1]);

        waitForElementIsClickable(By.xpath(xpath));
        List<WebElement> actualRes = driver.findElements(By.xpath(xpath));
        Assert.assertEquals(!actualRes.isEmpty(), true, "Mail does not exist in folder Inbox ");
    }

    @Test(description = "Check send email IN inbox and Send mail", dependsOnMethods = "sendMailWithoutSubjectBody")
    public void checkSendFolder() {

        driver.get("https://mail.yandex.by/?#sent");
//        waitForElementIsClickable(SENT_LINK_LOCATOR);
//        WebElement sendLink = driver.findElement(SENT_LINK_LOCATOR);
//        sendLink.click();

        waitForElementIsClickable(By.xpath(xpath));
        List<WebElement> actualRes1 = driver.findElements(By.xpath(xpath));
        Assert.assertEquals(!actualRes1.isEmpty(), true, "Mail does not exist in folder Send ");

    }

}

