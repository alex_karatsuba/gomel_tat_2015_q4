package com.epam.tat.home8.ui.page.yandexmail;

import com.epam.tat.home8.bo.Letter;
import com.epam.tat.home8.ui.page.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.epam.tat.home8.util.WebDriverHelper.waitForDisappear;
import static com.epam.tat.home8.util.WebDriverHelper.waitForElementIsClickable;

public class TrashPage extends Page {

    public static final int TIME_OUT_MAIL_ARRIVED_SECONDS = 10;
    public static final By TRASH_LINK_LOCATOR = By.xpath(" //a[@ class='b-folders__folder__link' and @href='#trash']");
    public static final String MAIL_LINK_LOCATOR_PATTERN = "//*[@class='block-messages']//a[contains(., '%s')]";
    public static final String CHECKBOX_LOCATOR_PATTERN = "//*[@class='block-messages']//a[contains(., '%s')]/ancestor::span/preceding-sibling::label/input";
    public static final By DELETE_BUTTON_LOCATOR = By.xpath(" //a[@data-action='delete']");

    public TrashPage(WebDriver driver) {
        super(driver);
    }

    public void open() {
//        driver.get("https://mail.yandex.by/?#trash");
        waitForElementIsClickable(TRASH_LINK_LOCATOR).click();
    }

    public void clickCheckbox(Letter letter) {
        new WebDriverWait(driver, TIME_OUT_MAIL_ARRIVED_SECONDS)
                .until(ExpectedConditions.visibilityOfElementLocated(
                        By.xpath(String.format(CHECKBOX_LOCATOR_PATTERN, letter.getSubject()))));

        WebElement checkboxInput = driver.findElement(By.xpath(String.format(CHECKBOX_LOCATOR_PATTERN, letter.getSubject())));
        waitForElementIsClickable(By.xpath(String.format(CHECKBOX_LOCATOR_PATTERN, letter.getSubject())));
        checkboxInput.click();
    }

    public void deleteMail() {
        new WebDriverWait(driver, TIME_OUT_MAIL_ARRIVED_SECONDS)
                .until(ExpectedConditions.visibilityOfElementLocated(DELETE_BUTTON_LOCATOR));
        WebElement deleteButton = driver.findElement(DELETE_BUTTON_LOCATOR);
        waitForElementIsClickable(DELETE_BUTTON_LOCATOR);
        deleteButton.click();
    }

    public boolean isLetterPresent(Letter letter) {
        try {
            waitForDisappear(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, letter.getSubject())));
            driver.findElement(By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, letter.getSubject())));
        } catch (NoSuchElementException e) {
            return false;
        }
        return true;
    }


}
