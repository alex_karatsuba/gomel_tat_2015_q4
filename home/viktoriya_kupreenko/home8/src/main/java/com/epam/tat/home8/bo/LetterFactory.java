package com.epam.tat.home8.bo;


public class LetterFactory {

    public static Letter getRandomLetter() {
        String mailTo = "tat-test-user@yandex.ru";
        String mailSubject = "test subject" + Math.random() * 100000000;
        String mailContent = "mail content" + Math.random() * 100000000;
        return new Letter(mailTo, mailSubject, mailContent);
    }
}
