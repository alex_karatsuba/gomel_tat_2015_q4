package com.gomel.tat.home8.test;


import com.gomel.tat.home8.bo.Letter;
import com.gomel.tat.home8.bo.LetterFactory;
import com.gomel.tat.home8.page.yandexmail.ComposePage;
import com.gomel.tat.home8.page.yandexmail.InboxPage;
import com.gomel.tat.home8.util.WebDriverHelper;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class SendMailWithoutAdressTest extends LoginIntoMailTest {

    private Letter letter;
    private static final By Error = By.xpath("//*[@data-params='field=to']//ancestor::tr//span[contains(@class,'error_required')]");

    @BeforeClass
    public void prepareData() {
        letter = LetterFactory.getRandomLetter();
    }

    @Test
    public void sendMail() {
        ComposePage composePage = new ComposePage(driver);
        composePage.open();
        composePage.sendLetterWithoutAdress(letter);
        Assert.assertTrue(driver.findElement(Error).isDisplayed());
    }

    @AfterClass
    public void shutdown() {
        WebDriverHelper.shutdownWebDriver();
    }
}
