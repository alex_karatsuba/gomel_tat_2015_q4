package com.gomel.tat.home8.test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;

public class BrowserStartTest {
    public static final String YANDEX_START_PAGE = "http://ya.ru";
    public WebDriver driver;

    @Test(description = "Firefox launch test")
    public void firefoxLocalLaunch() {
        driver = new FirefoxDriver();
        driver.get(YANDEX_START_PAGE);
        driver.close();
    }

    @Test(description = "Chrome launch test")
    public void chromeLocalLaunch() {
        System.setProperty("webdriver.chrome.driver", "./src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.get(YANDEX_START_PAGE);
        driver.close();
    }

    @Test(description = "Firefox remote launch")
    public void firefoxRemoteLaunch() throws MalformedURLException {
        // java -jar selenium-server-standalone-2.45.0.jar -port 4444
        driver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), DesiredCapabilities.firefox());
        driver.get(YANDEX_START_PAGE);
        driver.close();
    }

    @Test(description = "Chrome remote launch")
    public void chromeRemoteLaunch() throws MalformedURLException {
        // java -jar selenium-server-standalone-2.45.0.jar -port 4444 -Dwebdriver.chrome.driver="d:\path\to\chromedriver.exe"
        driver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), DesiredCapabilities.chrome());
        driver.get(YANDEX_START_PAGE);
        driver.close();
    }
}

