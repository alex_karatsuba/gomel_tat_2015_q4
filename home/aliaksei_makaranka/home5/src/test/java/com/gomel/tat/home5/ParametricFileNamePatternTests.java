package com.gomel.tat.home5;

import org.testng.Assert;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.util.Map;

public class ParametricFileNamePatternTests extends BaseWordListTests{

    @Parameters({"first-name"})
    @Test
    public void parametricFileNamePatternTests(@Optional(value ="hello3.txt")String firstName) {
        wordList.getWords("D:\\tempDirectory", firstName);
        System.out.println(firstName);
         Assert.assertNotNull(wordList);
    }

    @Test
    public void TestGetWordsFileName()   {
        wordList.getWords("D:\\tempDirectory", "*.txt");
        Map<String, Integer> words = wordList.getWords();
        Assert.assertEquals(words, expectedResult, "Invalid result of operation");
    }
}
