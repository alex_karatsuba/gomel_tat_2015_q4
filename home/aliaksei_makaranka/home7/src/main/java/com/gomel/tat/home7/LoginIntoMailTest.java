package com.gomel.tat.home7;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.*;

public class LoginIntoMailTest extends Methods {

    private static final By USER_LINK_LOCATOR = By.id("nb-1");
    private static final By EXIT_BUTTON_LOCATOR = By.xpath("//a[text()='Выход']");
    private static final By ERROR_MSG_WRONG_LOGIN_OR_PASSWORD_LOCATOR = By.xpath("//div[@class='error-msg']");

    @Test()
    public void TestLoginIntoMailPositive() {
        LoginToYandex();
        WebElement userLink = driver.findElement(USER_LINK_LOCATOR);
        userLink.click();
        WebElement exitButton = driver.findElement(EXIT_BUTTON_LOCATOR);
        exitButton.click();
    }

    @Test()
    public void TestLoginIntoMailNegativeWrongPassword() {
        driver.get(YANDEX_START_PAGE);
        WebElement enterButton = driver.findElement(ENTER_BUTTON_LOCATOR);
        enterButton.click();
        WebElement loginInput = driver.findElement(LOGIN_INPUT_LOCATOR);
        loginInput.sendKeys(login);
        WebElement passwordInput = driver.findElement(PASSOWRD_INPUT_LOCATOR);
        passwordInput.sendKeys(wrongPassword);
        passwordInput.submit();
        Assert.assertTrue(driver.findElement(ERROR_MSG_WRONG_LOGIN_OR_PASSWORD_LOCATOR).isDisplayed());
    }
}

