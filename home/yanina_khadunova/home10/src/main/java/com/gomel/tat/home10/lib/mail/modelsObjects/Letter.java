package com.gomel.tat.home10.lib.mail.modelsObjects;


public class Letter {
    private String mailTo;
    private String mailSubject;
    private String mailContent;

    public void setMailTo(String mailTo) {
        this.mailTo = mailTo;
    }

    public String getMailTo() {
        return mailTo;
    }

    public void setMailSubject(String mailSubject) {
        this.mailSubject = mailSubject;
    }

    public String getMailSubject() {
        return mailSubject;
    }

    public void setMailContent(String mailContent) {
        this.mailContent = mailContent;
    }

    public String getMailContent() {
        return mailContent;
    }

}
