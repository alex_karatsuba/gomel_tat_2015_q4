package com.gomel.tat.home10.test.mail;

import com.gomel.tat.home10.lib.mail.screen.MenueToolsPage;
import com.gomel.tat.home10.lib.mail.service.LoginService;
import org.testng.Assert;
import org.testng.annotations.Test;

import static com.gomel.tat.home10.lib.common.service.AccountBuilder.*;

public class LoginTestSuccess extends Base {

    @Test(description = "Success mail login")
    public void loginMailSuccess() {
        LoginService loginService = new LoginService(myDriver);
        MenueToolsPage menueTools = new MenueToolsPage(myDriver);

        loginService.loginToMail(getDefaultAccount());
        Assert.assertEquals(menueTools.getUserIdentification(), getDefaultAccount().getLogin());
        System.out.println("Success mail login");
    }

}
