package com.gomel.tat.home10.lib.mail.service;

import com.gomel.tat.home10.lib.mail.screen.*;
import com.gomel.tat.home10.lib.ui.MyWebDriver;
import org.openqa.selenium.WebElement;

public class FindMailService {
    // private MyWebDriver myDriver;
    private MailBoxPage mailBoxPage;
    private MenueToolsPage menueToolsPage;

    public FindMailService(MyWebDriver myDriver) {
    //    this.myDriver = myDriver;
        mailBoxPage = new MailBoxPage(myDriver);
        menueToolsPage = new MenueToolsPage(myDriver);
    }

    public boolean searchLetterOutcoming(String mailId) {
        menueToolsPage.gotoOutcoming();
        WebElement maileExistOutcoming = mailBoxPage.findMail(mailId);

        if (maileExistOutcoming != null) {
            return true;
        } else {
            return false;
        }
    }

    public boolean searchLetterInbox(String mailId) {
        menueToolsPage.gotoInbox();
        WebElement maileExistInbox = mailBoxPage.findMail(mailId);

        if (maileExistInbox != null) {
            return true;
        } else {
            return false;
        }
    }

}
