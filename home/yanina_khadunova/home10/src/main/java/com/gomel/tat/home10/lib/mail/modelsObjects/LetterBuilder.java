package com.gomel.tat.home10.lib.mail.modelsObjects;

import static com.gomel.tat.home10.lib.common.service.CommonConstants.*;

public class LetterBuilder {

    public static Letter getDefaultLetter() {
        Letter letter = new Letter();
        letter.setMailTo(DEFAULT_MAIL_TO_SEND);
        letter.setMailSubject(DEFAULT_MAIL_SUBJECT);
        letter.setMailContent(DEFAULT_MAIL_CONTENET);
        return letter;
    }

    public static Letter getLetterWithoutToSendField() {
        Letter letter = getDefaultLetter();
        letter.setMailTo(WITHOUT_MAIL_TO_SEND);
        return letter;
    }

    public static Letter getLetterWithoutSubjectAdnBody() {
        Letter letter = getDefaultLetter();
        letter.setMailSubject(WITHOUT_MAIL_SUBJECT);
        letter.setMailContent(WITHOUT_MAIL_CONTENET);
        return letter;
    }

}
