package com.gomel.tat.home10.test.mail;

import com.gomel.tat.home10.lib.mail.screen.NewMailPage;
import com.gomel.tat.home10.lib.mail.service.*;
import org.testng.Assert;
import org.testng.annotations.*;

import static com.gomel.tat.home10.lib.mail.modelsObjects.LetterBuilder.*;

public class SendMailTest extends BaseLogin {

    @Test(description = "Success send message")
    public void mailTest() {
        MailService mailService = new MailService(myDriver);
        FindMailService findMailService = new FindMailService(myDriver);
        NewMailPage newMailPage = new NewMailPage(myDriver);

        mailService.sendLetter(getDefaultLetter());
        String mailId = newMailPage.getMailIdFromNotification();

        Assert.assertTrue(findMailService.searchLetterInbox(mailId));
        Assert.assertTrue(findMailService.searchLetterOutcoming(mailId));
        System.out.println("Success send message");
    }

}
