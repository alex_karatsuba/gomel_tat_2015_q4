package com.gomel.tat.home10.test.mail;


import com.gomel.tat.home10.lib.ui.MyWebDriver;
import org.testng.annotations.*;

import java.net.MalformedURLException;

import static com.gomel.tat.home10.lib.common.service.CommonConstants.*;
import static com.gomel.tat.home10.lib.mail.screen.LoginPage.*;

public class Base {
    protected MyWebDriver myDriver;

    @BeforeClass
    @Parameters({"webDriverName"})
    protected void setupDriver(String webDriverName) throws MalformedURLException {
        myDriver = new MyWebDriver(webDriverName);
    }

    @BeforeClass(dependsOnMethods = "setupDriver")
    protected void goToLoginPage() {
        myDriver.getDriver().get(MAILBOX_URL);
        myDriver.waitForShow(LOGIN_INPUT_LOCATOR);
        myDriver.waitForShow(PASSWORD_INPUT_LOCATOR);
    }

    @AfterClass
    protected void ClearTestClass() {
        myDriver.getDriver().quit();
    }

}
