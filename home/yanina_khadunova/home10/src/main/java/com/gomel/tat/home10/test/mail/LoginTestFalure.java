package com.gomel.tat.home10.test.mail;

import com.gomel.tat.home10.lib.common.service.AccountBuilder;
import com.gomel.tat.home10.lib.mail.screen.PassportPage;
import com.gomel.tat.home10.lib.mail.service.LoginService;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTestFalure extends Base {

    @Test(description = "Failure mail login")
    public void loginMailFailure() {
        LoginService loginService = new LoginService(myDriver);
        PassportPage pasportPage = new PassportPage(myDriver);

        loginService.loginToMail(AccountBuilder.getAccountWithWrongPass());
        Assert.assertNotNull(pasportPage.errorMessage());
        System.out.println("Failure mail login");
    }

}
