package com.gomel.tat.home10.lib.common.service;

import org.apache.commons.lang3.RandomStringUtils;

import static com.gomel.tat.home10.lib.common.service.CommonConstants.*;

public class AccountBuilder {

    public static Account getDefaultAccount() {
        Account account = new Account();
        account.setLogin(DEFAULT_MAIL_USER_LOGIN);
        account.setPassword(DEFAULT_MAIL_USER_PASSWORD);
        return account;
    }

    public static Account getAccountWithWrongPass() {
        Account acount = getDefaultAccount();
        acount.setPassword(acount.getPassword() + RandomStringUtils.randomAlphabetic(3));
        return acount;
    }

}
