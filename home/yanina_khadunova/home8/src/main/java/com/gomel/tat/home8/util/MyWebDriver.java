package com.gomel.tat.home8.util;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.*;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;


public class MyWebDriver {

    public static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 20;

    private WebDriver driver;
    private String driverName;
    Actions action;

    public MyWebDriver(String webDriverName) throws MalformedURLException {
        driverName = webDriverName;

        if (webDriverName.equalsIgnoreCase("firefox")) {
            driver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), DesiredCapabilities.firefox());
        } else if (webDriverName.equalsIgnoreCase("chrome")) {
            driver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), DesiredCapabilities.chrome());
        }

        driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        action = new Actions(driver);
    }

    public WebDriver getDriver() {
        return driver;
    }

    public WebElement waitForDisappear(By locator) {
        return waitForDisappear(locator, 5);
    }

    public WebElement waitForDisappear(By locator, long timeoutInSeconds) {
        new WebDriverWait(driver, timeoutInSeconds)
                .until(ExpectedConditions.invisibilityOfElementLocated(locator));
        return driver.findElement(locator);
    }

    public WebElement waitForShow(By locator) {
        return waitForShow(locator, 5);
    }

    public WebElement waitForShow(By locator, long timeoutInSeconds) {
        new WebDriverWait(driver, timeoutInSeconds)
                .until(ExpectedConditions.visibilityOfElementLocated(locator));
        return driver.findElement(locator);
    }

    public void clickOn(WebElement webElement) {
        if (driverName.equalsIgnoreCase("firefox")) {
            webElement.click();
        } else if (driverName.equalsIgnoreCase("chrome")) {
            action.moveToElement(webElement, 1, 1).click().perform();
        }
    }

}
