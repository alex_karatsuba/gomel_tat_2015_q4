package com.gomel.tat.home8.tests;

import com.gomel.tat.home8.ui.page.yandexmail.*;
import org.openqa.selenium.*;
import org.testng.Assert;
import org.testng.annotations.*;

public class SendMailTest extends BaseClass {

    public static final By NOTIFICATION_MAIL_SEND_LOCATOR = By.xpath("//a[@class='b-statusline__link']");

    private MenueToolsPage menueTools;

    @BeforeClass
    public void loginMail(){
        logOnMail();
        clickButtonEnter();
        menueTools = new MenueToolsPage(myDriver);

    }

    @BeforeMethod
    public void goOnMailPage(){
        menueTools.gotoInbox();
    }

    @Test(description = "Success send message", dataProvider = "sendMail")
    public void mailTest(String mailTo, String mailSubject, String mailContent) {
        NewMailPage newMail = new NewMailPage(myDriver);
        MailBoxPage mailBox = new MailBoxPage(myDriver);

        menueTools.clickButtonCompose();
        newMail.fillMailFields(mailTo,mailSubject,mailContent);
        newMail.clickSendMailButton();

        myDriver.waitForShow(NOTIFICATION_MAIL_SEND_LOCATOR);
        String mailId = newMail.getMailIdfromNotification(NOTIFICATION_MAIL_SEND_LOCATOR);

        menueTools.gotoOutcoming();
        WebElement maileExistOutcoming = mailBox.findMail(mailId);
        menueTools.gotoInbox();
        WebElement maileExistInbox = mailBox.findMail(mailId);

        Assert.assertNotNull(maileExistOutcoming);
        Assert.assertNotNull(maileExistInbox);
        System.out.println("Success send message");

    }

    @DataProvider(name = "sendMail")
    public Object[][] valuesForMap() {

        String mailTo = "epamtest2015@yandex.ru";
        String mailSubject = "test subject" + Math.random() * 100000000;
        String mailContent = "mail content" + Math.random() * 100000000;

        return new Object[][]{
                {mailTo, mailSubject, mailContent},
                {mailTo, "", ""}
        };
    }

}
