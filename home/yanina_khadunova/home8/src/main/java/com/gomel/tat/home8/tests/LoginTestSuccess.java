package com.gomel.tat.home8.tests;

import com.gomel.tat.home8.ui.page.yandexmail.MenueToolsPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTestSuccess extends BaseClass {

    @Test(description = "Success mail login")
    public void loginMailSuccess() {
        MenueToolsPage menueTools = new MenueToolsPage(myDriver);
        logOnMail();
        clickButtonEnter();
        Assert.assertEquals(menueTools.getUserIdentification(), userLogin);
        System.out.println("Success mail login");
    }

}
