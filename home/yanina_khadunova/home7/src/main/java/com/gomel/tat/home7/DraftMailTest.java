package com.gomel.tat.home7;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;

public class DraftMailTest extends SendMailTestBase {

    // UI data
    public static final By DELETE_BUTTON_LOCATOR = By.xpath("//div[@class='block-toolbar']//div[2]/a[8]");
    public static final By DRAFT_MAIL_LINK_LOCATOR = By.xpath("//a[@href='#draft' and contains(@data-params,'folder')]");
    public static final By DELETED_MAIL_LINK_LOCATOR = By.xpath("//a[@href='#trash' and contains(@data-params,'folder')]");
    public static final By NOTIFICATION_DRAFT_MAIL_CREATE_LOCATOR = By.xpath("//div[@data-compose-type='letter postcard']//span[contains(@data-action,'save-dropdown')]");
    public static final By NOTIFICATION_MAIL_DELETED_LOCATOR = By.xpath("//div[@class ='b-statusline']");

    private String mailTo = "epamtest2015@yandex.ru"; // ENUM
    private String mailSubject = "test subject" + Math.random() * 100000000; // RANDOM
    private String mailContent = "mail content" + Math.random() * 100000000;// RANDOM

    @Test(description = "Create and delete draft mail", expectedExceptions = NoSuchElementException.class)
    public void sendMessage() throws InterruptedException {
        fillNewMailFields(mailTo, mailSubject, mailContent);

        waitForShow(NOTIFICATION_DRAFT_MAIL_CREATE_LOCATOR);

        String currentUrl = driver.getCurrentUrl();
        int index = currentUrl.indexOf("#");
        String draftId = currentUrl.substring(index + 9);

        WebElement draftMail = driver.findElement(DRAFT_MAIL_LINK_LOCATOR);
        draftMail.click();
        WebElement findMailDraft = driver.findElement(By.xpath("//a[contains(@href,'" + draftId + "')]//ancestor::div[@data-action='mail.message.show-or-select']//input"));
        findMailDraft.click();
        WebElement deleteButton = driver.findElement(DELETE_BUTTON_LOCATOR);
        deleteButton.click();

        waitForShow(NOTIFICATION_MAIL_DELETED_LOCATOR);

        WebElement deletedMail = driver.findElement(DELETED_MAIL_LINK_LOCATOR);
        deletedMail.click();
        WebElement findMailDeleted = driver.findElement(By.xpath("//a[contains(@href,'" + draftId + "')]//ancestor::div[@data-action='mail.message.show-or-select']//input"));
        findMailDeleted.click();
        WebElement deletePermanentlyButton = driver.findElement(DELETE_BUTTON_LOCATOR);
        deletePermanentlyButton.click();

        Thread.sleep(2000);
        driver.findElement(By.xpath("//a[contains(@href,'" + draftId + "')]//ancestor::div[@data-action='mail.message.show-or-select']//input"));
    }

    private WebElement waitForShow(By locator) {
        new WebDriverWait(driver, 5000)
                .until(ExpectedConditions.visibilityOfElementLocated(locator));
        return driver.findElement(locator);
    }
}