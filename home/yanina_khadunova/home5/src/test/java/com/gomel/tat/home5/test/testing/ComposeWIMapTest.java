package com.gomel.tat.home5.test.testing;

import com.gomel.tat.home5.WordList;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.*;

public class ComposeWIMapTest {

    @Test(groups = {"first", "second"})
    public void wordCountThree() {
        Map<String, Integer> resultsMap = new HashMap<String, Integer>();
        resultsMap.put("Hello", 1);
        resultsMap.put("Hi", 2);
        resultsMap.put("No", 3);

        List<String> wordList = Arrays.asList("Hello", "No", "Hi", "No", "No", "Hi");
        Map<String, Integer> realMap = WordList.composeWordInclusionMap(wordList);
        Assert.assertEquals(realMap, resultsMap);
        System.out.println("wordCount-base-Three");
    }

    @Test(groups = "first")
    public void wordCountTwo() {
        Map<String, Integer> resultsMap = new HashMap<String, Integer>();
        resultsMap.put("Hello", 2);
        resultsMap.put("Hi", 2);

        List<String> wordList = Arrays.asList("Hello", "Hi", "Hi", "Hello");
        Map<String, Integer> realMap = WordList.composeWordInclusionMap(wordList);
        Assert.assertEquals(realMap, resultsMap);
        System.out.println("wordCount-base-Two");
    }

    @Test(groups = "first")
    public void wordCountOne() {
        Map<String, Integer> resultsMap = new HashMap<String, Integer>();
        resultsMap.put("Hello", 2);

        List<String> wordList = Arrays.asList("Hello", "Hello");
        Map<String, Integer> realMap = WordList.composeWordInclusionMap(wordList);
        Assert.assertEquals(realMap, resultsMap);
        System.out.println("wordCount-base-One");
    }

    @Test(expectedExceptions = IllegalArgumentException.class, dependsOnGroups = "first")
    public void wordCountException() {
        System.out.println("wordCount-Exception");
        WordList.composeWordInclusionMap(null);
    }
}
