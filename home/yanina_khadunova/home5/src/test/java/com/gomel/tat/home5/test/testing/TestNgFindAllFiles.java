package com.gomel.tat.home5.test.testing;

import com.gomel.tat.home5.WordList;
import org.testng.Assert;
import org.testng.annotations.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class TestNgFindAllFiles {
    private List<Path> path;
    private Path testDirectory;
    private Path testFile1;
    private Path testFile2;
    private Path testFile3;

    public TestNgFindAllFiles (List<Path> path){
        this.path = path;
    }

    @BeforeClass
    public void beforeClass() throws IOException {
        System.out.println("Before class allFileF");
        testDirectory = Paths.get("C:", "Child");
        Files.createDirectories(testDirectory);
    }

    @BeforeMethod
    public void bm() throws IOException {
        System.out.println("Before method allFileF");

        testFile1 = testDirectory.resolve("Ho.txt");
        testFile2 = testDirectory.resolve("Hello.txt");
        testFile3 = testDirectory.resolve("file.txt");
        Files.createFile(testFile1);
        Files.createFile(testFile2);
        Files.createFile(testFile3);
    }

    @AfterMethod
    public void am() throws IOException {
        System.out.println("After method allFileF");

        Files.deleteIfExists(testFile1);
        Files.deleteIfExists(testFile2);
        Files.deleteIfExists(testFile3);
        testFile1 = null;
        testFile2 = null;
        testFile3 = null;
    }

    @AfterClass
    public void afterClass() throws IOException {
        Files.deleteIfExists(testDirectory);
        System.out.println("After class allFileF");
    }

    public class TestNgFindAllFilesFactory {

        @Factory
        public Object[] createInstance() {

            Path path1 = Paths.get("C://Child/Hello.txt");
            Path path2 = Paths.get("C://Child/Ho.txt");
            List<Path> path = new ArrayList<Path>();
            path.add(path1);
            path.add(path2);
            return new Object[] {new TestNgFindAllFiles(path)};

        }
    }

    @Parameters({ "baseDirectory", "fileNamePattern" })
    @Test
    public void testPath() throws IOException {
        List<Path> resultList = path;

        List<Path> realList = WordList.findAllFiles("C://Child/", "h*.txt");
        Assert.assertEquals(realList, resultList);
        System.out.println("allFileFactoryFactory");
    }
}
