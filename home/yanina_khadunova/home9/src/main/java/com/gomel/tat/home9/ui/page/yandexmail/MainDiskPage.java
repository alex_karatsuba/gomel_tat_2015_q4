package com.gomel.tat.home9.ui.page.yandexmail;

import com.gomel.tat.home9.util.MyWebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class MainDiskPage {
    protected MyWebDriver myDriver;
    public static final By UPLOAD_BUTTON_LOCATOR = By.xpath("//div[@class='header']//input[@type='file']");
    public static final By DISK_LINK_LOCATOR = By.xpath("//div[@class='header__side-left']/a[2]");
    public static final By TRASH_ICON_LOCATOR = By.xpath("//div[@data-id='/trash']/div");

    public MainDiskPage(MyWebDriver myDriver){
        this.myDriver = myDriver;
    }

    public WebElement uploadFiles(){
        WebElement upload = myDriver.getDriver().findElement(UPLOAD_BUTTON_LOCATOR);
        return upload;
    }

    public void diskLinkShow(){
        myDriver.waitForShow(DISK_LINK_LOCATOR,20);

    }

    public WebElement findUploadFile (By path){
        WebElement uploadFile = myDriver.waitForShow(path,20);
        return uploadFile;

    }

    public WebElement trash () {
        WebElement trash = myDriver.getDriver().findElement(TRASH_ICON_LOCATOR);
        return trash;
    }

}
