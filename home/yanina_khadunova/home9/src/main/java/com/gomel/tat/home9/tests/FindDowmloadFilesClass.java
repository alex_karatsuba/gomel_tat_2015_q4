package com.gomel.tat.home9.tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.nio.file.Path;
import java.util.List;


public class FindDowmloadFilesClass {

    public String fileContent(Path path) throws IOException {
        Charset charset = Charset.forName("UTF-8");
        List<String> content = Files.readAllLines(path, charset);
        StringBuilder sbuilder = new StringBuilder();
        for (String s : content) {
            sbuilder.append(s);
        }
        return sbuilder.toString();

    }
}
