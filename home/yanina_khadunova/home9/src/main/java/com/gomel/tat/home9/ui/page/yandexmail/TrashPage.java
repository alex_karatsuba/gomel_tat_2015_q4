package com.gomel.tat.home9.ui.page.yandexmail;

import com.gomel.tat.home9.util.MyWebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class TrashPage {
    protected MyWebDriver myDriver;
    public static final By FILE_DELETED_NOTIFICATION_LOCATOR = By.xpath("//div[@data-key='box=layerWdg']//div[contains(@class,'item_moved')]");

    public TrashPage(MyWebDriver myDriver) {
        this.myDriver = myDriver;
    }

    public WebElement findUploadFile(By path) {
        WebElement uploadFile = myDriver.waitForShow(path, 30);
        return uploadFile;

    }

    public WebElement fileDeletedNotification() {
        WebElement deletedNotofication = myDriver.waitForShow(FILE_DELETED_NOTIFICATION_LOCATOR,20);
        return deletedNotofication;

    }
}
