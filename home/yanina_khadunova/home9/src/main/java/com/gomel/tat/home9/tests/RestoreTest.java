package com.gomel.tat.home9.tests;

import com.gomel.tat.home9.ui.page.yandexmail.MainDiskPage;
import com.gomel.tat.home9.ui.page.yandexmail.TrashPage;
import com.gomel.tat.home9.ui.page.yandexmail.TrashPanelPage;
import com.gomel.tat.home9.ui.page.yandexmail.UploadModalWindowPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.nio.file.Files;


public class RestoreTest extends BaseClass {
    public static final By UPLOAD_FILE_DISK_LOCATOR = By.xpath("//div[contains(@data-id,'disk/home9.txt')]");
    public static final By UPLOAD_FILE_TRASH_LOCATOR = By.xpath("//div[contains(@data-id,'trash/home9.txt')]");
    public static String uploadFrom = "C:\\home9\\upload\\home9.txt";

    private MainDiskPage mainDiskPage;
    private UploadModalWindowPage uploadModalWindow;
    private TrashPage trashPage;
    private TrashPanelPage trashPanelPage;
    private DiskClass commonActions;

    @Test(description = "Restore test")
    public void restore() throws InterruptedException, IOException {
        mainDiskPage = new MainDiskPage(myDriver);
        uploadModalWindow = new UploadModalWindowPage(myDriver);
        trashPage = new TrashPage(myDriver);
        trashPanelPage = new TrashPanelPage(myDriver);
        commonActions = new DiskClass();

        logOnMail();
        goToDisk();
        mainDiskPage.diskLinkShow();
        WebElement uploadFile = commonActions.upload(UPLOAD_FILE_DISK_LOCATOR, uploadFrom, myDriver);
        commonActions.removeToTrash(uploadFile, myDriver);
        commonActions.goToTrash(myDriver);
        WebElement uploadFileInTrash = trashPage.findUploadFile(UPLOAD_FILE_TRASH_LOCATOR);

        uploadFileInTrash.click();
        trashPanelPage.restoreFile();

        myDriver.getDriver().navigate().back();
        WebElement restoreFile = mainDiskPage.findUploadFile(UPLOAD_FILE_DISK_LOCATOR);

        Assert.assertNotNull(uploadFile);
        Assert.assertNotNull(uploadFileInTrash);
        Assert.assertNotNull(restoreFile);

        System.out.println("Restore test success");

    }

    @AfterMethod(description = "Delete the test data")
    public void deletedTestData() throws IOException, InterruptedException {
        mainDiskPage = new MainDiskPage(myDriver);
        commonActions = new DiskClass();

        WebElement uploadedFile = mainDiskPage.findUploadFile(UPLOAD_FILE_DISK_LOCATOR);
        commonActions.removePermanently(uploadedFile, myDriver , UPLOAD_FILE_TRASH_LOCATOR);
    }
}