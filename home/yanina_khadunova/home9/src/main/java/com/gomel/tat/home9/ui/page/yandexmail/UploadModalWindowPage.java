package com.gomel.tat.home9.ui.page.yandexmail;

import com.gomel.tat.home9.util.MyWebDriver;
import org.openqa.selenium.*;

public class UploadModalWindowPage {

    protected MyWebDriver myDriver;
    public static final By CHECK_COMPLETE_LOCATOR = By.xpath("//div[contains(@class,'icon_done')]");
    public static final By CLOSE_BUTTON_LOCATOR = By.xpath("//div[@class='b-dialog-upload__actions']/button[4]");

    public UploadModalWindowPage(MyWebDriver myDriver) {
        this.myDriver = myDriver;
    }

    public void uploadCheckComplete() {
        myDriver.waitForShow(CHECK_COMPLETE_LOCATOR,90);
    }

    public void closeUploadModalWindow() {
        WebElement closeUploadModalWindow = myDriver.waitForShow(CLOSE_BUTTON_LOCATOR);
        closeUploadModalWindow.click();
    }


}
