package com.gomel.tat.home9.tests;

import com.gomel.tat.home9.ui.page.yandexmail.*;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.*;
import org.testng.Assert;
import org.testng.annotations.*;

import java.io.IOException;
import java.nio.file.*;

public class RemoveSeveralElementsTest extends BaseClass {
    public static final By UPLOAD_FILE_ONE_DISK_LOCATOR = By.xpath("//div[contains(@data-id,'disk/home9.txt')]");
    public static final By UPLOAD_FILE_TWO_DISK_LOCATOR = By.xpath("//div[contains(@data-id,'disk/home9copy.txt')]");
    public static final By UPLOAD_FILE_ONE_TRASH_LOCATOR = By.xpath("//div[contains(@data-id,'trash/home9.txt')]");
    public static final By UPLOAD_FILE_TWO_TRASH_LOCATOR = By.xpath("//div[contains(@data-id,'trash/home9copy.txt')]");
    public static String uploadFromOne = "C:\\home9\\upload\\home9.txt";
    public static String uploadFromTwo = "C:\\home9\\upload\\home9copy.txt";


    private MainDiskPage mainDiskPage;
    private UploadModalWindowPage uploadModalWindowPage;
    private MainDiskPanelPage mainDiskPanelPage;
    private DiskClass commonActions;
    private TrashPage trashPage;
    private TrashPanelPage trashPanelPage;

    @Test(description = "Remove Several Elements")
    public void removeSeveral() {
        mainDiskPage = new MainDiskPage(myDriver);
        uploadModalWindowPage = new UploadModalWindowPage(myDriver);
        trashPage = new TrashPage(myDriver);
        trashPanelPage = new TrashPanelPage(myDriver);
        commonActions = new DiskClass();
        mainDiskPanelPage = new MainDiskPanelPage(myDriver);

        logOnMail();
        goToDisk();
        mainDiskPage.diskLinkShow();
        WebElement uploadFileOne = commonActions.upload(UPLOAD_FILE_ONE_DISK_LOCATOR, uploadFromOne, myDriver);
        WebElement uploadFileTwo = commonActions.upload(UPLOAD_FILE_TWO_DISK_LOCATOR, uploadFromTwo, myDriver);

        uploadFileOne.click();
        Action severalElements = new Actions(myDriver.getDriver())
                .sendKeys(Keys.CONTROL).sendKeys(Keys.SHIFT).build();
        severalElements.perform();
        uploadFileTwo.click();
        severalElements.perform();

        mainDiskPanelPage.deleteSeveralFiles();
        commonActions.goToTrash(myDriver);

        WebElement uploadFileInTrashOne = myDriver.waitForShow(UPLOAD_FILE_ONE_TRASH_LOCATOR, 20);
        WebElement uploadFileInTrashTwo = myDriver.waitForShow(UPLOAD_FILE_TWO_TRASH_LOCATOR, 20);

        Assert.assertNotNull(uploadFileInTrashOne);
        Assert.assertNotNull(uploadFileInTrashTwo);

        System.out.println("Remove Several Elements test success");

    }

    @AfterMethod(description = "Delete the test data")
    public void deletedTestData() throws IOException, InterruptedException {
        commonActions = new DiskClass();

        commonActions.removeFromTrashPermanently(myDriver, UPLOAD_FILE_ONE_TRASH_LOCATOR);
        commonActions.removeFromTrashPermanently(myDriver, UPLOAD_FILE_TWO_TRASH_LOCATOR);

    }
}
