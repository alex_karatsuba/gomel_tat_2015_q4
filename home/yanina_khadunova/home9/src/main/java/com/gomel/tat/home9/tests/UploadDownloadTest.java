package com.gomel.tat.home9.tests;

import com.gomel.tat.home9.ui.page.yandexmail.*;
import org.openqa.selenium.*;
import org.testng.Assert;
import org.testng.annotations.*;
import java.io.IOException;
import java.nio.file.*;


public class UploadDownloadTest extends BaseClass {
    public static final By UPLOAD_FILE_DISK_LOCATOR = By.xpath("//div[contains(@data-id,'disk/home9.txt')]");
    public static final By UPLOAD_FILE_TRASH_LOCATOR = By.xpath("//div[contains(@data-id,'trash/home9.txt')]");
    private static final Path PATH_DOWNLOAD = Paths.get("C:\\home9\\download\\home9.txt");
    public static String uploadFrom = "C:\\home9\\upload\\home9.txt";

    private MainDiskPage mainDiskPage;
    private UploadModalWindowPage uploadModalWindowPage;
    private MainDiskPanelPage mainDiskPanelPage;
    private FindDowmloadFilesClass findDowmloadFilePC;
    private DiskClass commonActions;

    @Test(description = "Upload file and download file")
    public void uploadDownload() throws InterruptedException, IOException {
        mainDiskPage = new MainDiskPage(myDriver);
        uploadModalWindowPage = new UploadModalWindowPage(myDriver);
        mainDiskPanelPage = new MainDiskPanelPage(myDriver);
        findDowmloadFilePC = new FindDowmloadFilesClass();

        logOnMail();
        goToDisk();

        mainDiskPage.diskLinkShow();
        mainDiskPage.uploadFiles().sendKeys(uploadFrom);
        uploadModalWindowPage.uploadCheckComplete();
        uploadModalWindowPage.closeUploadModalWindow();

        WebElement uploadFile = mainDiskPage.findUploadFile(UPLOAD_FILE_DISK_LOCATOR);
        uploadFile.click();
        mainDiskPanelPage.downloadFile();

        while (!Files.exists(PATH_DOWNLOAD)) {
            Thread.sleep(1000);
        }

        String expecteContentFile = new String("Hello World!");
        String realCcontentFile = findDowmloadFilePC.fileContent(PATH_DOWNLOAD);

        Assert.assertNotNull(uploadFile);
        Assert.assertEquals(realCcontentFile, expecteContentFile);

        System.out.println("Upload file and download file test success");
    }

    @AfterMethod(description = "Delete the test data")
    public void deletedTestData() throws IOException, InterruptedException {
        mainDiskPage = new MainDiskPage(myDriver);
        commonActions = new DiskClass();

        Files.deleteIfExists(PATH_DOWNLOAD);
        WebElement uploadedFile = mainDiskPage.findUploadFile(UPLOAD_FILE_DISK_LOCATOR);
        commonActions.removePermanently(uploadedFile, myDriver, UPLOAD_FILE_TRASH_LOCATOR);

    }

}
