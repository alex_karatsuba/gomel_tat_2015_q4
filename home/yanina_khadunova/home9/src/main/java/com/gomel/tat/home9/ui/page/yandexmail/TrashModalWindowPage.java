package com.gomel.tat.home9.ui.page.yandexmail;

import com.gomel.tat.home9.util.MyWebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class TrashModalWindowPage {
    protected MyWebDriver myDriver;
    public static final By EMPTY_BUTTON_LOCATOR = By.xpath("//div[@data-key='box=layerModal']/div[contains(@class,'ns-view-visible')]//button[contains(@class,'action_right')]");

    public TrashModalWindowPage(MyWebDriver myDriver) {
        this.myDriver = myDriver;
    }

    public void clearTrash() {
        WebElement clear = myDriver.waitForShow(EMPTY_BUTTON_LOCATOR);
        clear.click();
    }
}
