package com.gomel.tat.util;

        import org.openqa.selenium.By;
        import org.openqa.selenium.Platform;
        import org.openqa.selenium.WebDriver;
        import org.openqa.selenium.WebElement;
        import org.openqa.selenium.chrome.ChromeDriver;
        import org.openqa.selenium.firefox.FirefoxDriver;
        import org.openqa.selenium.remote.DesiredCapabilities;
        import org.openqa.selenium.remote.RemoteWebDriver;
        import org.openqa.selenium.support.ui.ExpectedConditions;
        import org.openqa.selenium.support.ui.WebDriverWait;
        import java.net.MalformedURLException;
        import java.net.URL;
        import java.util.concurrent.TimeUnit;


public class WebDriverUtility {

    public static final String BASE_URL = "http://mail.yandex.ru";
    public static final String YANDEX_START_PAGE = "http://mail.yandex.ru";
    public static final By COMPOSE_CANCEL_BUTTON_LOCATOR = By.xpath(".//*[@data-action='compose.close']");
    public static final By SAVE_DIALOG_POPUP_LOCATOR = By.xpath(".//p[text()='Сохранить сделанные изменения?']");
    public static final By SAVE_DIALOG_CANCEL_BUTTON_LOCATOR = By.xpath(".//*[@data-action='dialog.dont_save']");
    public static final By DROPDOWN_ACCOUNT_MENU_LOCATOR= By.xpath(".//*[@class='header-user-name js-header-user-name']");
    public static final By DROPDOWN_LOGOFF_LOCATOR=By.xpath(".//*[@data-metric='Меню сервисов:Выход']");
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
    public static final By INBOX_LINK_LOCATOR = By.xpath("//a[@href='#inbox']");

    public static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 5;
    public static final int DRIVE_IMPL_WAIT_TIMEOUT_SECONDS = 3;

    private static WebDriver driver;

    public static void firefoxLocalLaunch() {
        driver = new FirefoxDriver();
        driver.get(YANDEX_START_PAGE);
    }

    public static void chromeLocalLaunch() {
        System.setProperty("webdriver.chrome.driver", "chromedriver");
        driver = new ChromeDriver();
        driver.get(YANDEX_START_PAGE);
    }

    public static WebDriver firefoxRemoteLaunch() throws MalformedURLException {
        // java -jar selenium-server-standalone-2.45.0.jar -port 4444
        standAloneServerLaunch("FF");
        driver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), DesiredCapabilities.firefox());
        return  driver;
    }

    public static WebDriver chromeRemoteLaunch() throws MalformedURLException {
        // java -jar selenium-server-standalone-2.45.0.jar -port 4444 -Dwebdriver.chrome.driver="d:\path\to\chromedriver.exe"
        standAloneServerLaunch("CR");
        DesiredCapabilities capability = DesiredCapabilities.chrome();
        capability.setBrowserName("chrome" );
        capability.setPlatform(Platform.LINUX);
        capability.setVersion("3.6");
        driver = new RemoteWebDriver(new URL("http://127.0.0.1:4444/wd/hub"), capability);
        return  driver;
    }

    public static void standAloneServerLaunch(String browser) {
        String[] cmd;
        if (browser=="FF"){
            cmd = new String[]{"java", "-jar", "selenium-server-standalone-2.48.2.jar", "-port4444"};
        } else if (browser=="CR") {
            cmd = new String[]{"java", "-jar", "selenium-server-standalone-2.48.2.jar", "-port4444", "-Dwebdriver.chrome.driver=chromedriver"};
        } else { return; }
        try {
            Runtime.getRuntime().exec(cmd);
        } catch (Exception e) {
            System.err.println("Problem with launching standalone-server: " + e.getMessage());
        }
    }

    public static boolean isLoggedInCheck () {
        return !(0==driver.findElements(INBOX_LINK_LOCATOR).size());
    }

    public void YandexLogoff () {
        WebElement dropdownMenu = driver.findElement(DROPDOWN_ACCOUNT_MENU_LOCATOR);
        dropdownMenu.click();
        WebElement exitMenu = driver.findElement(DROPDOWN_LOGOFF_LOCATOR);
        exitMenu.click();
    }


    public static WebDriver getWebDriver() {
        if (driver == null) {
            standAloneServerLaunch("FF"); // "FF" for Firefox, "CR" for Chrome
            WebDriver driver=null;
            try {
                driver = firefoxRemoteLaunch();
                //driver = chromeRemoteLaunch();
            } catch (MalformedURLException e) {
                System.err.println("Problem with launching driver: " + e.getMessage());
            }
            driver.manage().window().maximize();
            driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
            driver.manage().timeouts().implicitlyWait(DRIVE_IMPL_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        }
        return driver;
    }

    public static void shutdownWebDriver() {
        try {
            driver.quit();
        } catch (Exception e) {
            System.err.println("Problem with shutting down driver: " + e.getMessage());
        }
    }

    public static WebElement waitForElementIsClickable(By locator) {
        new WebDriverWait(driver, 5).until(ExpectedConditions.elementToBeClickable(locator));
        return driver.findElement(locator);
    }

    public static WebElement waitForElementIsDisplayed(By locator) {
        new WebDriverWait(driver, 5).until(ExpectedConditions.visibilityOfElementLocated(locator));
        return driver.findElement(locator);
    }

    public static WebElement waitForDisappear(By locator) {
        new WebDriverWait(driver, 5)
                .until(ExpectedConditions.invisibilityOfElementLocated(locator));
        return driver.findElement(locator);
    }

}