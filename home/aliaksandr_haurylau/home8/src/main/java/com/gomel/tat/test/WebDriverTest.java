package com.gomel.tat.test;

import com.gomel.tat.baseobj.*;
import com.gomel.tat.ui.*;
import com.gomel.tat.util.WebDriverUtility;
import junit.framework.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;


public class WebDriverTest extends WebDriverUtility {

    protected WebDriver driver;

    private static String userLogin = "a22333g";
    private static String userPassword = "cnhfyyjctrhtnysq2";
    private static String mailTo = "a22333g@yandex.ru";
    private Letter letter;


    @Test
    public void testLoginNegative() {

        if (isLoggedInCheck()) {YandexLogoff();}
        LoginPage loginPage = new LoginPage(driver);
        loginPage.open();
        loginPage.login(userLogin,"NotAPassword");
        Assert.assertTrue(!isLoggedInCheck());
    }


    @Test
    public void testLoginPositive() {

        if (isLoggedInCheck()) {YandexLogoff();}
        LoginPage loginPage = new LoginPage(driver);
        loginPage.open();
        loginPage.login(userLogin, userPassword);
        Assert.assertTrue(isLoggedInCheck());
    }

    @Test
     public void testSendMailPositive(){
        letter = LetterFactory.getRandomLetter();
        String letterID;

        ComposePage composePage = loginAndComposeLetter();
        letterID=composePage.sendLetter(letter);
        SentboxPage sentboxPage = new SentboxPage(driver);
        sentboxPage.open();
        Assert.assertTrue(sentboxPage.isMessageWithIDPresent(letterID));
     }

     @Test
     public void testSendMailNegative(){
         String mailSubject = "test subject" + Math.random() * 100000000;
         letter = new Letter("NotAnAdress",mailSubject,"any");

         ComposePage composePage = loginAndComposeLetter();
         composePage.sendLetterUnchecked(letter);
         WebElement cancelButton=driver.findElement(COMPOSE_CANCEL_BUTTON_LOCATOR);
         cancelButton.click();
         waitForElementIsDisplayed(SAVE_DIALOG_POPUP_LOCATOR);
         cancelButton=driver.findElement(SAVE_DIALOG_CANCEL_BUTTON_LOCATOR);
         cancelButton.click();
         SentboxPage sentboxPage = new SentboxPage(driver);
         sentboxPage.open();
         Assert.assertTrue(!sentboxPage.isLetterPresent(letter));
     }


    @Test
        public void testSendMailWithoutSubject(){
            String mailContent = "test content" + Math.random() * 100000000;
            letter = new Letter("a22333g@yandex.ru","",mailContent);
            String letterID;

        ComposePage composePage = loginAndComposeLetter();
            letterID=composePage.sendLetter(letter);
            InboxPage inboxPage = new InboxPage(driver);
            inboxPage.open();
            Assert.assertTrue(inboxPage.isMessageWithIDPresent(letterID));
        }


        @Test
        public void testSendMailWithoutContent(){
            String mailSubject = "test subject" + Math.random() * 100000000;
            letter = new Letter("a22333g@yandex.ru",mailSubject,"");
            String letterID;

            ComposePage composePage = loginAndComposeLetter();
            letterID=composePage.sendLetter(letter);
            InboxPage inboxPage = new InboxPage(driver);
            inboxPage.open();
            Assert.assertTrue(inboxPage.isMessageWithIDPresent(letterID));
        }

        @Test
        public void testCreateAndDeleteDraft() throws InterruptedException {
            letter = LetterFactory.getRandomLetter();

            ComposePage composePage = loginAndComposeLetter();
            composePage.createDraft(letter);
            waitForDisappear(SEND_MAIL_BUTTON_LOCATOR);
            DraftPage draftPage = new DraftPage(driver);
            draftPage.open();
            draftPage.deleteLetter(letter);
            TrashPage trashPage = new TrashPage(driver);
            trashPage.open();
            Assert.assertTrue(trashPage.isLetterPresent(letter));
        }

        @Test
        public void testDraftPermanentDelete(){
            letter = LetterFactory.getRandomLetter();

            ComposePage composePage = loginAndComposeLetter();
            composePage.createDraft(letter);
            waitForDisappear(SEND_MAIL_BUTTON_LOCATOR);
            DraftPage draftPage = new DraftPage(driver);
            draftPage.open();
            draftPage.deleteLetter(letter);
            TrashPage trashPage = new TrashPage(driver);
            trashPage.open();
            trashPage.deleteLetter(letter);
            Assert.assertTrue(!trashPage.isLetterPresent(letter));
        }

    private ComposePage loginAndComposeLetter() {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.open();
        loginPage.login(userLogin, userPassword);
        ComposePage composePage = new ComposePage(driver);
        composePage.open();
        return composePage;
    }

    @BeforeSuite
    public void prepeareForTests() {
        driver = WebDriverUtility.getWebDriver();
    }

    @AfterSuite
    public void closeWebdriver() {
        WebDriverUtility.shutdownWebDriver();
    }

}
