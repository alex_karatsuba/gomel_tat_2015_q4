package com.gomel.tat.baseobj;

public class Letter {

    private String mailto;
    private String subject;
    private String body;


    public Letter(String mailto, String subject, String body) {
        this.mailto = mailto;
        this.subject = subject;
        this.body = body;
    }

    public String getMailto() {
        return mailto;
    }

    public String getSubject() {
        return subject;
    }

    public String getBody() {
        return body;
    }
}