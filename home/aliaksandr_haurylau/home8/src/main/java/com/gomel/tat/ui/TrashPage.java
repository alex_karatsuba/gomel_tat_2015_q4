package com.gomel.tat.ui;

import com.gomel.tat.baseobj.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import static com.gomel.tat.util.WebDriverUtility.waitForElementIsClickable;



public class TrashPage extends Page {

    public static final By TRASH_LINK_LOCATOR = By.xpath("//a[@href='#trash']");
    public static final String TRASH_LINK_LOCATOR_PATTERN = "//*[@class='block-messages']//a[contains(., '%s')]";
    public static final By DELETE_MAIL_BUTTON_LOCATOR = By.xpath("//a[@data-action='delete']");

    public TrashPage(WebDriver driver) {
        super(driver);
    }


    public void open()    {
        waitForElementIsClickable(TRASH_LINK_LOCATOR).click();
    }

    public boolean isMessageWithIDPresent(String messageID) {
        try {
            new WebDriverWait(driver, TIME_OUT_WEBDRIVER_WAIT_SECONDS)
                    .until(ExpectedConditions.visibilityOfElementLocated(
                            By.xpath("//a[contains(@href,'"+messageID+"')]")));
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public boolean isLetterPresent(Letter letter) {
        try {
            new WebDriverWait(driver, TIME_OUT_WEBDRIVER_WAIT_SECONDS)
                    .until(ExpectedConditions.visibilityOfElementLocated(
                            By.xpath(String.format(TRASH_LINK_LOCATOR_PATTERN, letter.getSubject()))));
        } catch (Exception e) {
            return false;
        }
        return true;
    }


    public void deleteLetter(Letter letter) {
        WebElement thisLetter = driver.findElement(By.xpath(".//*[@title='"+letter.getSubject()+"']"));
        thisLetter.click();
        waitForElementIsClickable(DELETE_MAIL_BUTTON_LOCATOR);
        WebElement deleteButton= driver.findElement(DELETE_MAIL_BUTTON_LOCATOR);
        deleteButton.click();
    }

}
