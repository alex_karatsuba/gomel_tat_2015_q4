package com.gomel.tat.tests;

import com.gomel.tat.home5.WordList;
import junit.framework.Assert;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import java.io.File;
import java.util.List;

@Listeners(MyTestListener.class)
public class FindFilesWithPatternTest extends WordListTest {

    private WordList wordList = new WordList();

    @Test
    public void testFindWithEmptyFolderParameter(){
        List<File> inventoryOfFiles=wordList.getListOfFiles("","*.txt");
        Assert.assertTrue(inventoryOfFiles.isEmpty());
    }

    @Test
    public void testFindWithEmptyPatternParameter(){
        List<File> inventoryOfFiles=wordList.getListOfFiles("./src/test/resources/testfolder/","");
        Assert.assertTrue(inventoryOfFiles.isEmpty());
    }


    @Test
    public void testFindNoFiles(){
        List<File> inventoryOfFiles=wordList.getListOfFiles("./src/test/resources/testfolder/","nobody.here");
        Assert.assertTrue(inventoryOfFiles.isEmpty());
    }

    @Test
    public void testFindNoFolders(){
        List<File> inventoryOfFiles=wordList.getListOfFiles("./src/test/resources/no_such_folder/","*.*");
        Assert.assertTrue(inventoryOfFiles.isEmpty());
    }

    @Test
    public void testFindOneFile(){
        List<File> inventoryOfFiles=wordList.getListOfFiles("./src/test/resources/testfolder/","*folder1/textfile1.txt");
        Assert.assertTrue(1==inventoryOfFiles.size());
    }

    @Test
    public void testFindFilesByMask(){
         List<File> inventoryOfFiles=wordList.getListOfFiles("./src/test/resources/testfolder/folder3/","*.txt");
         Assert.assertTrue(2==inventoryOfFiles.size());
    }

    @Test
    public void testFindFoldersByMask(){
        List<File> inventoryOfFiles=wordList.getListOfFiles("./src/test/resources/testfolder/","*estfol*.txt");
        Assert.assertTrue(6==inventoryOfFiles.size());
    }

    @Test(expectedExceptions = RuntimeException.class)
    public void testExpectedException() {
        // this test should fail for listener can show what it does
        // but also it used as 'excluded' example in WordListTests1.xml suite
         }


}
