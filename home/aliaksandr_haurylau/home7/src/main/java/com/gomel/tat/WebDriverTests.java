package com.gomel.tat;

import junit.framework.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;


public class WebDriverTests extends WebDriverUtility{

    private WebDriverUtility u = new WebDriverUtility();

    private static String userLogin = "a22333g@yandex.ru"; // ACCOUNT
    private static String userPassword = "cnhfyyjctrhtnysq2"; // ACCOUNT
    private static String mailTo = "a22333g@yandex.ru"; // ENUM
    public static final String YANDEX_START_PAGE = "http://mail.yandex.ru";
    private static String mailSubject,mailContent;


    @Test
    public void testLoginNegative(){

        u.driver.get(BASE_URL);
        if (u.YandexIsLoginCheck()) u.YandexLogoff(); //Check if login before
        YandexMailLoginRoutine(u.driver,userLogin,"notAPassword");
        WebElement errorMsg = u.driver.findElement(By.xpath("//*[@class='error-msg']"));
        Assert.assertTrue(errorMsg.isDisplayed());
    }


   @Test
    public void testLoginPositive(){

        if (u.YandexIsLoginCheck()) u.YandexLogoff(); //Check if login before
        YandexMailLoginRoutine(u.driver,userLogin,userPassword);
        Assert.assertTrue(u.YandexIsLoginCheck());
    }

    @Test
    public void testSendMailPositive(){

        u.driver.get(BASE_URL);
        if (!u.YandexIsLoginCheck()) YandexMailLoginRoutine(u.driver,userLogin,userPassword);
        mailSubject= "test subject" + (Math.random() * 100000000);
        mailContent = "mail content" + (Math.random() * 100000000);
        u.YandexSendMessgeRoutine(mailTo,mailSubject,mailContent);
        WebElement sentFolder = u.driver.findElement(SENT_LINK_LOCATOR);
        sentFolder.click();
        Assert.assertTrue(u.YandexMessageExsistenceCheck(mailSubject));
        WebElement inboxFolder = u.driver.findElement(INBOX_LINK_LOCATOR);
        inboxFolder.click();
        Assert.assertTrue(u.YandexMessageExsistenceCheck(mailSubject));
    }

    @Test
    public void testSendMailNegative(){

        u.driver.get(BASE_URL);
        if (!u.YandexIsLoginCheck()) YandexMailLoginRoutine(u.driver,userLogin,userPassword);
        u.YandexSendMessgeRoutine(mailTo,"notAnAdress","any");
        Assert.assertTrue(!u.YandexIsMessageSent());
        WebElement inboxFolder = u.driver.findElement(INBOX_LINK_LOCATOR);
        inboxFolder.click();
    }

    @Test
    public void testSendMailWithoutSubject(){

        u.driver.get(BASE_URL);
        if (!u.YandexIsLoginCheck()) YandexMailLoginRoutine(u.driver,userLogin,userPassword);
        mailSubject= "";
        mailContent = "mail content" + (Math.random() * 100000000);
        u.YandexSendMessgeRoutine(mailTo,mailSubject,mailContent);
        WebElement sentFolder = u.driver.findElement(SENT_LINK_LOCATOR);
        sentFolder.click();
        Assert.assertTrue(u.YandexMessageExsistenceCheck(mailSubject));
        WebElement inboxFolder = u.driver.findElement(INBOX_LINK_LOCATOR);
        inboxFolder.click();
        Assert.assertTrue(u.YandexMessageExsistenceCheck(mailSubject));
    }

    @Test
    public void testSendMailWithoutContent(){

        u.driver.get(BASE_URL);
        if (!u.YandexIsLoginCheck()) YandexMailLoginRoutine(u.driver,userLogin,userPassword);
        mailSubject= "test subject" + (Math.random() * 100000000);
        mailContent = "";
        u.YandexSendMessgeRoutine(mailTo,mailSubject,mailContent);
        WebElement sentFolder = u.driver.findElement(SENT_LINK_LOCATOR);
        sentFolder.click();
        Assert.assertTrue(u.YandexMessageExsistenceCheck(mailSubject));
        WebElement inboxFolder = u.driver.findElement(INBOX_LINK_LOCATOR);
        inboxFolder.click();
        Assert.assertTrue(u.YandexMessageExsistenceCheck(mailSubject));
    }

   @Test
    public void testCreateAndDeleteDraft() throws InterruptedException {

        u.driver.get(BASE_URL);
        if (!u.YandexIsLoginCheck()) YandexMailLoginRoutine(u.driver,userLogin,userPassword);
        mailSubject= "test subject" + (Math.random() * 100000000); // RANDOM
        mailContent = "dratf content" + (Math.random() * 100000000);// RANDOM
        u.YandexCreateDraftRoutine(mailTo,mailSubject,mailContent);
        WebElement draftFolder = u.driver.findElement(DRAFT_LINK_LOCATOR);
        draftFolder.click();
        WebElement thisLetter = u.driver.findElement(By.xpath(".//*[@title='"+mailSubject+"']"));
        thisLetter.click();
        WebElement deleteMailButton = u.driver.findElement(DELETE_MAIL_BUTTON_LOCATOR);
        deleteMailButton.click();
        WebElement trashFolder = u.driver.findElement(TRASH_LINK_LOCATOR);
        trashFolder.click();
        Assert.assertTrue(u.YandexMessageExsistenceCheck(mailSubject));
    }

    @Test
    public void testPermanentDeleteDraft(){

        u.driver.get(BASE_URL);
        if (!u.YandexIsLoginCheck()) YandexMailLoginRoutine(u.driver,userLogin,userPassword);
        mailSubject= "test subject" + (Math.random() * 100000000); // RANDOM
        mailContent = "draft content" + (Math.random() * 100000000);// RANDOM
        u.YandexCreateDraftRoutine(mailTo,mailSubject,mailContent);
        WebElement draftFolder = u.driver.findElement(DRAFT_LINK_LOCATOR);
        draftFolder.click();
        System.out.println("зашли в драфты "+mailSubject);
        WebElement thisLetter = u.driver.findElement(By.xpath(".//*[@title='"+mailSubject+"']"));
        thisLetter.click();
        WebElement deleteMailButton = u.driver.findElement(DELETE_MAIL_BUTTON_LOCATOR);
        deleteMailButton.click();
        WebElement trashFolder = u.driver.findElement(TRASH_LINK_LOCATOR);
        trashFolder.click();
        thisLetter = u.driver.findElement(By.xpath(".//span[@title='"+mailSubject+"']"));
        thisLetter.click();
        new WebDriverWait(driver, 500).until(ExpectedConditions.presenceOfElementLocated(By.xpath(".//p[contains(text(),'draft content')]")));
        deleteMailButton = u.driver.findElement(DELETE_MAIL_BUTTON_LOCATOR);
        deleteMailButton.click();
        System.out.println(u.YandexMessageExsistenceCheck(mailSubject));
        Assert.assertTrue(!u.YandexMessageExsistenceCheck(mailSubject));
    }

    @BeforeSuite
    public void prepeareForTests() {

        try {
            //u.chromeRemoteLaunch();
            u.firefoxRemoteLaunch();
        } catch (MalformedURLException e) {
            System.out.println("Error: Check start URL!");
        }

        u.driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        u.driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    @AfterSuite
    public void closeWebdriver() {
        u.driver.quit();
    }






}
