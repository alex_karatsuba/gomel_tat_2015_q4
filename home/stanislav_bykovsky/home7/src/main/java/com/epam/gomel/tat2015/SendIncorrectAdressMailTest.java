package com.epam.gomel.tat2015;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SendIncorrectAdressMailTest extends LoginTest{

    public static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    public static final By TO_INPUT_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    public static final By SUBJECT_INPUT_LOCATOR = By.name("subj");
    public static final By MAIL_TEXT_LOCATOR = By.id("compose-send");
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");

    private String incorrectmailTo = "tat-test-useryandex.ru"; // ENUM
    private String mailSubject = "test subject" + Math.random() * 100000000; // RANDOM
    private String mailContent = "mail content" + Math.random() * 100000000;// RANDOM

    public static final By ERROR_WINDOW = By.cssSelector(".b-mail-icon.b-mail-icon_error");

    @Test(description = "Incorrect adress send mail")
    public void sendmail() {
        WebElement composeButton = driver.findElement(COMPOSE_BUTTON_LOCATOR);
        composeButton.click();
        WebElement toInput = driver.findElement(TO_INPUT_LOCATOR);
        toInput.sendKeys(incorrectmailTo);
        WebElement subjectInput = driver.findElement(SUBJECT_INPUT_LOCATOR);
        subjectInput.sendKeys(mailSubject);
        WebElement mailContentText = driver.findElement(MAIL_TEXT_LOCATOR);
        mailContentText.sendKeys(mailContent);
        WebElement sendMailButton = driver.findElement(SEND_MAIL_BUTTON_LOCATOR);
        sendMailButton.click();
        WebElement searchError = driver.findElement(ERROR_WINDOW);
        Assert.assertFalse(searchError.isDisplayed());
    }
}
