package com.gomel.tat.home10.lib.mail.screen.pages;

import com.gomel.tat.home10.lib.mail.Letter;
import com.gomel.tat.home10.lib.mail.screen.Page;
import com.gomel.tat.home10.lib.mail.service.MailService;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

import static com.gomel.tat.home10.lib.ui.WebDriverWaits.*;

public class ComposePage extends Page {
    public static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    public static final By MAILTO_INPUT_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    public static final By SUBJECT_INPUT_LOCATOR = By.name("subj");
    public static final By MAIL_TEXT_LOCATOR = By.id("compose-send_ifr");
    //public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");

    public ComposePage(WebDriver driver) {
        super(driver);
    }

    public ComposePage open() {
        waitForElementIsClickable(COMPOSE_BUTTON_LOCATOR).click();
        return this;
    }

    protected ComposePage typeTo(String mailTo) {
        driver.findElement(MAILTO_INPUT_LOCATOR).sendKeys(mailTo);
        return this;
    }

    protected ComposePage typeSubject(String subject) {
        driver.findElement(SUBJECT_INPUT_LOCATOR).sendKeys(subject);
        return this;
    }

    protected ComposePage typeMailText(String mailText) {
        driver.findElement(MAIL_TEXT_LOCATOR).sendKeys(mailText);
        return this;
    }

    public ComposePage fillLetter(Letter letter) {
        typeTo(letter.getRecipient());
        typeSubject(letter.getSubject());
        typeMailText(letter.getBody());
        return this;
    }

    public ComposePage sendLetter(Letter letter) {
        return MailService.sendLetter(driver, letter);
    }

    public ComposePage submitSend() {
        Actions builder = new Actions(driver);
        builder.keyDown(Keys.CONTROL).sendKeys(Keys.ENTER).perform();
        return new ComposePage(driver);
    }
}
