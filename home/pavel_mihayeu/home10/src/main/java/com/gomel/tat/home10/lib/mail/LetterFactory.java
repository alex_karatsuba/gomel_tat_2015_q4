package com.gomel.tat.home10.lib.mail;

import org.apache.commons.lang3.RandomStringUtils;

import static com.gomel.tat.home10.lib.common.CommonConstants.*;

public class LetterFactory {
    public static final String emptyString = new String();

    public static Letter getRandomLetter() {
        return new Letter(DEFAULT_MAIL_TO_SEND, getRandomMailSubject(), getRandomMailContent());
    }

    public static Letter getLetterWithWrongAddress() {
        return new Letter(RandomStringUtils.randomAlphabetic(10), getRandomMailSubject(), getRandomMailContent());
    }

    public static Letter getEmptyLetter() {
        return new Letter(DEFAULT_MAIL_TO_SEND, emptyString, emptyString);
    }

    public static String getRandomMailSubject() {
        return "test subject: " + RandomStringUtils.randomAlphabetic(10);
    }

    public static String getRandomMailContent() {
        return "test content: " + RandomStringUtils.randomAlphabetic(500);
    }
}
