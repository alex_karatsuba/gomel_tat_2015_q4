package com.gomel.tat.home10.tests.mail;

import com.gomel.tat.home10.lib.mail.Letter;
import com.gomel.tat.home10.lib.mail.LetterFactory;
import com.gomel.tat.home10.lib.mail.screen.pages.ComposePage;
import com.gomel.tat.home10.lib.mail.screen.pages.InboxPage;
import com.gomel.tat.home10.lib.mail.screen.pages.SentPage;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import static com.gomel.tat.home10.lib.mail.service.MailService.isLetterPresent;

public class SendMailTest extends TestPreparations {
    private Letter letter;

    @BeforeClass(description = "Method create random or empty letter using parameter kindOfLetter")
    @Parameters("kindOfLetter")
    public void prepareData(String kindOfLetter) {
        try {
            if (kindOfLetter.equals("Properly")) {
                letter = LetterFactory.getRandomLetter();
            } else if (kindOfLetter.equals("Empty")) {
                letter = LetterFactory.getEmptyLetter();
            }
        } catch (NullPointerException e) {
            System.out.println(e.getMessage());
        }
    }

    @Test(description = "Method login to yandexmail, compose random or empty letter," +
            " open inbox folder and check is the sent letter present")
    @Parameters("kindOfLetter")
    public void sendMail(String kindOfLetter) {
        loginToMail();
        ComposePage composePage = new ComposePage(driver);
        composePage.open();
        composePage.sendLetter(letter);
        new InboxPage(driver).open();
        Assert.assertTrue(isLetterPresent(letter), kindOfLetter + "letter should be present in inbox page");
    }

    @Test(dependsOnMethods = "sendMail", description = "Method open sent mail folder " +
            "and check is the mail sent before present in this folder")
    @Parameters("kindOfLetter")
    public void checkSentMail(String kindOfLetter) {
        new SentPage(driver).open();
        Assert.assertTrue(isLetterPresent(letter), kindOfLetter + "letter should be present in sent page");
    }

}
