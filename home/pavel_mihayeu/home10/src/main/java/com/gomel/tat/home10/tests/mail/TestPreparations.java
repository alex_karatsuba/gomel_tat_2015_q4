package com.gomel.tat.home10.tests.mail;

import com.gomel.tat.home10.lib.mail.screen.pages.LoginPage;
import com.gomel.tat.home10.lib.ui.WebDriverHelper;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;

import java.net.MalformedURLException;

import static com.gomel.tat.home10.lib.common.AccountBuilder.getDefaultAccount;

public class TestPreparations {
    protected WebDriver driver;

    @BeforeClass(description = "Prepare browser")
    @Parameters("browser")
    public void prepareBrowser(String browser) throws MalformedURLException {
        driver = WebDriverHelper.getWebDriver(browser);
    }

    public void loginToMail() {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.open();
        loginPage.loginDefaultUser(getDefaultAccount());
    }

    @AfterClass(description = "Clear data")
    public void shutdown() {
        WebDriverHelper.shutdownWebDriver();
    }
}
