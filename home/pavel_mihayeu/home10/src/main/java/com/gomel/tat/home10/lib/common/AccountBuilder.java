package com.gomel.tat.home10.lib.common;

import org.apache.commons.lang3.RandomStringUtils;

import static com.gomel.tat.home10.lib.common.CommonConstants.*;

public class AccountBuilder {

    public static Account getDefaultAccount() {
        Account account = new Account();
        account.setLogin(DEFAULT_MAIL_USER_LOGIN);
        account.setPassword(DEFAULT_MAIL_USER_PASSWORD);
        account.setEmail(DEFAULT_MAIL_TO_SEND);
        return account;
    }

    public static Account getAccountWithWrongPassword() {
        Account account = getDefaultAccount();
        account.setPassword(account.getPassword() + RandomStringUtils.randomAlphabetic(3));
        return account;
    }
}
