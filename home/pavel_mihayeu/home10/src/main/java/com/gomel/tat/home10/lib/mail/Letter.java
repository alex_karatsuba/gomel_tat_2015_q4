package com.gomel.tat.home10.lib.mail;

import java.io.File;

public class Letter {
    private String recipient;
    private String subject;
    private String body;
    private File attachment;

    public Letter(String recipient, String subject, String body) {
        setRecipient(recipient);
        setSubject(subject);
        setBody(body);
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public File getAttachment() {
        return attachment;
    }

    public void setAttachment(File attachment) {
        this.attachment = attachment;
    }

    public boolean containsAttach() {
        return false;
    }
}
