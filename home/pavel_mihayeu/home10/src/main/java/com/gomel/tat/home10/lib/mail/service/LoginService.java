package com.gomel.tat.home10.lib.mail.service;

import com.gomel.tat.home10.lib.common.Account;
import com.gomel.tat.home10.lib.mail.screen.pages.InboxPage;
import com.gomel.tat.home10.lib.mail.screen.pages.LoginPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import static com.gomel.tat.home10.lib.ui.WebDriverWaits.waitForElementIsVisible;

public class LoginService {
    public static final By USER_NAME_LOCATOR = By.xpath("//*[contains(@class, 'user-name')]");
    public static final By WRONG_LOGIN_ERROR_LOCATOR = By.xpath("//div[@class='error-msg']");


    public static InboxPage loginDefaultUser(WebDriver driver, Account account) {
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        loginPage.typeUsername(account.getLogin());
        loginPage.typePassword(account.getPassword());
        return loginPage.submitLogin();
    }

    public static LoginPage wrongPasswordLogin(WebDriver driver, Account account) {
        LoginPage loginPage = PageFactory.initElements(driver, LoginPage.class);
        loginPage.typeUsername(account.getLogin());
        loginPage.typePassword(account.getPassword());
        return loginPage.submitLoginExpectingFailure();
    }

    public static Boolean checkSuccessLogin() {
        return waitForElementIsVisible(USER_NAME_LOCATOR).isDisplayed();
    }

    public static Boolean checkErrorOnFailedLogin() {
        return waitForElementIsVisible(WRONG_LOGIN_ERROR_LOCATOR).isDisplayed();
    }
}
