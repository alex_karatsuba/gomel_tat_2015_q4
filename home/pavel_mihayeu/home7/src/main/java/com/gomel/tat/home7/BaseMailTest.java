package com.gomel.tat.home7;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

/**
 * Created by Mig on 23.12.2015.
 */
public class BaseMailTest {
    // AUT data
    public static final String BASE_URL = "http://www.ya.ru";
    // UI data
    public static final By ENTER_BUTTON_LOCATOR = By.xpath("//a[contains(@href, 'mail.yandex')]");
    public static final By LOGIN_INPUT_LOCATOR = By.name("login");
    public static final By PASSWORD_INPUT_LOCATOR = By.name("passwd");
    public static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    public static final By TO_INPUT_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    public static final By SUBJECT_INPUT_LOCATOR = By.name("subj");
    public static final By MAIL_TEXT_LOCATOR = By.id("compose-send_ifr");
    public static final By SENT_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
    public static final By DELETE_MAIL_BUTTON_LOCATOR = By.xpath("//div[@class='b-toolbar__i']/div[2]/a[8]");
    public static final By INBOX_LINK_LOCATOR = By.xpath("//a[@href = '#inbox']");
    public static final By SENT_LINK_LOCATOR = By.xpath(".//div[@class='b-folders__i']/div[2]/span[2]/a");
    public static final By NOTIFICATION_ERROR_LOCATOR = By.xpath("//span[@class='b-notification b-notification_error b-notification_error_invalid']");
    public static final By FIRST_MAIL_LOCATOR = By.xpath("//div[@class='b-messages b-messages_threaded']/div[1]");//NOT LINK
    public static final By FIRST_DRAFT_LOCATOR = By.xpath("//div[@class='b-messages']/div[1]");//NOT LINK
    public static final By DRAFT_LINK_LOCATOR = By.xpath("//a[@class='b-folders__folder__link' and @href='#draft']");
    public static final By DRAFT_MAIL_SUBJECT_LOCATOR = By.xpath("//*[@class='b-messages']/div[1]//span[@class='b-messages__subject']");
    public static final By DRAFT_MAIL_PARTICIPANTS_LOCATOR = By.xpath("//*[@class='b-messages']/div[1]//span[@class='b-messages__from__text']");
    // Tools data
    public static final int DRIVER_PAGE_LOAD_TIMEOUT_SECONDS = 20;
    public static final int DRIVE_IMPL_WAIT_TIMEOUT_SECONDS = 5;
    public WebDriver driver;
    // Test data
    public String userLogin = "tat2015-test-mail@yandex.ru";// ACCOUNT
    public String userPassword = "qwertytat2015";// ACCOUNT
    public String mailTo = "tat2015-test-mail@yandex.ru";// ENUM
    public String badMailTo = "asgfafgagfafadhgfh";
    public String mailSubject = "test subject" + Math.random() * 100000000; // RANDOM
    public String mailContent = "mail content" + Math.random() * 100000000;// RANDOM

    @BeforeClass(description = "Prepare browser")
    public void prepareBrowser() throws MalformedURLException {
        driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), DesiredCapabilities.chrome());
        driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(DRIVER_PAGE_LOAD_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(DRIVE_IMPL_WAIT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
    }

    @AfterClass(description = "Close browser")
    public void clearBrowser() {
        driver.quit();
    }

    public WebElement waitForElementIsClickable(By locator) {
        new WebDriverWait(driver, 5000).until(ExpectedConditions.elementToBeClickable(locator));
        return driver.findElement(locator);
    }

    public WebElement waitForElementIsVisible(By locator) {
        new WebDriverWait(driver, 5000).until(ExpectedConditions.visibilityOfElementLocated(locator));
        return driver.findElement(locator);
    }

    public WebElement waitForDisappear(By locator) {
        new WebDriverWait(driver, 5000)
                .until(ExpectedConditions.invisibilityOfElementLocated(locator));
        return driver.findElement(locator);
    }
}
