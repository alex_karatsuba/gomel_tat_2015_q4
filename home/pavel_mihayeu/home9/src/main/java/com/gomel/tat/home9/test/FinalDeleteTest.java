package com.gomel.tat.home9.test;

import com.gomel.tat.home9.ui.page.yandexdisk.TrashPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class FinalDeleteTest extends TrashTest {

    @Test (dependsOnMethods = "moveFileToTrash", description = "Method delete test files from trash and check that the delete notification is displayed")
    public void finalDeleteTest() {
        TrashPage trashPage = new TrashPage(driver);
        trashPage.finalDeleteFile();
        Assert.assertTrue(trashPage.finalDeleteCheck(), "Trash file does not finally deleted");
    }
}
