package com.gomel.tat.home9.test;

import com.gomel.tat.home9.ui.page.yandexdisk.TrashPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TrashTest extends UploadTest {

    @Test(dependsOnMethods = "uploadTest", description = "Method drag and drop test file to trash and check the file is displayed is trash box")
    public void moveFileToTrash() {
        TrashPage trashPage = new TrashPage(driver);
        trashPage.deleteFileToTrash();
        trashPage.open();
        Assert.assertTrue(trashPage.checkIsDeletedFileDisplayed(), "Deleted file does't exists in trash folder");
    }
}
