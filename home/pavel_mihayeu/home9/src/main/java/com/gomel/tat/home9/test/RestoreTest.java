package com.gomel.tat.home9.test;

import com.gomel.tat.home9.ui.page.yandexdisk.FilesPage;
import com.gomel.tat.home9.ui.page.yandexdisk.TrashPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class RestoreTest extends TrashTest {

    @Test(dependsOnMethods = "moveFileToTrash", description = "Method restore deleted to trash files into disk")
    public void restoreTest() {
        TrashPage trashPage = new TrashPage(driver);
        trashPage.restoreTestFileFromTrash();
        FilesPage filesPage = new FilesPage(driver).open();
        Assert.assertTrue(filesPage.checkIsFileDisplayed(), "Trash file does not finally deleted");
    }
}
