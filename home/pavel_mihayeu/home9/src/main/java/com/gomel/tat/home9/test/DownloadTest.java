package com.gomel.tat.home9.test;

import com.gomel.tat.home9.ui.page.yandexdisk.FilesPage;

import org.testng.Assert;
import org.testng.annotations.Test;

import static com.gomel.tat.home9.util.DataProvider.getData;

public class DownloadTest extends UploadTest {
    private String fileName = getData("fileName");
    private FilesPage filesPage = new FilesPage(driver);

    @Test(dependsOnMethods = "uploadTest", description = "Method download test file and check that it exists")
    public void downloadTest() {
        filesPage.downloadFile(fileName);
        Assert.assertTrue(filesPage.checkIsFileExists(), "File does not exist in destination folder");
    }

    @Test(dependsOnMethods = "downloadTest", description = "Method check that is test file content equals result file content")
    public void checkFileContent() {
        Assert.assertEquals(filesPage.getActualContent(), filesPage.getExpectedContent(), "Error. Lost file content");
    }
}
