package com.gomel.tat.home9.test;

import com.gomel.tat.home9.ui.page.yandexdisk.FilesPage;
import com.gomel.tat.home9.ui.page.yandexdisk.TrashPage;
import com.gomel.tat.home9.ui.page.yandexdisk.UploadPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class MultipleDelete extends TestPreparations {
    //TODO Implement multiple upload
    //public static final By MULTIPLE_UPLOAD_BUTTON_LOCATOR = By.xpath("//input[@multiple='multiple' and contains(@class, 'upload-helper')]");

    @Test (description = "Method login to yandex disk, upload 3 test files, drag and drop their to trash and check is that files present in trash box")
    public void multipleDelete() {
        loginYandexDisk();
        UploadPage uploadPage = new UploadPage(driver);
        uploadPage.uploadTestFile1();
        uploadPage.uploadTestFile2();
        uploadPage.uploadTestFile3();
        FilesPage filesPage = new FilesPage(driver);
        filesPage.multipleDeleteFiles();
        TrashPage trashPage = new TrashPage(driver);
        trashPage.open();
        Assert.assertTrue(trashPage.checkIsDeletedFilesDisplayed(), "File does not upload");
    }
}
