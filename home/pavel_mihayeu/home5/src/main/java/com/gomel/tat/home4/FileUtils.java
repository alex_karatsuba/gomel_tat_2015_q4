package com.gomel.tat.home4;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import org.mozilla.universalchardet.UniversalDetector;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * FileUtils provide file functions for WordList class
 *
 * @author Pavel Mihayeu
 * @version 0.2 15 Dec 2015
 */

public class FileUtils {
    public URL getGitArchiveURL(String address) {
        URL gitArchiveURL = null;
        try {
            gitArchiveURL = new URL(address);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return gitArchiveURL;
    }

    public void downloadGitArchive(String gitArchiveURL, String gitArchiveName) {
        try {
            File gitArchiveFile = new File(gitArchiveName);
            org.apache.commons.io.FileUtils.copyURLToFile(getGitArchiveURL(gitArchiveURL), gitArchiveFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void unZipGitArchive(String source, String destination) {
        try {
            ZipFile zipFile = new ZipFile(source);
            zipFile.extractAll(destination);
        } catch (ZipException e) {
            e.printStackTrace();
        }
    }

    public String getFileEncodingType(String filePath) {
        byte[] buf = new byte[8];
        UniversalDetector detector = new UniversalDetector(null);
        int bufNum;
        try {
            FileInputStream fileInputStream = new FileInputStream(filePath);
            try {
                while ((bufNum = fileInputStream.read(buf)) > 0 && !detector.isDone()) {
                    detector.handleData(buf, 0, bufNum);
                }
            } finally {
                fileInputStream.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        detector.dataEnd();
        String encoding = detector.getDetectedCharset();
        detector.reset();
        return encoding;
    }

    public void delTempFiles(String tempFolder, String zipArchive) {
        if (org.apache.commons.io.FileUtils.getFile(tempFolder).exists()) {
            try {
                org.apache.commons.io.FileUtils.deleteDirectory(new File(tempFolder));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (org.apache.commons.io.FileUtils.getFile(zipArchive).exists()) {
            try {
                org.apache.commons.io.FileUtils.forceDelete(new File(zipArchive));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


}