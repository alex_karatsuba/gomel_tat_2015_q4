package com.gomel.tat.home8.test;

import com.gomel.tat.home8.ui.page.yandexmail.LoginPage;
import com.gomel.tat.home8.util.Locators;
import org.testng.Assert;
import org.testng.annotations.Test;

import static com.gomel.tat.home8.util.DataProvider.getData;

/**
 * Created by Mig on 04.01.2016.
 */
public class NegativeLoginTest extends TestPreparations {

    @Test
    public void negativeLogin() {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.open();
        loginPage.badLoginAs(getData("userLogin"), getData("badUserPassword"));
        Assert.assertTrue(driver.findElement(Locators.get("BAD_LOGIN_ERROR_LOCATOR")).isDisplayed(), "Error! Login success");
    }
}
