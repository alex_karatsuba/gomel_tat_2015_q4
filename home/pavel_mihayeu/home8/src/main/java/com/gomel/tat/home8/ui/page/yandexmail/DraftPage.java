package com.gomel.tat.home8.ui.page.yandexmail;

import com.gomel.tat.home8.util.Locators;
import com.gomel.tat.home8.ui.page.Page;
import org.openqa.selenium.WebDriver;

import static com.gomel.tat.home8.util.DataProvider.getData;
import static com.gomel.tat.home8.util.WebDriverHelper.waitForElementIsVisible;

/**
 * Created by Mig on 03.01.2016.
 */
public class DraftPage extends Page {

    public DraftPage(WebDriver driver) {
        super(driver);
    }

    public DraftPage open() {
        driver.navigate().to(getPageUrl(getData("partOfDraftUrl")));
        return new DraftPage(driver);
    }

    public String getDraftId() {
        String currentUrl = driver.getCurrentUrl();
        String draftId = currentUrl.substring(currentUrl.lastIndexOf("/") + 1);
        return draftId;
    }

    public DraftPage deleteDraft(String draftId) {
        deleteMail(draftId);
        return new DraftPage(driver);
    }

    public void waitIsDraftSave() {
        waitForElementIsVisible(Locators.get("SAVE_DRAFT_LOCATOR"));
    }
}
