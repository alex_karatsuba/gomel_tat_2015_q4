package com.gomel.tat.home8.ui.page.yandexmail;

import com.gomel.tat.home8.util.Locators;
import com.gomel.tat.home8.ui.page.Page;
import org.openqa.selenium.WebDriver;

import static com.gomel.tat.home8.util.WebDriverHelper.waitForElementIsClickable;

/**
 * Created by Mig on 28.12.2015.
 */
public class SentPage extends Page {

    public SentPage(WebDriver driver) {
        super(driver);
    }

    public SentPage open() {
        waitForElementIsClickable(Locators.get("SENT_LINK_LOCATOR")).click();
        return new SentPage(driver);
    }
}
