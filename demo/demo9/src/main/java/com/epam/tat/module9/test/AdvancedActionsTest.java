package com.epam.tat.module9.test;

import com.epam.tat.module9.util.WebDriverHelper;
import org.omg.Messaging.SYNC_WITH_TRANSPORT;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DriverCommand;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

/**
 * Created by Aleh_Vasilyeu on 12/28/2015.
 */
public class AdvancedActionsTest {

    @Test
    public void testDragAndDrop2() {
        WebDriver driver = WebDriverHelper.getWebDriver();

        driver.get("https://jqueryui.com/draggable/");
        driver.switchTo().frame(driver.findElement(By.tagName("iframe")));

        WebElement draggable = driver.findElement(By.id("draggable"));

        Action action = new Actions(driver).dragAndDropBy(draggable, 100, 100).build();
        action.perform();
    }

    @Test
    public void testFrame() {
        WebDriver driver = WebDriverHelper.getWebDriver();

        driver.get("http://the-internet.herokuapp.com/nested_frames");

        driver.switchTo().defaultContent();
        driver.switchTo().frame("frame-top");
        driver.switchTo().frame("frame-left");
        System.out.println("Left: \n" + driver.getPageSource());


        driver.switchTo().defaultContent();
        driver.switchTo().frame("frame-top");
        driver.switchTo().frame("frame-right");
        System.out.println("Right: \n" + driver.getPageSource());

        driver.switchTo().defaultContent();
        driver.switchTo().frame("frame-bottom");

        System.out.println("Bottom: \n" + driver.getPageSource());
    }

    @Test
    public void testWindowSwitch() {
        WebDriver driver = WebDriverHelper.getWebDriver();
        driver.get("http://the-internet.herokuapp.com/windows");

        String oldHandle = driver.getWindowHandle();

        driver.findElement(By.linkText("Click Here")).click();
        for(String handle :driver.getWindowHandles()) {
            driver.switchTo().window(handle);
        }
        System.out.println("On new page present text: " + driver.findElement(By.tagName("h3")).getText());

        driver.switchTo().window(oldHandle);
        System.out.println("On old page present text: " + driver.findElement(By.linkText("Click Here")).getText());
    }

    @Test
    public void pressKey() {
        WebDriver driver = WebDriverHelper.getWebDriver();
        driver.get("http://the-internet.herokuapp.com/key_presses");

        Action action = new Actions(driver).sendKeys("d").build();
        action.perform();

        System.out.println("Result: " + driver.findElement(By.id("result")).getText());
    }

    public void testHover() {

    }

    @Test
    public void testConfirm() {
        WebDriver driver = WebDriverHelper.getWebDriver();
        driver.get("http://the-internet.herokuapp.com/javascript_alerts");
        WebElement button = driver.findElement(By.xpath("//button[text()='Click for JS Confirm']"));
        button.click();

        Alert alert = driver.switchTo().alert();
        alert.accept();

        System.out.println("Result: " + driver.findElement(By.id("result")).getText());
    }

    @Test
    public void testAlert() {
        WebDriver driver = WebDriverHelper.getWebDriver();
        driver.get("http://the-internet.herokuapp.com/javascript_alerts");
        WebElement button = driver.findElement(By.xpath("//button[text()='Click for JS Alert']"));
        button.click();

        System.out.println("Result: " + driver.findElement(By.id("result")).getText());
    }

    @Test
    public void testPrompt() {
        WebDriver driver = WebDriverHelper.getWebDriver();
        driver.get("http://the-internet.herokuapp.com/javascript_alerts");
        WebElement button = driver.findElement(By.xpath("//button[text()='Click for JS Prompt']"));
        button.click();

        Alert alert = driver.switchTo().alert();
        alert.sendKeys("Hello");
        alert.accept();

        System.out.println("Result: " + driver.findElement(By.id("result")).getText());
    }


    @Test
    public void testJavaScriptExecute() {
        WebDriver driver = WebDriverHelper.getWebDriver();
        driver.get("http://the-internet.herokuapp.com/javascript_alerts");
        String result = (String) ((JavascriptExecutor)driver).executeScript("alert('hello');");

        Alert alert = driver.switchTo().alert();
        alert.accept();

        System.out.println("Result:" + result);
    }

    @Test
    public void testUpload() {
        WebDriver driver = WebDriverHelper.getWebDriver();
        driver.get("http://the-internet.herokuapp.com/upload");

        driver.findElement(By.id("file-upload")).sendKeys("C:\\screenshot.png");
        driver.findElement(By.id("file-upload")).submit();

        Assert.assertTrue(driver.findElement(By.id("uploaded-files")).getText().trim().equals("screenshot.png"));
    }

    @Test
    public void testDownload() throws InterruptedException {
        WebDriver driver = WebDriverHelper.getWebDriver();
        driver.get("http://the-internet.herokuapp.com/download");
        driver.findElement(By.linkText("screenshot.png")).click();

        Thread.sleep(10000);
    }

    @AfterClass
    public void stopBrowser() {
        WebDriverHelper.shutdownWebDriver();
    }
}
