package com.epam.tat.module9.ui.page.yandexmail;

import com.epam.tat.module9.bo.Letter;
import com.epam.tat.module9.ui.page.Page;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

import static com.epam.tat.module9.util.WebDriverHelper.waitForElementIsClickable;

/**
 * Created by Aleh_Vasilyeu on 12/24/2015.
 */
public class InboxPage extends Page {

    public static final int TIME_OUT_MAIL_ARRIVED_SECONDS = 10;

    public static final By INPOX_LINK_LOCATOR = By.xpath("//a[@href='#inbox']");

    public static final String MAIL_LINK_LOCATOR_PATTERN = "//*[@class='block-messages']//a[contains(., '%s')]";

    public InboxPage(WebDriver driver) {
        super(driver);
    }

    public void open() {
        waitForElementIsClickable(INPOX_LINK_LOCATOR).click();
    }

    public boolean isLetterPresent(Letter letter) {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.MILLISECONDS);

        try {
            new WebDriverWait(driver, TIME_OUT_MAIL_ARRIVED_SECONDS)
                    .until(ExpectedConditions.visibilityOfElementLocated(
                            By.xpath(String.format(MAIL_LINK_LOCATOR_PATTERN, letter.getSubject()))));
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
