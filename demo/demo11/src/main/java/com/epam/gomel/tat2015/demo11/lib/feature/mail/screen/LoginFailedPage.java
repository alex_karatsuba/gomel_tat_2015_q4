package com.epam.gomel.tat2015.demo11.lib.feature.mail.screen;

import com.epam.gomel.tat2015.demo11.lib.ui.Browser;
import org.openqa.selenium.By;

/**
 * Created by Konstantsin_Simanenk on 1/14/2016.
 */
public class LoginFailedPage {

    By errorMessage = By.className("");

    public String getErrorMassage() {
        return Browser.current().getText(errorMessage);
    }
}
