package com.epam.gomel.tat2015.demo11.lib.feature.mail.service;

import com.epam.gomel.tat2015.demo11.lib.feature.mail.screen.LoginFailedPage;
import com.epam.gomel.tat2015.demo11.lib.feature.common.Account;
import com.epam.gomel.tat2015.demo11.lib.feature.mail.screen.MailLoginPage;

/**
 * Created by Konstantsin_Simanenk on 1/11/2016.
 */
public class MailLoginService {

    public void loginToMailbox(Account account) {
        MailLoginPage.open()
                .login(account.getLogin(), account.getPassword());
    }

    public void checkSuccessLogin(Account account) {
        // Logger
        String address = MailLoginPage.open()
                .login(account.getLogin(), account.getPassword())
                .getCurrentAddress();
        if (account == null || !address.equals(account.getEmail())) {
            throw new RuntimeException("Login to mailbox failed. Login = " + account.getLogin() + ", Password = " + account.getPassword() + ". Current email = " + address);
        }
    }

    public String retrieveErrorOnFailedLogin(Account account) {
        MailLoginPage.open().login(account.getLogin(), account.getPassword());
        return new LoginFailedPage().getErrorMassage();
    }

}
