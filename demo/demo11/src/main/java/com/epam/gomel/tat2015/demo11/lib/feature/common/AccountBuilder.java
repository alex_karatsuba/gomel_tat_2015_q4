package com.epam.gomel.tat2015.demo11.lib.feature.common;


import com.epam.gomel.tat2015.demo11.lib.util.Randomizer;

import static com.epam.gomel.tat2015.demo11.lib.feature.common.CommonConstants.*;

/**
 * Created by Konstantsin_Simanenk on 1/11/2016.
 */
public class AccountBuilder {

    public static Account getDefaultAccount() {
        Account account = new Account();
        account.setLogin(DEFAULT_USER_LOGIN);
        account.setPassword(DEFAULT_USER_PASSWORD);
        account.setEmail(DEFAULT_USER_MAIL);
        return account;
    }

    public static Account getAccountWithWrongPass() {
        Account account = getDefaultAccount();
        account.setPassword(account.getPassword() + Randomizer.numeric());
        return account;
    }

    public static Account getNonExistedAccount() {
        Account account = getDefaultAccount();
        account.setLogin(account.getLogin() + Randomizer.numeric());
        return account;
    }

}
