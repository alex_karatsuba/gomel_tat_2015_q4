package com.epam.tat.module8.test;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.security.UserAndPassword;
import org.openqa.selenium.support.ui.Select;

import java.util.concurrent.TimeUnit;

public class SeleniumWebDriverApiOverview {

    public void webDriverCommandsOverview() {
        WebDriver driver = null;

        // base browser operation
        driver.get("http://...");
        driver.navigate().back();
        driver.navigate().forward();
        driver.navigate().refresh();
        driver.navigate().to("http://...");
        driver.close();
        driver.quit();

        // base browser getters
        driver.getCurrentUrl();
        driver.getPageSource();
        driver.getTitle();
        driver.getWindowHandle();
        driver.getWindowHandles();

        // browser cookies
        driver.manage().getCookies();

        // window position
        driver.manage().window().getPosition();
        driver.manage().window().getSize();
        driver.manage().window().maximize();
        driver.manage().window().setPosition(new Point(12, 12));

        // driver timeouts
        driver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        driver.manage().timeouts().setScriptTimeout(10, TimeUnit.SECONDS);

        // driver logs
        driver.manage().logs().getAvailableLogTypes();

        // switch
        driver.switchTo().window("");
        driver.switchTo().frame(1);
        driver.switchTo().frame("nameOrId");
        driver.switchTo().frame(driver.findElement(By.id("")));
        driver.switchTo().parentFrame();
        driver.switchTo().defaultContent();
        driver.switchTo().activeElement();

        // alert ops
        driver.switchTo().alert().accept();
        driver.switchTo().alert().dismiss();
        driver.switchTo().alert().getText();
        driver.switchTo().alert().sendKeys("");
        driver.switchTo().alert().authenticateUsing(new UserAndPassword("test", "test"));

        // Finding elements
        WebElement someElement = driver.findElement(By.className(""));
        driver.findElements(By.xpath(""));

        // Elements ops
        driver.findElement(By.className("")).findElement(By.className(""));
        driver.findElement(By.className("")).findElements(By.cssSelector(""));

        // element basic user operations
        driver.findElement(By.className("")).click();
        driver.findElement(By.className("")).sendKeys("test string");
        driver.findElement(By.className("")).clear();
        driver.findElement(By.className("")).submit(); // for forms

        // Element getters
        driver.findElement(By.className("")).getTagName();
        driver.findElement(By.className("")).getAttribute("name");
        driver.findElement(By.className("")).getCssValue("color");
        driver.findElement(By.className("")).getSize();
        driver.findElement(By.className("")).getLocation();
        driver.findElement(By.className("")).getText();

        // Element boolean options
        driver.findElement(By.className("")).isDisplayed();
        driver.findElement(By.className("")).isEnabled();
        driver.findElement(By.className("")).isSelected();

        // Special for select tags
        Select select = new Select(driver.findElement(By.id("")));

        // Select : select smth
        select.selectByIndex(1);
        select.selectByValue("");

        // Select : get options / get selected
        select.getOptions();
        select.getFirstSelectedOption();
        select.getAllSelectedOptions();

        // Select : deselect smth
        select.deselectAll();
        select.deselectByIndex(1);
        select.deselectByValue("Test");
        select.deselectByVisibleText("Test");

        // javascript executor
        ((JavascriptExecutor) driver).executeScript("window.open()");
        ((JavascriptExecutor) driver).executeAsyncScript("window.open(); arguments[arguments.length - 1]()");

        // advanced actions
        Actions actions = new Actions(driver);
        actions.build();
        actions.perform();
    }

}
