package com.epam.tat.module8.bo;

/**
 * Created by Aleh_Vasilyeu on 12/24/2015.
 */
public class Letter {

    private String to;

    private String subject;

    private String body;


    public Letter(String to, String subject, String body) {
        this.to = to;
        this.subject = subject;
        this.body = body;
    }

    public String getTo() {
        return to;
    }

    public String getSubject() {
        return subject;
    }

    public String getBody() {
        return body;
    }
}
