package com.epam.tat.module8.ui.page.yandexmail;

import com.epam.tat.module8.bo.Letter;
import com.epam.tat.module8.ui.page.Page;
import com.epam.tat.module8.util.WebDriverHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import static com.epam.tat.module8.util.WebDriverHelper.waitForDisappear;

/**
 * Created by Aleh_Vasilyeu on 12/24/2015.
 */
public class ComposePage extends Page {

    public static final By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");
    public static final By TO_INPUT_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    public static final By SUBJECT_INPUT_LOCATOR = By.name("subj");
    public static final By MAIL_TEXT_LOCATOR = By.id("compose-send");
    public static final By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");

    public ComposePage(WebDriver driver) {
        super(driver);
    }

    public void open() {
        WebElement composeButton = driver.findElement(COMPOSE_BUTTON_LOCATOR);
        composeButton.click();

    }

    public void sendLetter(Letter letter) {
        WebElement toInput = driver.findElement(TO_INPUT_LOCATOR);
        toInput.sendKeys(letter.getTo());
        WebElement subjectInput = driver.findElement(SUBJECT_INPUT_LOCATOR);
        subjectInput.sendKeys(letter.getSubject());
        WebElement mailContentText = driver.findElement(MAIL_TEXT_LOCATOR);
        mailContentText.sendKeys(letter.getBody());

        WebElement sendMailButton = driver.findElement(SEND_MAIL_BUTTON_LOCATOR);
        sendMailButton.click();

        waitForDisappear(SEND_MAIL_BUTTON_LOCATOR);
    }
}
