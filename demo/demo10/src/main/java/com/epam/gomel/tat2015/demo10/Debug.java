package com.epam.gomel.tat2015.demo10;

import com.epam.gomel.tat2015.demo10.lib.mail.screen.MailLoginPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

/**
 * Created by Konstantsin_Simanenk on 1/11/2016.
 */
public class Debug {

    @Test(description = "Debug")
    public void test() {
        WebDriver driver = new FirefoxDriver();
        driver.get("http://mail.yandex.ru");
        MailLoginPage mailLoginPage = PageFactory.initElements(driver, MailLoginPage.class);
        mailLoginPage.login("Test", "Test");
    }
}
