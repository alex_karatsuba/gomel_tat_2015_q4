package com.epam.gomel.tat2015.demo10.tests.mail;

import com.google.common.base.Function;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class SendMailSuccessTest {

    public static final String MAIL_LOGIN_PAGE_URL = "http://mail.yandex.ru";
    private String login = "igorepamtest";
    private String password = "qwertyqwerty";

    private String emailTo = "igorepamtest@ya.ru";
    private String emailSubject = RandomStringUtils.randomAlphanumeric(40);
    private String emailBodyText = RandomStringUtils.randomAlphanumeric(1000);

    private By loginInput = By.name("login");
    private By loginPassInput = By.name("passwd");
    private By loginFormSubmit = By.cssSelector(".new-auth-form-line button[type='submit']");

    private By composeButton = By.cssSelector("[href='#compose']");
    private By latterFieldToFocusElement = By.xpath("//div[./input[@name='to']]");
    private By fieldToInput = By.xpath("//input[@id]");
    private By letterSubjectInput = By.id("compose-subj");
    private By letterBodyInput = By.id("compose-send");
    private By letterSubmitButton = By.id("compose-submit");

    private By inboxLink = By.cssSelector(".block-left-box [href='#inbox']");

    private By inboxPaneElement = By.xpath("//div[@class='block-messages' and .//div[@data-value='#inbox']]");
    private By sentLetterLink = By.linkText("");
    private String LATTER_LOCATOR_PATTERN = "//*[text()='%s']";

    public static final int OPEN_INBOX_TIME_OUT = 10;
    public static final int COMMON_OPERATION_TIME_OUT = 10;
    public static final int WEBDRIVER_IMPLICIT_TIME_OUT = 10;
    private WebDriver driver;

    @BeforeClass(description = "Login to mailbox")
    public void loginToMailbox() {
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(WEBDRIVER_IMPLICIT_TIME_OUT, TimeUnit.SECONDS);
        driver.get(MAIL_LOGIN_PAGE_URL);
        driver.findElement(loginInput).sendKeys(login);
        driver.findElement(loginPassInput).sendKeys(password);
        driver.findElement(loginFormSubmit).click();
        new WebDriverWait(driver, OPEN_INBOX_TIME_OUT).until(ExpectedConditions.visibilityOfElementLocated(inboxLink));
    }

    @Test(description = "Send common letter")
    public void sendMail() {
        driver.findElement(composeButton).click();
        WebElement fieldToFocus = driver.findElement(latterFieldToFocusElement);
        fieldToFocus.findElement(fieldToInput).sendKeys(emailTo);
        driver.findElement(letterSubjectInput).sendKeys(emailSubject);
        driver.findElement(letterBodyInput).sendKeys(emailBodyText);
        driver.findElement(letterSubmitButton).click();
        new WebDriverWait(driver, COMMON_OPERATION_TIME_OUT).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[text()='Письмо успешно отправлено.']")));
    }

    @Test(description = "Check letter present in inbox", dependsOnMethods = "sendMail")
    public void checkMailExistsInInbox() {
        driver.findElement(inboxLink).click();
        new WebDriverWait(driver, COMMON_OPERATION_TIME_OUT).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(String.format(LATTER_LOCATOR_PATTERN, emailSubject))));

        /*

        final WebElement inboxPane = new WebDriverWait(driver, OPEN_INBOX_TIME_OUT).until(ExpectedConditions.visibilityOfElementLocated(inboxPaneElement));
        new FluentWait<WebElement>(inboxPane)
                .withTimeout(COMMON_OPERATION_TIME_OUT, TimeUnit.SECONDS)
                .ignoring(WebDriverException.class)
                .until(new Function<WebElement, Object>() {
                    public WebElement apply(WebElement input) {
                        return input.findElement(By.xpath(String.format(LATTER_LOCATOR_PATTERN, emailSubject)));
                    }
        });

        */
    }

    @AfterClass(description = "Clear data")
    public void finish() {
        driver.quit();
    }

}
